#! /usr/bin/python
# Author: Anna Corazza, 24 Jul 2015
# 28 Jul 2015: second version including barrier features
# 29 Jul 2015: bug fix and modification of the syntax of the annotation file

# A feature selection step is based on a precompiled dictionnary
# A few additional features can be included
# Input: a file containing informations about entity and relations
# occurring in the document:
# a line for each entity in the document, containing:
# entityCode, number of corresponding mentions N, i_1, j_1, i_2, j_2,
#                                                               ..., i_N, j_N
# where the entity code is for example D007069 or C064227; i and j are
# the indexes of the first and last tokens of each mention; the end of
# sentence is taken into account in this numbering; fields are
# separated by a blank
#
# Following these lines, a line 
# RELATION
# and then, a line for each potential relation, that is
# for each pair of entities which is compatible with a relation; these
# lines should contain:
# isARel, CID, entityCode, typeOfEntity, entityCode, typeOfEntity
# where isARel is a Boolean flag (1 for true, -1 for false) signalling
#                     wether we are considering a golden case relation
# CID is the only type of relation we are considering now, but I would
#                    insert it for generality
# entityCode as above
# typeOfEntity is either Chemical or Disease
# the last two fields are repeated for each of the two entities
# involved in the relation
# Again, all fields are separated by blanks. 
# Output: the file in the SVM format

import sys, re, os
from datetime import datetime

INPUTDIR = 'data/features/'
ANNOTATIONDIR = 'data/annotation/'

# input: a string containing the barrier feature notation, which can
# assume two different forms:
#    bar:Barrierar(DT-VBN)={NNS, VBD, CC}
#    setBarriera_sinistra(DT-NNP)={NNP,POS}

# output: a list [direction, delimiters, contents], where direction
# can be l(eft) or r(ight), delimiters are the trigger and the
# end-point and contents is a list corresponding to the set of the PoS
# between the two
def extractBarrierFeature(ff):
    if ff.startswith('bar:'):
        l = len('bar:Barriera');
        direction=ff[l];
    else:    # ff.startswith('setBarriera_'):
        if ff.startswith('setBarriera_sinistra'):
            direction = 'l';
        else:   # ff.startswith('setBarriera_destra'):
            direction = 'r';
    [prefix,pattern] = ff.split('(',1);
    [delimiters, rest] = pattern.split(')',1);
    # COMMA 
    rest=rest.replace('{,,', 'COMMA, ');
    rest=rest.replace(',,}', 'COMMA, ');
    rest=rest.replace(',,,', ',COMMA, ');
    contents = rest[2:len(rest)-1].split(',',1);
    return [direction, delimiters, contents];

# a BF activate a dictionnary one whenever the first two args are
# exactly the same, while the contents is included in the one of the
# dictionary
def isBarrierFeatureIn(a,b):
    if a[0:1] == b[0:1] and set(a[2]).issubset(set(b[2])):
        return True;
    return False;

############# Msain program starts here
try:
    dictFeaturesFile = open(sys.argv[1], 'r');
    INPUTDIR = sys.argv[2]
    ANNOTATIONDIR = sys.argv[3]    
    outDir = sys.argv[4]
    #print ( 'dictFeaturesFile INPUTDIR ANNOTATIONDIR fout');
    print ('dictFeaturesFile'+' '+INPUTDIR+' '+ANNOTATIONDIR+' '+outDir, file=sys.stdout);
except:
    print ('Correct syntax: ' + sys.argv[0] + ' dictFeaturesFile'+" "+INPUTDIR+" "+ANNOTATIONDIR+ " "+outDir,file=sys.stdout);
    exit(0);
    
# reading the feature dictionnaries (BF and the others)
dictFeatures = [];
dictBarrierFeatures = [];
for line in dictFeaturesFile:
    line = line[0:len(line)-1];  # chop
    fields = line.split();
    if fields[1].startswith('bar:'):
        dictBarrierFeatures.append(extractBarrierFeature(''.join(fields[1:len(fields)-1])));
    else:
        dictFeatures.append(''.join(fields[1:len(fields)-1]));
featN = len(dictFeatures)+len(dictBarrierFeatures);
    
# reading the list of files corresponding to the documents to process
fileList = os.listdir(INPUTDIR);
for fileName in fileList:
    (name, ext) = fileName.split('.');
    outFile = outDir+"/"+name+".dat"
    fout = open(outFile,"w")
    # processing the file containg the features extracted from the
    # document
    try:
        features4tokenFile = open(INPUTDIR+name+'.features', 'r');
    except:
        print ('Warning: not able to open features file ' + INPUTDIR + name + '.features',file=sys.stderr);

    # print  >> sys.stderr, 'Processing document ' + INPUTDIR + name + '...';
    tokens = [];  # only for debugging
    features4token = [];
    start = datetime.now()
    for line in features4tokenFile:
        line = line[0:len(line)-1];  # chop
        fields = line.split();
        tokens.append(fields[0]);
        # Delete spaces inside {}
        # line=re.sub(r'(?=\{[^\s\}]*)\s+',r'',line);
        featList = [];
        barrierFeatList = [];
        for feat in fields[1:]:
            # barrier features need a more accurate comparison
            if feat.startswith('bar:') or feat.startswith('setBarriera_'):
                barrierFeat = extractBarrierFeature(feat);
                index = len(dictFeatures);
                for refBarrierFeat in dictBarrierFeatures:
                    if isBarrierFeatureIn(barrierFeat,refBarrierFeat):
                        barrierFeatList.append(index);
                    index += 1;
            else:
                if feat in dictFeatures:
                    index = dictFeatures.index(feat);
                    if index not in featList:
                        featList.append(index); 
        featList.sort();
        barrierFeatList.sort();

        features4token.append(featList);
        features4token.append(barrierFeatList);
    features4tokenFile.close();
    end = datetime.now()
    featureTime=end -start;

    # processing the annotation file
    try:
        annotationFile = open(ANNOTATIONDIR+name+'.annotation', 'r');
    except:
        print ('Warning: not able to open annotation file ' + ANNOTATIONDIR+name+'.annotation',file=sys.stderr);
    print ('Processing annotation file ' + ANNOTATIONDIR+name+'.annotation',file=sys.stderr);
    
    start = datetime.now()
    # entity lines first
    entityList = {};    
    for line in annotationFile:
        if line.startswith('RELATION'):
            break;
        line = line[0:len(line)-1];  # chop
        fields = line.split();
        if len(fields) == 0:
            break;
        # Construction of the part of the feature vector corresponding 
        # to the entity
        # fields: entityCode, number of corresponding mentions N, mentions
        mentionList = [];
        ptr = 2;
        for i in range(int(fields[1])):
            try:
                for j in range(int(fields[ptr]),int(fields[ptr+1])+1): 
                    # print >> sys.stderr, tokens[j-1];
                    for feat in features4token[j-1]:
                        if feat not in mentionList:
                            mentionList.append(feat);
                ptr += 2;
            except:
                print ( 'Warning: wrong annotation in file ', name, ' in line ', line,file=sys.stderr); 
                print ( "Error:", sys.exc_info(),file=sys.stderr)
            # print '::'.join(mentionList);
            mentionList.sort();
            entityList[fields[0]] = mentionList;


    # and then relation lines
    for line in annotationFile:
        line = line[0:len(line)-1];  # chop
        fields = line.split();
        if len(fields) == 0:
            break;
        # len(dictFeatures) must be added to indexes of the second
        # entity for every feature; futhermore, the value of the
        # features is always 1 as the features are Boolean
        # SVM features indexes start from 1
        out = fields[0] + ' ';
        try:
            for feat in entityList[fields[2]]:
                out += str(feat+1) + ':1 ';
            for feat in entityList[fields[4]]:
                out += str(feat + 1 + featN) + ':1 ';
            for i in range(6,len(fields)):    # additional features
                if int(fields[i]) > 0:
                    out += str(2*featN+i+1-6) + ':1 ';
            print (out,file=fout);
        except: 
            print ( 'Wrong annotation (may be due to preceding errors) in annotation file ' + name,file=sys.stderr); 
            print ( fields,file=sys.stderr);
            print ( "Unexpected error:", sys.exc_info()[0],file=sys.stderr);
            raise;
            
    annotationFile.close();
    end = datetime.now()
    fout.close();
    print  ('Document ' + INPUTDIR + name + ' completed in\t', featureTime, ' + ', end -start,file=sys.stderr);
