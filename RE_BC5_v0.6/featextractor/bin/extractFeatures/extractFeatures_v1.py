import sys, re, os
import string
from pprint import pprint
from nltk.corpus import wordnet as wn

# author: Anita Alicante
# date: 14/10/2012
# modified by: Anna Corazza (AC)
# date: 14/7/2014

def aggiungi_features(old_feature, features, value, i, features_count,fh):
    # funzione che costruisce il vettore di features

    if not old_feature:
        features[i] = value
        features_count.append(1)
        print(str(features[i]),end=" ",file=fh)
        #       print str(i)+' ',

        #Prints for debbuging
        #print 'NUOVA '+str(i)+':1 ',
        #print 'FEATURES_count', features_count
        #print 'FEATURES_value',features
        i = i + 1
    else:
        features_count[old_feature - 1] = features_count[old_feature - 1] + 1
        #       if(features_count[old_feature-1]> UNKNOWN):
        print(str(value),end=" ",file=fh),
    #print  str(i)+' ',
    #features_count.pop(old_feature-1)
    #last_index=len(features_count)
    #features[last_index]=value
    #features_count.append(4)

    #Prints for debbuging
    # print 'VECCHIA '+str(old_feature)+':1 ',
    #    print 'FEATURES_count', features_count
    #    print 'FEATURES_value',features
    #       i=len(features)
    return i


def esiste_feature(value, features, i):
    # Verifica se esiste o meno la features
    # Se non esiste ritorna il valore 0
    # Altrimenti restituisce l'indice della feature

    # param1 valore della feature
    # param2 struttura dati che contien tutte le features
    # param3 indice della features

    feature = 0

    if len(features):
        for k, v in features.items():
            if v == value:
                feature = k
    return feature

def aggiungi_features_wordnet(item):
#       print maxf
        #hyper_list=dict()
        #print concepts_sen
        hypernyms_list=list()
       # for item in concepts_sen:
        if item:
                #print item
                synsets=wn.synsets(item)
                if synsets:
                        hypernyms_list=synsets[0].hypernyms()
                        if len(hypernyms_list)>0:
                               # if item not in hyper_list:
                                #        hyper_list[item]=list()
                                        #print item
                                        #print  hypernyms_list
                                        for i,ob in enumerate(hypernyms_list):
                          #                      maxf=maxf+1
                                                #print maxf
                                                index=i+1
                                                #print "Hyper"+str(index)+":("+item+"):"+hypernyms_list[i].name+" "
                                                string_hyper=str(hypernyms_list[i].name)
                                                #print(string_hyper)													
                                                string_hyper=string_hyper[string_hyper.find('(')+2:string_hyper.find(')')]
                                                #print(string_hyper)
                                                sh=string_hyper.split('.')[0] 
                                                hypernyms_list[i]="Hyper:hyper"+str(index)+":("+item+"):"+ sh
                #               print hyper_list[item]

                        #print


        return  hypernyms_list


#Metodi per estrare le features dalla frase

def push_word(sent, index):
    #description _ pushes a word feature onto a feature set
    #param1 _ feature set
    #param3 _ argument list reference

    word_list = list()

    #Unigrams
    if (index > 1):
        word_list.append('unigram_word:' + sent[index - 2])
        word_list.append('bigram_word:' + sent[index - 2] + "----" + sent[index - 1])
        word_list.append('trigram_word:' + sent[index - 2] + '----' + sent[index - 1] + '----' + sent[index])
        if (index < (len(sent) - 1)):
            word_list.append('trigram_word:' + sent[index - 2] + '----' + sent[index - 1] + '----' + sent[index])
    if (index > 0):
        word_list.append('unigram_word:' + sent[index - 1])
        word_list.append('bigram_word:' + sent[index - 1] + "----" + sent[index])
        if (index < (len(sent) - 1)):
            word_list.append('trigram_word:' + sent[index - 1] + '----' + sent[index] + '----' + sent[index + 1])
    word_list.append('unigram_word:' + sent[index])
    if (index < (len(sent) - 1)):
        word_list.append('unigram_word:' + sent[index + 1])
        word_list.append('bigram_word:' + sent[index] + "----" + sent[index + 1])
    #if(index>0):
    #	word_list.append('bigram_word:'+sent[index-1]+"----"+sent[index+1])
    if (index < (len(sent) - 2)):
        word_list.append('unigram_word:' + sent[index + 2])
        word_list.append('bigram_word:' + sent[index + 1] + "----" + sent[index + 2])

    #Bigrams

    #Trigrams

    return word_list

def push_pos(pos_sent,index):
 pos_list=list()

 if(index>1):
        pos_list.append('unigram_pos:'+pos_sent[index-2])
        pos_list.append('bigram_pos:'+pos_sent[index-2]+'-'+pos_sent[index-2])
        pos_list.append('trigram_pos:'+pos_sent[index-2]+'-'+pos_sent[index-2]+'-'+pos_sent[index])
 if(index>0):
        pos_list.append('unigram_pos:'+pos_sent[index-1])
        pos_list.append('bigram_pos:'+pos_sent[index-1]+'-'+pos_sent[index])
 pos_list.append('unigram_pos:'+pos_sent[index])

 return pos_list

def push_ca(word):
#  print word[0]
  return 'ca:'+word[0]

def push_cz(word):
#  print word[len(word)-1]
  return 'cz:'+word[len(word)-1]

def push_length(word):
  return 'len:'+str(len(word))

def push_prefix(word):
   prefix_list=list()
   l=len(word)
   if (l>2):
        prefix_list.append('prefix:'+word[0:2])
   if (l>3):
        prefix_list.append('prefix:'+word[0:3])
   if (l>4):
        prefix_list.append('prefix:'+word[0:4])
#   print prefix_list
   return prefix_list

def push_suffix(word):
   suffix_list=list()
   l=len(word)
   if (l>4):
        suffix_list.append('suffix:'+word[l-4:l])
   if (l>3):
        suffix_list.append('suffix:'+word[l-3:l])
   if (l>2):
        suffix_list.append('suffix:'+word[l-2:l])
#   print suffix_list
   return suffix_list

def push_SA(word):
  # La parola inizia con una maiuscola
  pattern_SA = re.compile(r'^[A-ZÇÑÁÉÍÓÚÀÈÌÒÙÄËÏÖÜ].*$')
  if pattern_SA.match(word):
#       print 'MAIUSCOLA'+word+' '
        return 'SA'

def push_Sa(word):
  # La parola inizia con una minuscola
  pattern_Sa = re.compile(r'^[a-zçñáéíóúàèìòùäëïöü].*$')
  if pattern_Sa.match(word):
#       print 'MAIUSCOLA'+word+' '
        return 'Sa'

def push_CA(word):
  #La parola contiene una maiuscola
  pattern_CA = re.compile(r'^.+[A-ZÇÑÁÉÍÓÚÀÈÌÒÙÄËÏÖÜ].*$')
  if pattern_CA.match(word):
#       print 'MAIUSCOLA'+word+' '
        return 'CA'

def push_CAA(word):
  #La parola contiene piu' di una lettera maiuscola
  pattern_CAA = re.compile(r'^.*[A-ZÇÑÁÉÍÓÚÀÈÌÒÙÄËÏÖÜ].*[A-ZÇÑÁÉÍÓÚÀÈÌÒÙÄËÏÖÜ].*$')
  if pattern_CAA.match(word):
#       print 'MAIUSCOLA'+word+' '
        return 'CAA'

def push_AA(word):
  #Le lettere della parola sono tutte maiuscole
  pattern_AA = re.compile(r'^[A-ZÇÑÁÉÍÓÚÀÈÌÒÙÄËÏÖÜ]+$')
  if pattern_AA.match(word):
#       print 'MAIUSCOLA'+word+' '
        return 'AA'

def push_aa(word):
  #Le lettere della parola sono tutte minuscole
  pattern_aa = re.compile(r'^[a-zçñáéíóúàèìòùäëïöü]+$')
  if pattern_aa.match(word):
#       print 'MAIUSCOLA'+word+' '
        return 'aa'

def push_SN(word):
  #La parola inizia con un numero
  pattern_SN = re.compile(r'^[0-9].*$')
  if pattern_SN.match(word):
#       print 'MAIUSCOLA'+word+' '
        return 'SN'

def push_CN(word):
 #La parola contiene un numero
  pattern_CN = re.compile(r'^.*[0-9].*$')
  if pattern_CN.match(word):
#       print 'MAIUSCOLA'+word+' '
        return 'CN'

def push_CP(word):
 #La parola contiene un punto
  pattern_CP = re.compile(r'^.*[\.].*$')
  if pattern_CP.match(word):
#       print 'MAIUSCOLA'+word+' '
        return 'CP'

def push_CC(word):
# La parola contiene una virgola
  pattern_CC = re.compile(r'^.*[\,].*$')
  if pattern_CC.match(word):
#       print 'MAIUSCOLA'+word+' '
        return 'CC'

def push_MW(word):
# La parola contiene un trattino -
  pattern_MW = re.compile(r'^.*[\-].*$')
  if pattern_MW.match(word):
#       print 'MAIUSCOLA'+word+' '
        return 'MW'


def trova_barriera_sinistra(pos_sent, barriera, endpoit):
    #Trova la features a barriera a sinistra
    # Se trovo la barriera ma non trovo nessuna PoS nel mezzo restituisco l'insieme vuoto

    # param
    # param1 Elenco Part of Speech delle parole che precedono la end_point
    # param2 Barriera (PoS) da iricercare nelle primo pramentro

    #print 'La barriera e\' %s'%barriera
    #  print "CALCOLO BARRIERA"
    # End Point
    pattern_barriera = re.compile(r'^' + barriera + '$')
    out = 'setBarriera_sinistra(' + barriera + '-' + endpoit + ')={}'
    out_list = list()
    out_tmp = list()
    #for index, obj in enumerate(pos_sent.reverse()):
    pos_sent.reverse()
    no_barrier = 0
    for item in pos_sent:
        if pattern_barriera.match(item):
            #        print 'Ho trovato la barriera %s'%barriera
            #       print 'OUT:= ', out
            return out
            no_barrier = 1
            break
        else:
            #out += '%s ' %item
            #Crea il valore della feature a barriera
            #L'insieme delta
            out_list.append(item)
            out_tmp = sorted(set(out_list))
            out = 'setBarriera_sinistra(' + barriera + '-' + endpoit + ')={' + ",".join(out_tmp) + '}'

    if not no_barrier:
        #print 'Non ho trovato la barriera %s'%barriera
        #print 'OUT:= ', out
        return out


def push_barriera(pos_sent, index, barrier_pairs):
    delta = ''
    delta_list = list()

    #print  barrier_pairs

    # barrier_pairs=list()
    # (endpoint, target)
    # barrier_pairs.append(("PRP","NN"));
    # barrier_pairs.append(("IN","NN"));
    #barrier_pairs.append(("VB","NN"));
    #barrier_pairs.append(("DT","NN"));
    #barrier_pairs.append(("JJ","NNP"));
    #barrier_pairs.append(("NN","NNP"));
    #barrier_pairs.append(("DT","NNP"));
    #barrier_pairs.append(("IN","NNP"));
    #barrier_pairs.append(("PRP","VB"));
    #barrier_pairs.append(("NN","VB"));

    #print 'BARRIERA_POS '+pos_sent[index]
    pos_pre = pos_sent[0:index]
    pos_post1 = pos_sent[0:index + 2]

    for pair in barrier_pairs:
        pattern = re.compile(r'^' + pair[1] + '$')
        if pattern.match(pos_sent[index]):
            #print sent[index]+' '+pos_sent[index]
            delta_list.append(trova_barriera_sinistra(pos_pre, pair[0], pair[1]))
        #                delta_list.append(trova_barriera_sindes(pos_post1,pair[0],pair[1]))

    return delta_list



def estrai_features(sent, pos_sent, i, features, features_count, num_sen, bf_pairs, id_sen,fh):
    #Estrai le features dalla frase data in input

    #param1 Frase
    #param2 Elenco delle PoS della frase
    #param3 indice ultima feature inserita
    #param4 Elenco di tutte le features calcolate

    #  print 'Calcolo Features'

    delta = ''
    new_feature = 0
    out_feat = ''
    value_word = ''
    #value_features='UNKNOWN'
    value_features = ''
    for index, object in enumerate(sent):

        features_list = list()

        print(num_sen+'----'+sent[index]+':'+pos_sent[index] ,end=" ",file=fh)

        #Estrazione features a barriera
        features_list.extend(push_barriera(pos_sent, index, bf_pairs))
        features_list = sorted(set(features_list))
	
	#Estrazione features  prefissi e suffissi
        features_list.extend(push_prefix(sent[index]))
        features_list.extend(push_suffix(sent[index]))

       	#Inserisco le features word
       	features_list.extend(push_word(sent,index))

        #Inserisco le features pos
        features_list.extend(push_pos(pos_sent,index))

        value_features=push_ca(sent[index])
        if value_features:
        	features_list.append(value_features)
        value_features=''

        value_features=push_cz(sent[index])
        if value_features:
        	features_list.append(value_features)
        value_features=''
    
        value_length=push_length(sent[index])
        features_list.append(value_length)
        #Estrazione features lessicalizate
        #Extracting lexicalized features
        value_features=push_SA(sent[index])
        if value_features:
                features_list.append(value_features)
        value_features=''

       	value_features=push_Sa(sent[index])
        if value_features:
                features_list.append(value_features)
        value_features=''

        value_features=push_CA(sent[index])
        if value_features:
                features_list.append(value_features)
        value_features=''

        value_features=push_CAA(sent[index])
        if value_features:
                features_list.append(value_features)
        value_features=''

        value_features=push_AA(sent[index])
        if value_features:
                features_list.append(value_features)
        value_features=''

        value_features=push_aa(sent[index])
        if value_features:
                features_list.append(value_features)
        value_features=''

        value_features=push_SN(sent[index])
        if value_features:
                features_list.append(value_features)
        value_features=''  	

        value_length=push_length(sent[index])
        features_list.append(value_length)
        #Estrazione features lessicalizate
        #Extracting lexicalized features
        value_features=push_SA(sent[index])
        if value_features:
                features_list.append(value_features)
        value_features=''

       	value_features=push_CN(sent[index])
        if value_features:
                features_list.append(value_features)
        value_features=''

        value_features=push_CP(sent[index])
        if value_features:
                features_list.append(value_features)
        value_features=''

        value_features=push_CC(sent[index])
        if value_features:
                features_list.append(value_features)
        value_features=''

        value_features=push_MW(sent[index])
        if value_features:
                features_list.append(value_features)
        value_features=''
        
        #Wordnet
        hyper_list=aggiungi_features_wordnet(sent[index])
        if len(hyper_list)>0:
               	features_list.extend(hyper_list)

        for item in features_list:
            	new_feature = esiste_feature(item, features, i)
            	i = aggiungi_features(new_feature, features, item, i, features_count,fh)
        print("",file=fh)
        features_list = list()

    features_string = ''
    # print 'FEATURES:=',features
    # print 'FEATURES_COUNT:=',features_count
    # print 'LUNGHEZZA FEATURES_COUNT:=',len(features_count)
    # for k,v in features.iteritems():
    #     features_string +='%s:1 '%k
    #  print 'FEATURES_STRING:=',features_string
    #print '.:. '
    #file_feature_count.close()
    return i


def featureExtractionprocess(text_tokenPos, bf_file,fh):
	file1 = open(text_tokenPos, 'r')
# AC: sostituito find con rfind per gestire nomi di files che contengono il trattino
	num_abstract=text_tokenPos[text_tokenPos.rfind('/')+1:text_tokenPos.find('.')]
#	print(num_abstract)
	file_bf = open(bf_file, 'r')
	
	#Pattern per catturare i caratteri di fine frase.
	pattern_END = re.compile(r'\>+END OF SENTENCE*')
	features = dict()
	features_count = list()
	sentence = list()
	pos_sen = list()
	id_sen = list()
	hyper_list = dict()
	i = 1
	bf_pairs = []
	for line_BF in file_bf:
		line_BF = line_BF.strip()
		# print line_BF
		line_BF = re.sub(r'\(', '', line_BF)
		line_BF = re.sub(r'\)', '', line_BF)
		li_BF = line_BF.split(',')
		#print li_BF
		bf_pairs.append((li_BF[0], li_BF[1]))
#	pprint(bf_pairs)

	conutId=1	
	for line in file1.readlines():
		line = line.strip()
	#	print(line)
		if line:
			li = line.split()
			try:
				sentence.append(li[0])
				pos_sen.append(li[1])
#				id_sen.append(li[2])
			except:
				continue
		if re.match(pattern_END,line):
#			print ('FINE FRASE')
        #                       print '\n'
			#print('*************************** FRASE '+li[0]+' *********************************')

#			print(id_sen)
#			print(len(id_sen))
#			print(len(sentence))
#			print(len(pos_sen))
			
        #delta rappresenta il valore della featurea barriera
			num_last_feature = estrai_features(sentence[:-1], pos_sen[:-1], i, features, features_count, num_abstract, bf_pairs,id_sen[:-1],fh)
			i = num_last_feature;
        #                       num_last_feature=estrai_features_wn(sentence, pos_sen,i,features,features_count)
        #                       i=num_last_feature;
        #       print hyper_list
        #       print line+':PUNTO'

        #       push_word(sentence)

        #reset
			sentence = list()
			pos_sen = list()
			id_sen = list()
	file1.close()


if __name__ == '__main__':
    if not len(sys.argv) > 1:
        print('Hai dimenticato il file che contiene le frasi di input taggate e il file che contiene le coppie di barriere (endpoint, trigger)')
        exit(1)

    text_tokenPos = sys.argv[1]
    bf_file = sys.argv[2]  #il file contiene le coppie di barriere (endpoint, trigger)a
    fh = open( sys.argv[3],"w")
    featureExtractionprocess(text_tokenPos, bf_file,fh)
    fh.close();