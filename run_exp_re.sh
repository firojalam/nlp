#! /bin/bash


## Run Roberto's data preprocessing system to generate *.content.restored.iob2 and *.dner.output.pubtator files to inputDir

inputDir= # dir *.content.restored.iob2 and *.dner.output.pubtator files
goldAnnotationFile=
outputFile=
tokForm=
dirNLP=
java -classpath $dirNLP/NLP.jar nlp.cdr.dataprepare.PreprocessorSent -i $inputDir -c $dirNLP/config.txt -g $goldAnnotationFile -o $outputFile -t $tokForm

#This will generate SVM_light formatted file to run experiments with SVM light for sentence level classifier
