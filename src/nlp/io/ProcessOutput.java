/*
 * This work by Firoj Alam is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License.
 * Permissions beyond the scope of this license may be available by sending an email to firojalam@gmail.com.
 * http://creativecommons.org/licenses/by-nc/4.0/deed.en_US
 * 
 */


package nlp.io;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import nlp.cdr.dataanalysis.Document;
import nlp.utils.Write2File;


/**
 *
 * @author Firoj Alam
 */
public class ProcessOutput {

    public void processTitleAndAbstract(LinkedHashMap<String, Document> dataDict, String outputFile) {
        Write2File wrt = new Write2File();
        wrt.openFile(outputFile);
        StringBuilder text = null;
        for (Map.Entry<String, Document> entrySet : dataDict.entrySet()) {
            String docID = entrySet.getKey();
            Document document = entrySet.getValue();
            text = new StringBuilder();
            ArrayList<String> sent= document.getTitleSentList();
            for (int i = 0; i < sent.size(); i++) {
                String str = sent.get(i);
                text.append(str).append(" ");
            }
            wrt.write2File(docID+"|t|"+text.toString().trim()+"\n");
            sent = document.getDocumentSentList();
            text = new StringBuilder();
            for (int i = 0; i <sent.size(); i++) {
                String str = sent.get(i);
                text.append(str).append(" ");                                
            }
            wrt.write2File(docID+"|a|"+text.toString().trim()+"\n\n");
        }
        
        wrt.closeFile();
    }

    
}
