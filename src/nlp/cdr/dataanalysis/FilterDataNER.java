/*
 * This work by Firoj Alam is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License.
 * Permissions beyond the scope of this license may be available by sending an email to firojalam@gmail.com.
 * http://creativecommons.org/licenses/by-nc/4.0/deed.en_US
 * 
 */


package nlp.cdr.dataanalysis;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import static nlp.cdr.dataanalysis.Pipeline.duration;
import nlp.io.ProcessOutput;
import nlp.utils.LibraryPath;
import nlp.utils.Write2File;

/**
 *
 * @author Firoj Alam
 */
public class FilterDataNER {
    
    private void toWord2VecSent(LinkedHashMap<String, Document> dataDict, String outputFile) {
        Write2File wrt = new Write2File();
        wrt.openFile(outputFile);
        StringBuilder text = null;
        for (Map.Entry<String, Document> entrySet : dataDict.entrySet()) {
            String docID = entrySet.getKey();
            Document doc = entrySet.getValue();
            ArrayList<String> titleList = doc.getTitleSentList();
            ArrayList<String> absList = doc.getDocumentSentList();
            text = new StringBuilder();
            for (String sent : titleList) {
                text.append(docID).append("\t").append(sent).append("\n");
            }
            for (String sent : absList) {
                text.append(docID).append("\t").append(sent).append("\n");
            }
            wrt.write2File(text.toString()+"\n");
        }
        wrt.closeFile();
    }
    /**
     * 
     * @param args 
     */
    public static void main(String[] args){
        Date d1 = new Date();
        System.out.println("System started at: " + d1);

        //String inFileName = args[0];
        //String outFileName = args[1];
        String path="/Users/firojalam/Study_PhD_projects/CDR/data/";
        String inFileName = path+"CDR_Training/CDR_TrainingSet.PubTator.txt";
//        String inFileName = path+"CDR_Dev/CDR_TrainingSet.PubTator.txt";
        String outputFile = path+"data_title_abs_w2v.txt";
        
        DataReader dataReader = new DataReader();
        LinkedHashMap<String, Document> dataDict = dataReader.readData(inFileName);
//        ProcessOutput processOutput = new ProcessOutput();
//        processOutput.processTitleAndAbstract(dataDict, outputFile);
        FilterDataNER filterDataNER = new FilterDataNER();
        filterDataNER.toWord2VecSent(dataDict,outputFile);
        Date d2 = new Date();
        duration(d1,d2);
    }  


}
