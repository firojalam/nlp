/*
 * This work by Firoj Alam is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License.
 * Permissions beyond the scope of this license may be available by sending an email to firojalam@gmail.com.
 * http://creativecommons.org/licenses/by-nc/4.0/deed.en_US
 * 
 */


package nlp.cdr.dataanalysis;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.serializers.MapSerializer;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import static nlp.cdr.dataanalysis.Pipeline.duration;


/**
 *
 * @author Firoj Alam
 */
public class REDict {
    private static final Logger logger = Logger.getLogger(REDict.class.getName());
    
    /**
     * Reads data and design a ArrayList object containing chemical disease
     * pair.
     *
     * @param dataDict
     * @return HashMap object
     */
    public HashSet<String> makeDictionary(LinkedHashMap<String, Document> dataDict) {
        HashSet<String> reList = new HashSet<>();
        try {
            for (Map.Entry<String, Document> entrySet : dataDict.entrySet()) {
                String docID = entrySet.getKey();
                Document document = entrySet.getValue();
                ArrayList<ArrayList<String>> cidList = document.getCIDList();
                for (int i = 0; i < cidList.size(); i++) {
                    ArrayList<String> cid = cidList.get(i);
                    String chemicalDisease = cid.get(0) + "-" + cid.get(1);
                    reList.add(chemicalDisease);
                }
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Something is wrong with parsing the file, please check.", ex);
        }
        return reList;
    }

    /**
     *
     * @param reList
     * @param outputFile
     */
    private void writeObject(HashSet<String> reList, String outputFile) {
        try {
            RandomAccessFile raf = new RandomAccessFile(outputFile, "rw");
            Output output = new Output(new FileOutputStream(raf.getFD()));
            Kryo kryo = new Kryo();
            //MapSerializer serializer = new MapSerializer();
            kryo.register(HashSet.class);
            kryo.writeObject(output, reList);
            output.close();
        } catch (FileNotFoundException ex) {
            logger.log(Level.SEVERE, "File is not available while writing ArrayList Object file.", ex);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Something is wrong while writing object file.", ex);
        }
    }
    
    public HashSet<String> readDictionaryObject(String fileName) {
        HashSet<String> dataREDict = new HashSet<>();
        try {
            RandomAccessFile raf = new RandomAccessFile(fileName, "r");
            FileInputStream fileIn = new FileInputStream(raf.getFD());
            Input input = new Input(fileIn);
          //Read
            Kryo kryo = new Kryo();
            //MapSerializer serializer = new MapSerializer();
            kryo.register(HashSet.class);
            dataREDict = kryo.readObject(input, HashSet.class);
            input.close();
            fileIn.close();
            fileIn.close();
        } catch (FileNotFoundException ex) {
            logger.log(Level.SEVERE, "File is not available, please check the stack-trace.", ex);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Something is wrong with parsing the file, please check.", ex);
        }
        return dataREDict;

    }    
    
    public static void main(String[] args) {
        // ChemicalDiseaseDict dict = null; 
        try {
            Date d1 = new Date();
            String fileName = "/home/firoj.alam/Study_PhD_projects/CDR/data/CDR_Training/CDR_TrainingSet.PubTator.txt";
            String outputFile = "/home/firoj.alam/Study_PhD_projects/CDR/data/train_re.ser";
            //String fileName = "/home/firoj.alam/Study_PhD_projects/CDR/data/CDR_Tr_Dev.txt";
            //String outputFile = "/home/firoj.alam/Study_PhD_projects/CDR/data/train_dev_re.ser";
            DataReader dataReader = new DataReader();
            LinkedHashMap<String, Document> dataDict =dataReader.readData(fileName);
            
            REDict reDict = new REDict();
            HashSet<String> reList = reDict.makeDictionary(dataDict);
            reDict.writeObject(reList, outputFile);
            
            HashSet<String> reListR = reDict.readDictionaryObject(outputFile);
            System.out.println("Size "+reListR.size());
            Date d2 = new Date();
            duration(d1, d2);

        } catch (Exception ex) {
            Logger.getLogger(DictionaryReader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
}
