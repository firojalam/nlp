/*
 * This work by Firoj Alam is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License.
 * Permissions beyond the scope of this license may be available by sending an email to firojalam@gmail.com.
 * http://creativecommons.org/licenses/by-nc/4.0/deed.en_US
 * 
 */


package nlp.cdr.dataanalysis;

/**
 *
 * @author Firoj Alam
 */
public class Mention {
    private String docID = null;
    private String token = null;
    private String mention = null;    
    private String mentionID = null;        
    private int startIndex = 0;
    private int endIndex = 0;

    public Mention() {
    }

    public String getDocID() {
        return docID;
    }

    public void setDocID(String docID) {
        this.docID = docID;
    }

    
    
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMention() {
        return mention;
    }

    public void setMention(String mention) {
        this.mention = mention;
    }

    public String getMentionID() {
        return mentionID;
    }

    public void setMentionID(String mentionID) {
        this.mentionID = mentionID;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public void setEndIndex(int endIndex) {
        this.endIndex = endIndex;
    }

    @Override
    public String toString() {
        String arr[]=docID.split("_");        
        return arr[0] + "\t" + startIndex + "\t" + endIndex + "\t" + token + "\t" + mention + "\t" + mentionID;
    }
    
    
}
