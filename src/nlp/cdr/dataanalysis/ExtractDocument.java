/*
 * This work by Firoj Alam is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License.
 * Permissions beyond the scope of this license may be available by sending an email to firojalam@gmail.com.
 * http://creativecommons.org/licenses/by-nc/4.0/deed.en_US
 * 
 */


package nlp.cdr.dataanalysis;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import nlp.utils.Write2File;

/**
 *
 * @author Firoj Alam
 */
public class ExtractDocument {
    private static final Logger logger = Logger.getLogger(ExtractDocument.class.getName());
    public void readData(String fileName, String dirName) { 
        try {
            String dirPath = new File(dirName).getAbsolutePath();
            BufferedReader fileRead = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
            String str = "";
            String docID ="";
            StringBuilder text = new StringBuilder();
            while ((str = fileRead.readLine()) != null) {
                str = str.trim();                
                if(str.isEmpty()){
                    Write2File wrt = new Write2File();
                    wrt.openFile(dirPath+"/"+docID+".ner.output");
                    wrt.write2File(text.toString().trim()+"\n");
                    wrt.closeFile();
                    text = new StringBuilder();
                }else {
                    //String arr[] = null;
                    if(str.contains("|t|")){
                        String arr[] = str.split("\\|");
                        docID = arr[0];                    
                    }else if(str.contains("CID")){
                        continue;
                    }else{
                        String arr[] = str.split("\t");
                        docID = arr[0];                                        
                    }
                    text.append(str).append("\n");
                }
                
            }//end read file
            fileRead.close();
        } catch (FileNotFoundException ex) {
            logger.log(Level.SEVERE, "Please check your file. It is not available", ex);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Something is wrong. Please follow stack-trace....", ex);
        }
    } 
    
    
    public static void main(String args[]){
        //String inFileName = args[0];
//        String path = args[1];        
        String path = "/Users/firojalam/Study_PhD_projects/CDR/data/DevelopmentSet_7_15_15/";
        String inFileName = "/Users/firojalam/Study_PhD_projects/CDR/data/CDR_Dev/CDR_DevelopmentSet.PubTator.txt";
        ExtractDocument extractDocument = new ExtractDocument();
        extractDocument.readData(inFileName, path);
    }
}
