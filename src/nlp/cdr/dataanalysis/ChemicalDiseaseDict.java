/*
 * This work by Firoj Alam is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License.
 * Permissions beyond the scope of this license may be available by sending an email to firojalam@gmail.com.
 * http://creativecommons.org/licenses/by-nc/4.0/deed.en_US
 * 
 */


package nlp.cdr.dataanalysis;

import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author Firoj Alam
 */
public class ChemicalDiseaseDict implements Serializable {

    private static final long serialVersionUID = -4851637177439350772L;
    private Map<String, ChemicalDisease> map;

    public ChemicalDiseaseDict(HashMap<String, ChemicalDisease> mymap) throws Exception {
        this.map=mymap;
    }

    public Map<String, ChemicalDisease> getMap() {
        return map;
    }
}
