/*
 * This work by Firoj Alam is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License.
 * Permissions beyond the scope of this license may be available by sending an email to firojalam@gmail.com.
 * http://creativecommons.org/licenses/by-nc/4.0/deed.en_US
 * 
 */
package nlp.cdr.dataanalysis;

import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import nlp.utils.Stat;

import nlp.utils.Write2File;

/**
 *
 * @author Firoj Alam
 */
public class Statistics {

    private HashSet<String> uniqueMentionListSameSent = new HashSet<>();
    private ArrayList<String> mentionList = new ArrayList<>();
    private HashSet<String> uniqueMentionList = new HashSet<>();
    private HashSet<String> uniqueSentList = new HashSet<>();
    private LinkedHashSet<String> uniqueSentMentionList = new LinkedHashSet<>();

    public void processStat(LinkedHashMap<String, Document> dataDict, HashMap<String, ChemicalDisease> chemicalDiseaseDict, String outFileName) {
        Write2File wrt = new Write2File();
        wrt.openFile(outFileName);
        double average = 0.0, percentage = 0.0;
        String str = "";
        DecimalFormat decimalFormat = new DecimalFormat("##.##");
        /**
         * problem a toke avg at the sent level
         */
        average = this.tokenAvgDoc(dataDict, "title");
        wrt.write2File("Token average for complete title\t" + decimalFormat.format(average) + "\n");
        average = this.tokenAvgDoc(dataDict, "abstract");
        wrt.write2File("Token average for complete abstract\t" + decimalFormat.format(average) + "\n");
        average = this.tokenAvgDoc(dataDict, "document");
        wrt.write2File("Token average for complete document\t" + decimalFormat.format(average) + "\n");        
        /**
         * problem a toke avg at the sentence level
         */
        average = this.tokenAvg(dataDict, "title");
        wrt.write2File("Token average at the sentence level: title\t" + decimalFormat.format(average) + "\n");
        average = this.tokenAvg(dataDict, "abstract");
        wrt.write2File("Token average at the sentence level: abstract\t" + decimalFormat.format(average) + "\n");

        /**
         * problem b
         */
        average = this.absSentAvg(dataDict);
        wrt.write2File("Sent average: abstract\t" + decimalFormat.format(average) + "\n");
        /**
         * problem c: average number of relation in a document
         */
        average = this.relationAvg(dataDict);
        wrt.write2File("Relation (average)\t" + decimalFormat.format(average) + "\n");
        /**
         * problem d
         */
        double mentionAvg = this.mentionAvgOfRelation(dataDict, "Chemical");
        wrt.write2File("Mention (chemicals) average:\t" + decimalFormat.format(mentionAvg) + "\n");
        mentionAvg = this.mentionAvgOfRelation(dataDict, "Disease");
        wrt.write2File("Mention (diseases) average:\t" + decimalFormat.format(mentionAvg) + "\n");
        
        /**
         * problem e
         */
        //percentage of mentions involved in a relation that co-occur in the same sentence
        //TODO
        str = this.mentionAvgOfRelationSameSent(dataDict);
        wrt.write2File(str + "\n");
        //str = this.getNumberOfMentionSent(dataDict);
        wrt.write2File(str + "\n");
        /**
         * problem f: gazetters coverage
         */
        str = this.gazetteersCoverage2(dataDict, chemicalDiseaseDict);
        wrt.write2File(str + "\n");
        wrt.closeFile();
    }

    /**
     *
     * @param dataDict
     * @param str
     * @return
     */
    private double tokenAvg(LinkedHashMap<String, Document> dataDict, String str) {
        double tokenAvg = 0.0;
        Stat stat = new Stat();
        ArrayList<Integer> tokenCountList = new ArrayList<>();
        if (str.equals("title")) {
            for (Map.Entry<String, Document> entrySet : dataDict.entrySet()) {
                String key = entrySet.getKey();
                Document document = entrySet.getValue();
                for (int i = 0; i < document.getTitleSentList().size(); i++) {
                    String txt = document.getTitleSentList().get(i);
                    int numberOfToken = getTokenList(txt);
                    tokenCountList.add(numberOfToken);
                }
            }
            tokenAvg = stat.calMeanInt(tokenCountList);
            return tokenAvg;
        } else if (str.equals("abstract")) {
            for (Map.Entry<String, Document> entrySet : dataDict.entrySet()) {
                String key = entrySet.getKey();
                Document document = entrySet.getValue();
                for (int i = 0; i < document.getDocumentSentList().size(); i++) {
                    String txt = document.getDocumentSentList().get(i);
                    int numberOfToken = getTokenList(txt);
                    tokenCountList.add(numberOfToken);
                }
            }
            tokenAvg = stat.calMeanInt(tokenCountList);
            return tokenAvg;
        }
        return tokenAvg;
    }

    /**
     *
     * @param dataDict
     * @param str
     * @return
     */
    private double tokenAvgDoc(LinkedHashMap<String, Document> dataDict, String str) {
        double tokenAvg = 0.0;
        Stat stat = new Stat();
        ArrayList<Integer> tokenCountList = new ArrayList<>();
        if (str.equals("title")) {
            for (Map.Entry<String, Document> entrySet : dataDict.entrySet()) {
                String key = entrySet.getKey();
                Document document = entrySet.getValue();
                int numberOfToken = 0;
                for (int i = 0; i < document.getTitleSentList().size(); i++) {
                    String txt = document.getTitleSentList().get(i);
                    numberOfToken = numberOfToken + getTokenList(txt);
                }
                tokenCountList.add(numberOfToken);
            }
            tokenAvg = stat.calMeanInt(tokenCountList);
            return tokenAvg;
        } else if (str.equals("abstract")) {
            for (Map.Entry<String, Document> entrySet : dataDict.entrySet()) {
                String key = entrySet.getKey();
                Document document = entrySet.getValue();
                int numberOfToken = 0;
                for (int i = 0; i < document.getDocumentSentList().size(); i++) {
                    String absTxt = document.getDocumentSentList().get(i);
                    numberOfToken = numberOfToken + getTokenList(absTxt);
                }
                tokenCountList.add(numberOfToken);
            }
            tokenAvg = stat.calMeanInt(tokenCountList);
            return tokenAvg;
        } else if (str.equals("document")) {
            for (Map.Entry<String, Document> entrySet : dataDict.entrySet()) {
                String key = entrySet.getKey();
                Document document = entrySet.getValue();
                int numberOfToken = 0;
                for (int i = 0; i < document.getTitleSentList().size(); i++) {
                    String titleTxt = document.getTitleSentList().get(i);
                    numberOfToken = numberOfToken + getTokenList(titleTxt);
                }
                for (int i = 0; i < document.getDocumentSentList().size(); i++) {
                    String absTxt = document.getDocumentSentList().get(i);
                    numberOfToken = numberOfToken + getTokenList(absTxt);
                }
                tokenCountList.add(numberOfToken);
            }
            tokenAvg = stat.calMeanInt(tokenCountList);
            return tokenAvg;
        }
        return tokenAvg;
    }

    /**
     *
     * @param txt
     * @return
     */
    private int getTokenList(String txt) {
        int numberOfToken = 0;
        ArrayList<Integer> tokenCountList = new ArrayList<>();
        ArrayList<String> tokenList = new ArrayList<>();
        Properties props = new Properties();
        props.setProperty("annotators", "tokenize,ssplit");
        StanfordCoreNLP pipeline = new StanfordCoreNLP(props);

        Annotation annotation = new Annotation(txt);
        pipeline.annotate(annotation);
        List<CoreMap> sentences = annotation.get(SentencesAnnotation.class);
        for (CoreMap sentence : sentences) {
            for (CoreLabel token : sentence.get(TokensAnnotation.class)) {
                String word = token.get(TextAnnotation.class);
                tokenList.add(word);
            }
        }
        numberOfToken = tokenList.size();
        return numberOfToken;
    }

    /**
     *
     * @param dataDict
     * @param str
     * @return
     */
    private double absSentAvg(LinkedHashMap<String, Document> dataDict) {
        double tokenAvg = 0.0;
        Stat stat = new Stat();
        ArrayList<Integer> countList = new ArrayList<>();

        for (Map.Entry<String, Document> entrySet : dataDict.entrySet()) {
            String key = entrySet.getKey();
            Document document = entrySet.getValue();
            countList.add(document.getDocumentSentList().size());
        }
        tokenAvg = stat.calMeanInt(countList);
        return tokenAvg;
    }

    private double relationAvg(LinkedHashMap<String, Document> dataDict) {
        double average = 0.0;
        Stat stat = new Stat();
        ArrayList<Integer> countList = new ArrayList<>();
        for (Map.Entry<String, Document> entrySet : dataDict.entrySet()) {
            String key = entrySet.getKey();
            Document document = entrySet.getValue();
            countList.add(document.getCIDList().size());
        }
        average = stat.calMeanInt(countList);
        return average;
    }

    private double mentionAvgOfRelation(LinkedHashMap<String, Document> dataDict, String str) {
        double average = 0.0;
        Stat stat = new Stat();
        ArrayList<Integer> countList = new ArrayList<>();
        for (Map.Entry<String, Document> entrySet : dataDict.entrySet()) {
            String key = entrySet.getKey();
            Document document = entrySet.getValue();
            for (int i = 0; i < document.getCIDList().size(); i++) {
                ArrayList<String> cidList = document.getCIDList().get(i);
                int mentionCount = getNumberOfMention(cidList, document.getMentionList(), str);
                //ArrayList<Integer> mentionCount
                countList.add(mentionCount);
            }
        }
        System.out.println("Number of mention: "+stat.calSumInt(countList));
        average = stat.calMeanInt(countList);

        return average;
    }

    //ArrayList<Integer>
    private int getNumberOfMention(ArrayList<String> cidList, ArrayList<Mention> mentionList, String str) {
        //ArrayList<Integer> mentionCount = new ArrayList<>();
        int numberOfMention = 0;
        for (int i = 0; i < cidList.size(); i++) {
            String cid = cidList.get(i);

            for (int j = 0; j < mentionList.size(); j++) {
                Mention mention = mentionList.get(j);
                if (mention.getMention().equalsIgnoreCase(str) && mention.getMentionID().equals(cid)) {
                    numberOfMention++;
                }
            }
            //mentionCount.add(numberOfMention);
        }
        return numberOfMention;
    }

    private String mentionAvgOfRelationSameSent(LinkedHashMap<String, Document> dataDict) {
        double percentage = 0.0;
        Stat stat = new Stat();
        ArrayList<Integer> countList = new ArrayList<>();
        ArrayList<Integer> cidCountList = new ArrayList<>();
        HashSet<String> cidUniqListDoc = null;
        HashSet<String> cidUniqList = new HashSet<>();
        ArrayList<String> sentList = null;
        Write2File wrtMention = new Write2File();
        int TPandFP=0, TP=0;
        //wrtMention.openFile("/Users/firojalam/Study_PhD_projects/CDR/data/mentioninfo.txt");
        for (Map.Entry<String, Document> entrySet : dataDict.entrySet()) {
            String key = entrySet.getKey();
            Document document = entrySet.getValue();
            cidCountList.add(document.getCIDList().size());
            cidUniqListDoc = new HashSet<>();
            for (int i = 0; i < document.getCIDList().size(); i++) {
                String cid = document.getCIDList().get(i).get(0) + "-" + document.getCIDList().get(i).get(1);
                cidUniqListDoc.add(cid);
                cidUniqList.add(cid);
            }
            
            //cidUniqList.addAll(cidUniqListDoc);
            sentList = new ArrayList<>();
            sentList.addAll(document.getTitleSentList());
            sentList.addAll(document.getDocumentSentList());
            ArrayList<Sentence> sentListUpdated = getUpdatedSentList(sentList);
            //create candidate pair
            ArrayList<Mention> mentionList = document.getMentionList();
            LinkedHashSet<String> chemicalList = getCIDList(mentionList, "Chemical");
            LinkedHashSet<String> diseaseList = getCIDList(mentionList, "Disease");
            for (Iterator<String> iterator = chemicalList.iterator(); iterator.hasNext();) {
                String chemicalID = iterator.next();
                ArrayList<Mention> chemicalMentionList = getMentionList(mentionList, chemicalID);                                
                for (Iterator<String> iterator1 = diseaseList.iterator(); iterator1.hasNext();) {
                    String diseaseID = iterator1.next();
                    String candidatePair=chemicalID+"-"+diseaseID;
                    ArrayList<Mention> diseaseMentionList = getMentionList(mentionList, diseaseID);
                    boolean positiveFound = getCandidateMentionSent(chemicalMentionList,diseaseMentionList,sentListUpdated);
                    //int positiveFound = getCandidateMentionSentCount(chemicalMentionList,diseaseMentionList,sentListUpdated);
                    //TPandFP = TPandFP+positiveFound;
                    //boolean positiveFound = 
                    if(positiveFound){
                        //System.out.println(candidatePair);
                        TPandFP++;
                        if(cidUniqListDoc.contains(candidatePair)){
                            TP++;
                        }
                    }
                }
                
            }
//            for (int i = 0; i < document.getCIDList().size(); i++) {
//                ArrayList<String> cidListGold = document.getCIDList().get(i);
//                int numOfSentInARelation = getNumberOfMentionSent(cidListGold, sentListUpdated, document, wrtMention);
//                TP = TP +numOfSentInARelation;
//                countList.add(numOfSentInARelation);
//            }
        }//for each document
        
        String str = "";
        double precision = (double)TP/(double)TPandFP;
        double recall = (double)TP/(double)cidUniqList.size();
        double f1 = 2*(precision*recall)/(precision+recall);        
        str = str +"TP:"+TP+"\n";
        str = str +"TP+FP:"+TPandFP+"\n";
        str = str +"TP+FN:"+cidUniqList.size()+"\n";
        str = str +"Precision:"+precision+"\n";
        str = str +"Recall: "+recall+"\n";
        str = str +"F1: "+f1+"\n";
        
        //int numOfSent = stat.calSumInt(countList);
        
        int sumCIDCount = stat.calSumInt(cidCountList);
//        percentage = (double) numOfSent / (double) sumCIDCount;
        str = str + "Total number of relation: " + sumCIDCount + "\n";
        str = str + "Total number of UNIQUE relation: " + cidUniqList.size() + "\n";
        str = str + "Total number of UNIQUE mention pair: " + this.uniqueMentionList.size() + "\n";
        str = str + "Total number of sentence(same) containing mention pair(s): " + this.uniqueSentList.size() + "\n";
        str = str + "Total number of UNIQUE mention pair in the same sent: " + this.uniqueMentionListSameSent.size() + "\n";

        System.out.println(str);
//        for (Iterator<String> iterator = uniqueSentMentionList.iterator(); iterator.hasNext();) {
//            String txt = iterator.next();
//            wrtMention.write2File(txt + "\n");
//        }
//        wrtMention.closeFile();
        return str;
    }

    private boolean getCandidateMentionSent(ArrayList<Mention> chemicalMentionList, ArrayList<Mention> diseaseMentionList, ArrayList<Sentence> sentListUpdated) {
        boolean sameSentFound = false;
        for (int i = 0; i < sentListUpdated.size(); i++) {
            Sentence sent = sentListUpdated.get(i);
            String mentionStr = "";
            for (int j = 0; j < chemicalMentionList.size(); j++) {
                Mention mention = chemicalMentionList.get(j);
                if (mention.getStartIndex() >= sent.getStartIndex() && mention.getEndIndex() <= sent.getEndIndex()) {
                    for (int k = 0; k < diseaseMentionList.size(); k++) {
                        Mention mentionCID1 = diseaseMentionList.get(k);
                        mentionStr = mention.getToken().trim() + "#" + mentionCID1.getToken().trim();
                        this.uniqueMentionList.add(mentionStr);
                        if (mentionCID1.getStartIndex() >= sent.getStartIndex() && mentionCID1.getEndIndex() <= sent.getEndIndex()) {
                            //System.out.print(mentionStr+"\t"+sent.getText()+"\t");
                            return true;
                            
                        }
                    }//disease loop
                }
            }//chemical loop
        }
        return sameSentFound;
    }
    
    private int getCandidateMentionSentCount(ArrayList<Mention> chemicalMentionList, ArrayList<Mention> diseaseMentionList, ArrayList<Sentence> sentListUpdated) {
        boolean sameSentFound = false;
        int candidateMentionPair = 0;
        for (int i = 0; i < sentListUpdated.size(); i++) {
            Sentence sent = sentListUpdated.get(i);
            String mentionStr = "";
            for (int j = 0; j < chemicalMentionList.size(); j++) {
                Mention mention = chemicalMentionList.get(j);
                if (mention.getStartIndex() >= sent.getStartIndex() && mention.getEndIndex() <= sent.getEndIndex()) {
                    for (int k = 0; k < diseaseMentionList.size(); k++) {
                        Mention mentionCID1 = diseaseMentionList.get(k);
                        mentionStr = mention.getToken().trim() + "#" + mentionCID1.getToken().trim();
                        this.uniqueMentionList.add(mentionStr);
                        if (mentionCID1.getStartIndex() >= sent.getStartIndex() && mentionCID1.getEndIndex() <= sent.getEndIndex()) {
                            System.out.print(mentionStr+"\t"+sent.getText()+"\t");
                            //return true;
                            candidateMentionPair++;
                            
                        }
                    }//disease loop
                }
            }//chemical loop
        }
        return candidateMentionPair;
    }    
    private int getNumberOfMentionSent(ArrayList<String> cidList, ArrayList<Sentence> sentListUpdated, Document document, Write2File wrtMention) {
        String cid0 = cidList.get(0);
        String cid1 = cidList.get(1);
        int sameSentFound = 0;
        int numberOfMention = 0;
        ArrayList<Mention> mentionList = document.getMentionList();
        //HashSet<String> cid0TokenList = getTokenList(mentionList, cid0);
        //HashSet<String> cid1TokenList = getTokenList(mentionList, cid1);
        ArrayList<Mention> cid0mentionList = getMentionList(mentionList, cid0);
        ArrayList<Mention> cid1mentionList = getMentionList(mentionList, cid1);
        StringBuilder txt = null;
        for (int i = 0; i < sentListUpdated.size(); i++) {

            Sentence sent = sentListUpdated.get(i);

            boolean cid0Found = false, cid1Found = false;
            String mentionStr = "";
            for (int j = 0; j < cid0mentionList.size(); j++) {
                Mention mention = cid0mentionList.get(j);
                if (mention.getStartIndex() >= sent.getStartIndex() && mention.getEndIndex() <= sent.getEndIndex()) {
                    cid0Found = true;
                    for (int k = 0; k < cid1mentionList.size(); k++) {
                        Mention mentionCID1 = cid1mentionList.get(k);
                        mentionStr = mention.getToken().trim() + "#" + mentionCID1.getToken().trim();
                        //this.mentionList.add(mentionStr);
                        this.uniqueMentionList.add(mentionStr);
                        if (mentionCID1.getStartIndex() >= sent.getStartIndex() && mentionCID1.getEndIndex() <= sent.getEndIndex()) {
                            cid1Found = true;
                            //mentionStr = mention.getToken().trim()+"-"+mentionCID1.getToken().trim();
                            //System.out.println("Mention: "+mentionStr);
                            //txt =new StringBuilder();
                            //txt.append().append(mentionStr).append("\n");
                            String str = sent.getText().trim() + "\t" + mentionStr;
                            this.uniqueSentList.add(sent.getText().trim());
                            this.uniqueSentMentionList.add(str);
                            //wrtMention.write2File(txt.toString());                            
                            this.uniqueMentionListSameSent.add(mentionStr);
                            numberOfMention++;
                        }
                    }
                }//cid0
            }
//            if (cid0Found && cid1Found) {
//                sameSentFound++;
//            }            
        }
        System.out.println("Doc " + document.getDocumentID() + " Mention: " + numberOfMention + " Same sent: " + sameSentFound);
        //System.out.println("Number of mention: "+numberOfMention);
        return numberOfMention;
    }

    private ArrayList<Sentence> getUpdatedSentList(ArrayList<String> sentList) {
        ArrayList<Sentence> sentListUpdated = new ArrayList<>();
        Sentence sentObj = null;
        int startIndex = 0;
        int endIndex = 0;
        for (int i = 0; i < sentList.size(); i++) {
            String sent = sentList.get(i);
            sentObj = new Sentence();
            sentObj.setText(sent);
            sentObj.setStartIndex(startIndex);
            endIndex = endIndex + sent.length();
            sentObj.setEndIndex(endIndex);
            startIndex = endIndex + 1;
            sentListUpdated.add(sentObj);
        }
        return sentListUpdated;
    }

    private ArrayList<Mention> getMentionList(ArrayList<Mention> mentionList, String cid0) {
        ArrayList<Mention> tokenList = new ArrayList<>();
        for (int j = 0; j < mentionList.size(); j++) {
            Mention mention = mentionList.get(j);
            if (mention.getMentionID().equals(cid0)) {
                tokenList.add(mention);
            }
        }
        return tokenList;
    }

    private HashSet<String> getTokenList(ArrayList<Mention> mentionList, String cid0) {
        HashSet<String> tokenList = new HashSet<>();
        for (int j = 0; j < mentionList.size(); j++) {
            Mention mention = mentionList.get(j);
            if (mention.getMentionID().equals(cid0)) {
                tokenList.add(mention.getToken());
            }
        }
        return tokenList;
    }

    private double gazetteersCoverage(LinkedHashMap<String, Document> dataDict, HashMap<String, ChemicalDisease> chemicalDiseaseDict) {
        double percentage = 0.0;
        Stat stat = new Stat();
        int cidFound = 0;
        int sum = 0;
        HashSet<String> cidDict = new HashSet<>();
        ArrayList<Integer> cidCount = new ArrayList<>();
        for (Map.Entry<String, Document> entrySet : dataDict.entrySet()) {
            String id = entrySet.getKey();
            Document document = entrySet.getValue();
            for (int i = 0; i < document.getCIDList().size(); i++) {
                ArrayList<String> cidList = document.getCIDList().get(i);
                //cidCount.add(cidList.size());                
                for (int j = 0; j < cidList.size(); j++) {
                    String cid = cidList.get(j);
                    cidDict.add(cid);
                }//traverse cidList realtion
            }//traverse cidList of documnets
        }//traverse documents

        for (Iterator<String> iterator = cidDict.iterator(); iterator.hasNext();) {
            String cid = iterator.next();
            if (chemicalDiseaseDict.containsKey(cid)) {
                cidFound++;
            } else {
                System.err.println("Not found: " + cid);
            }
        }

        //sum = stat.calSumInt(cidCount);
        sum = cidDict.size();
        percentage = (double) cidFound / (double) sum;
        return percentage;
    }

    private String gazetteersCoverage2(LinkedHashMap<String, Document> dataDict, HashMap<String, ChemicalDisease> chemicalDiseaseDict) {
        double percentage = 0.0;
        Stat stat = new Stat();
        DecimalFormat decimalFormat = new DecimalFormat("##.##");
        int cidFound = 0;
        int sum = 0, positive=0,positiveGold=0,positiveGZ=0;
        ArrayList<String> cidDictGold = new ArrayList<>();
        ArrayList<Integer> cidCount = new ArrayList<>();
        for (Map.Entry<String, Document> entrySet : dataDict.entrySet()) {
            String id = entrySet.getKey();
            Document document = entrySet.getValue();
            for (int i = 0; i < document.getCIDList().size(); i++) {
                ArrayList<String> cidList = document.getCIDList().get(i);
                cidDictGold.add(cidList.get(0).trim() + "-" + cidList.get(1).trim());
            }//traverse cidList of documnets
        }//traverse documents

        HashSet<String> chemicalDiseaseList = new HashSet<>();
        for (Map.Entry<String, Document> entrySet : dataDict.entrySet()) {
            String key = entrySet.getKey();
            Document document = entrySet.getValue();
            ArrayList<Mention> mentionList1 = document.getMentionList();
            LinkedHashSet<String> chemicalList = getCIDList(mentionList1, "Chemical");
            LinkedHashSet<String> diseaseList = getCIDList(mentionList1, "Disease");
            ArrayList<String> cidDictGoldTmp = new ArrayList<>();
            for (int i = 0; i < document.getCIDList().size(); i++) {
                ArrayList<String> cidList = document.getCIDList().get(i);
                cidDictGoldTmp.add(cidList.get(0).trim() + "-" + cidList.get(1).trim());
            }//traverse cidList of documnets
            
            for (Iterator<String> iterator = chemicalList.iterator(); iterator.hasNext();) {
                String chemical = iterator.next();
                for (Iterator<String> iterator1 = diseaseList.iterator(); iterator1.hasNext();) {
                    String disease = iterator1.next();
                    String chemicalDisease= chemical.trim()+"-"+disease.trim();
                    chemicalDiseaseList.add(chemicalDisease);
                    if(chemicalDiseaseDict.containsKey(chemicalDisease) && cidDictGoldTmp.contains(chemicalDisease)){
                        positive++; //TP
                    }
                    if( cidDictGoldTmp.contains(chemicalDisease)){
                        positiveGold++; 
                    }
                    if(chemicalDiseaseDict.containsKey(chemicalDisease)){
                        positiveGZ++; ///TP+FP
                    }                    
                }                
            }            
        }
        
        String str = "";
        str = str +"Total number of entity pairs in the dataset (UNIQUE): "+chemicalDiseaseList.size()+"\n";
        str = str +"Number of relation found in gold and Gazetteers (Positive=>TP): "+positive+"\n";
        str = str +"NUmber of relation found in Gazetteers (PositiveGZ=>TP+FP): "+positiveGZ+"\n";
        str = str +"NUmber of relation => TP+FN: "+cidDictGold.size()+"\n";
        double precision = (double)positive/(double)positiveGZ;
        double recall = (double)positive/(double)cidDictGold.size();
        double f1 = 2*(precision*recall)/(precision+recall);
        str = str +"Precision:"+precision+"\n";
        str = str +"Recall: "+recall+"\n";
        str = str +"F1: "+f1+"\n";
        
//        int goldInGz=0;
//        for (Iterator<String> iterator = cidDictGold.iterator(); iterator.hasNext();) {
//            String chemicalDisease = iterator.next();
//            if (chemicalDiseaseDict.containsKey(chemicalDisease)) {
//                goldInGz++;
//            }
//            if (!chemicalDiseaseList.contains(chemicalDisease)) {
//                System.out.println("not found:\t"+chemicalDisease);
//            }            
//        }
        //str = str +"Total number of relation (gold) "+cidDictGold.size()+"\n";
        //str = str +"Gold relations found in Gazetteers "+goldInGz+"\n";
        
        System.out.println(str);
        return str;
    }

    private LinkedHashSet<String> getCIDList(ArrayList<Mention> mentionList, String cid) {
        LinkedHashSet<String> cidList = new LinkedHashSet<>();
        
        for (int i = 0; i < mentionList.size(); i++) {
            Mention mention = mentionList.get(i);
            if (mention.getMention().equals(cid)) {
//                if(mention.getMentionID().contains("|")){
//                
//                }
                cidList.add(mention.getMentionID());
            }                        
        }
        return cidList;
    }

}
