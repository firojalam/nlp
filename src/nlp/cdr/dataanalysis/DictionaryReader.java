/*
 * This work by Firoj Alam is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License.
 * Permissions beyond the scope of this license may be available by sending an email to firojalam@gmail.com.
 * http://creativecommons.org/licenses/by-nc/4.0/deed.en_US
 * 
 */
package nlp.cdr.dataanalysis;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.serializers.MapSerializer;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import static nlp.cdr.dataanalysis.Pipeline.duration;

/**
 *
 * @author Firoj Alam
 */
public class DictionaryReader {
    private static final Logger logger = Logger.getLogger(DictionaryReader.class.getName());
    /**
     * Reads CTD-chemical-disease database from tsv file and design a HashMap object.
     * It contains chemical-disease id as key and <ChemicalDisease> object as Value
     * @param fileName name of the CTD-chemical-disease file name
     * @return HashMap object
     */
    public HashMap<String, ChemicalDisease> readDictionary(String fileName) {
        HashMap<String, ChemicalDisease> chemicalDiseaseDict = new HashMap<>();
        try {
            BufferedReader fileRead = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
            String str = "";

            ChemicalDisease chemicalDisease = null;
            while ((str = fileRead.readLine()) != null) {
                // if(str.startsWith("#")) continue; //as the file is large, this checking is expensive. removed all comments externally
                String arr[] = str.split("\t");
                String chemicalName = arr[0];
                String chemicalID = arr[1];
                //String CasRN = arr[2];
                String diseaseName = arr[3];
                String diseaseID = arr[4].trim().replace("MESH:", "").replace("OMIM:", "");
                chemicalDisease = new ChemicalDisease();
                chemicalDisease.setChemicalID(chemicalID);
                chemicalDisease.setChemicalName(chemicalName);
                chemicalDisease.setDiseaseID(diseaseID);
                chemicalDisease.setDiseaseName(diseaseName);
                chemicalDiseaseDict.put(chemicalID + "-" + diseaseID, chemicalDisease);
            }//end read file
            fileRead.close();
        } catch (FileNotFoundException ex) {            
            logger.log(Level.SEVERE, "File is not available, please check the stack-trace.", ex);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Something is wrong with parsing the file, please check.", ex);
        }
        return chemicalDiseaseDict;

    }

    /**
     * Reads the java object file, which has been stored as HashMap object.
     * It contains chemical-disease id as key and <ChemicalDisease> object as Value
     * @param fileName name of the CTD-chemical-disease file name
     * @return HashMap object
     */
    public HashMap<String, ChemicalDisease> readDictionaryObject(String fileName) {
        HashMap<String, ChemicalDisease> chemicalDiseaseDict = new HashMap<>();
        try {
            RandomAccessFile raf = new RandomAccessFile(fileName, "r");
            FileInputStream fileIn = new FileInputStream(raf.getFD());
            Input input = new Input(fileIn);
          //Read
            Kryo kryo = new Kryo();
            MapSerializer serializer = new MapSerializer();
            kryo.register(ChemicalDisease.class);
            kryo.register(HashMap.class, serializer);
            chemicalDiseaseDict = kryo.readObject(input, HashMap.class);
            input.close();
            fileIn.close();
            fileIn.close();
        } catch (FileNotFoundException ex) {
            logger.log(Level.SEVERE, "File is not available, please check the stack-trace.", ex);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Something is wrong with parsing the file, please check.", ex);
        }
        return chemicalDiseaseDict;

    }

    public static void main(String[] args) {
        // ChemicalDiseaseDict dict = null; 
        try {
            Date d1 = new Date();
            String fileName = "/home/firoj.alam/Study_PhD_projects/CDR/data/CTD_chemicals_diseases_v1.tsv";
            String outputFile = "/home/firoj.alam/Study_PhD_projects/CDR/data/CTD_chemicals_diseases.ser";
            DictionaryReader dictionaryReader = new DictionaryReader();
          //  /HashMap<String, ChemicalDisease> chemicalDiseaseDict = dictionaryReader.readDictionary(fileName);
            HashMap<String, ChemicalDisease> chemicalDiseaseDict = dictionaryReader.readDictionaryObject(outputFile);
            System.out.println(chemicalDiseaseDict.size());;
            /**
             * The following code is used to read the data and write to the javaobject file
             * Used Kryo serialization
             */
//            RandomAccessFile raf = new RandomAccessFile(outputFile, "rw");
//            Output output = new Output(new FileOutputStream(raf.getFD()));
//            Kryo kryo = new Kryo();
//            MapSerializer serializer = new MapSerializer();
//            kryo.register(ChemicalDisease.class);
//            kryo.register(HashMap.class, serializer);
//            kryo.writeObject(output, chemicalDiseaseDict);
//            output.close();

            Date d2 = new Date();
            duration(d1, d2);

        } catch (Exception ex) {
            Logger.getLogger(DictionaryReader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
