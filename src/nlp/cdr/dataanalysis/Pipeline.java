/*
 * This work by Firoj Alam is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License.
 * Permissions beyond the scope of this license may be available by sending an email to firojalam@gmail.com.
 * http://creativecommons.org/licenses/by-nc/4.0/deed.en_US
 * 
 */


package nlp.cdr.dataanalysis;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import nlp.io.ProcessOutput;
import nlp.utils.LibraryPath;

/**
 *
 * @author Firoj Alam
 */
public class Pipeline {

    //HashMap<String, ChemicalDisease> chemicalDiseaseDict =
    /**
     * Calculates execution time of the whole system
     * @param d1
     * @param d2 
     */
    public static void duration(Date d1, Date d2){
        String timeTaken = "";
        try {
            long diff = d2.getTime() - d1.getTime();
            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);
            timeTaken = diffDays + " days, " + diffHours + " hours, " + diffMinutes + " minutes, " + diffSeconds + " seconds.";
        } catch (Exception ex) {
            System.out.println("Time cal problem.");
        }
        System.out.println("Time taken: " + timeTaken);
        System.out.println("Done..");    
    }
    
    
    /**
     * 
     * @param args 
     */
    public static void main(String[] args){
        Date d1 = new Date();
        System.out.println("System started at: " + d1);

        //String inFileName = args[0];
        //String outFileName = args[1];
        String path="/Users/firojalam/Study_PhD_projects/CDR/data/";
        //String inFileName = path+"CDR_Training/CDR_TrainingSet.txt";
        //String inFileName = path+"bug.txt";
        String inFileName = path+"CDR_Data/CDR.Corpus/CDR_TestSet.PubTator.txt";
        String outFileName = path+"data_stat_test.txt";
        String outputFile = path+"data_title_abs_test.txt";
        String dictionaryFileName = path+"CTD_chemicals_diseases_v1.tsv";
        
        String libPath = new LibraryPath().getLibPath();
        DataReader dataReader = new DataReader();
        LinkedHashMap<String, Document> dataDict = dataReader.readData(inFileName);
        ProcessOutput processOutput = new ProcessOutput();
        processOutput.processTitleAndAbstract(dataDict, outputFile);
        DictionaryReader readDict = new DictionaryReader();
        HashMap<String, ChemicalDisease> chemicalDiseaseDict = readDict.readDictionary(dictionaryFileName);
        //HashMap<String, ChemicalDisease> chemicalDiseaseDict = null;        
        Statistics statistics = new Statistics();
        statistics.processStat(dataDict,chemicalDiseaseDict,outFileName);
//        Pipeline pipeline = new Pipeline();        

        
        Date d2 = new Date();
        duration(d1,d2);
    }    
    
    
}
