/*
 * This work by Firoj Alam is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License.
 * Permissions beyond the scope of this license may be available by sending an email to firojalam@gmail.com.
 * http://creativecommons.org/licenses/by-nc/4.0/deed.en_US
 * 
 */


package nlp.cdr.dataanalysis;

import edu.stanford.nlp.ling.CoreAnnotations;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.util.CoreMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
/**
 *
 * @author Firoj Alam
 */
public class DataReader {
    private static final Logger logger = Logger.getLogger(nlp.cdr.re.DataReader.class.getName());
    StanfordCoreNLP pipeline =null;
    Properties props = new Properties();
    Annotation annotation = null;
    public DataReader() {
        props.setProperty("annotators", "tokenize,ssplit");
        pipeline = new StanfordCoreNLP(props);
        
    }

    //private LinkedHashMap<String, String> dataDict = new LinkedHashMap<>();
    public LinkedHashMap<String, Document> readData(String fileName) { 
        LinkedHashMap<String, Document> dataDict = new LinkedHashMap<>();
        try {
            BufferedReader fileRead = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
            String str = "";
            Document document = new Document();
            ArrayList<String> tmpList = null;
            ArrayList<ArrayList<String>> cidList = null;
            ArrayList<Mention> mentionList = null;
            String docID ="";
            while ((str = fileRead.readLine()) != null) {
                str = str.trim();                
                if(str.isEmpty()){
                    dataDict.put(docID, document);
                    document = new Document();
                    cidList = new ArrayList<>();
                    mentionList = new ArrayList<>();
                    document.setMentionList(mentionList);
                    document.setCIDList(cidList);
                }else if(str.contains("|t|")){
                    String arr[] = str.split("\\|");
                    docID = arr[0];
                    document.setDocumentID(arr[0]);
                    ArrayList<String> sentList = this.getSentList(arr[2]);
                    document.setTitleSentList(sentList);
                }else if(str.contains("|a|")){
                    String arr[] = str.split("\\|");
                    ArrayList<String> sentList = this.getSentList(arr[2]);
                    document.setDocumentSentList(sentList);
                }else if(str.contains("CID")){
                    String arr[] = str.split("\\s+");
                    tmpList = new ArrayList<>();
                    tmpList.add(arr[2]);
                    tmpList.add(arr[3]);
                    document.getCIDList().add(tmpList);
                }else{
                    String arr[] = str.split("\t");
                    String id = arr[0];
                    int startIndex = Integer.parseInt(arr[1]);
                    int endIndex = Integer.parseInt(arr[2]);
                    String token = arr[3];
                    String mention = arr[4];
                    String mentionID =arr[5];
                    if (mentionID.contains("|")) {
                        String arr2[] = mentionID.split("\\|");
                        Mention mentionObj = new Mention();
                        mentionObj.setDocID(id + "_" + startIndex + "_" + endIndex);
                        mentionObj.setStartIndex(startIndex);
                        mentionObj.setEndIndex(endIndex);
                        mentionObj.setToken(token);
                        mentionObj.setMention(mention);
                        mentionObj.setMentionID(arr2[0]);
                        document.getMentionList().add(mentionObj);
                        document.setDocumentID(id);

                        mentionObj = new Mention();
                        mentionObj.setDocID(id + "_" + startIndex + "_" + endIndex);
                        mentionObj.setStartIndex(startIndex);
                        mentionObj.setEndIndex(endIndex);
                        mentionObj.setToken(token);
                        mentionObj.setMention(mention);
                        mentionObj.setMentionID(arr2[1]);
                        document.getMentionList().add(mentionObj);
                        document.setDocumentID(id);
                    } else {
                        Mention mentionObj = new Mention();
                        mentionObj.setDocID(id + "_" + startIndex + "_" + endIndex);
                        mentionObj.setStartIndex(startIndex);
                        mentionObj.setEndIndex(endIndex);
                        mentionObj.setToken(token);
                        mentionObj.setMention(mention);
                        mentionObj.setMentionID(mentionID);
                        document.getMentionList().add(mentionObj);
                        document.setDocumentID(id);
                    }
                }
                
            }//end read file
            fileRead.close();
        } catch (FileNotFoundException ex) {
            System.out.println("Please check your file.");
            Logger.getLogger(DataReader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(DataReader.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dataDict;
    }   

    /**
     * Read a data-stream 
     * @param stream pubtator formatted output as string
     * @return Document
     */
    public LinkedHashMap<String, Document> readDataStream(String stream) { 
        LinkedHashMap<String, Document> dataDict = new LinkedHashMap<>();                                
        try {
            //BufferedReader fileRead = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
            StringTokenizer fileRead = new StringTokenizer(stream,"\n");
            String str = "";
            Document document = new Document();
            ArrayList<String> tmpList = null;
            ArrayList<ArrayList<String>> cidList = null;
            ArrayList<Mention> mentionList = null;
            String docID ="";
            while ((str = fileRead.nextToken()) != null) {
                str = str.trim();                
                if(str.isEmpty()){
                    dataDict.put(docID, document);
                    document = new Document();
                    cidList = new ArrayList<>();
                    mentionList = new ArrayList<>();
                    document.setMentionList(mentionList);
                    document.setCIDList(cidList);
                }else if(str.contains("|t|")){
                    String arr[] = str.split("\\|");
                    docID = arr[0];
                    document.setDocumentID(arr[0]);
                    ArrayList<String> sentList = this.getSentList(arr[2]);
                    document.setTitleSentList(sentList);
                }else if(str.contains("|a|")){
                    String arr[] = str.split("\\|");
                    ArrayList<String> sentList = this.getSentList(arr[2]);
                    document.setDocumentSentList(sentList);
                }else if(str.contains("CID")){
                    String arr[] = str.split("\\s+");
//                    if(arr.length!=3){
//                        System.err.println(" relation ids "+str);
//                    }
                    tmpList = new ArrayList<>();
                    tmpList.add(arr[2]);
                    tmpList.add(arr[3]);
                    document.getCIDList().add(tmpList);
                }else{
                    String arr[] = str.split("\t");
                    String id = arr[0];
                    int startIndex = Integer.parseInt(arr[1]);
                    int endIndex = Integer.parseInt(arr[2]);
                    String token = arr[3];
                    String mention = arr[4];
                    String mentionID =arr[5];
                    if (mentionID.contains("|")) {
                        String arr2[] = mentionID.split("\\|");
                        Mention mentionObj = new Mention();
                        mentionObj.setDocID(id + "_" + startIndex + "_" + endIndex);
                        mentionObj.setStartIndex(startIndex);
                        mentionObj.setEndIndex(endIndex);
                        mentionObj.setToken(token);
                        mentionObj.setMention(mention);
                        mentionObj.setMentionID(arr2[0]);
                        document.getMentionList().add(mentionObj);
                        document.setDocumentID(id);

                        mentionObj = new Mention();
                        mentionObj.setDocID(id + "_" + startIndex + "_" + endIndex);
                        mentionObj.setStartIndex(startIndex);
                        mentionObj.setEndIndex(endIndex);
                        mentionObj.setToken(token);
                        mentionObj.setMention(mention);
                        mentionObj.setMentionID(arr2[1]);
                        document.getMentionList().add(mentionObj);
                        document.setDocumentID(id);
                    } else {
                        Mention mentionObj = new Mention();
                        mentionObj.setDocID(id + "_" + startIndex + "_" + endIndex);
                        mentionObj.setStartIndex(startIndex);
                        mentionObj.setEndIndex(endIndex);
                        mentionObj.setToken(token);
                        mentionObj.setMention(mention);
                        mentionObj.setMentionID(mentionID);
                        document.getMentionList().add(mentionObj);
                        document.setDocumentID(id);
                    }
                }
                
            }//end read file
            //fileRead.close();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Problem in reading stream...", ex);
        }
        return dataDict;
    }       
    /**
     * 
     * @param txt
     * @return 
     */
    private ArrayList<String> getSentList(String txt){
        ArrayList<String> sentList = new ArrayList<>();
        annotation = new Annotation(txt);        
        pipeline.annotate(annotation);
        List<CoreMap> sentences = annotation.get(CoreAnnotations.SentencesAnnotation.class);
        for (CoreMap sentence : sentences) {
            sentList.add(sentence.toString());
        }        
        return sentList;
    }
    
}
