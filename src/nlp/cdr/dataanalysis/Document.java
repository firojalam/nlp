/*
 * This work by Firoj Alam is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License.
 * Permissions beyond the scope of this license may be available by sending an email to firojalam@gmail.com.
 * http://creativecommons.org/licenses/by-nc/4.0/deed.en_US
 * 
 */


package nlp.cdr.dataanalysis;

import java.util.ArrayList;

/**
 *
 * @author Firoj Alam
 */
public class Document {

    private String documentID = null;
    private ArrayList<String> titleSentList = new ArrayList<>();
    private ArrayList<String> documentSentList = new ArrayList<>();
    private ArrayList<ArrayList<String>> CIDList = new ArrayList<>();
    private ArrayList<Mention> mentionList = new ArrayList<>();
    
    public Document() {
    }

    public ArrayList<String> getTitleSentList() {
        return titleSentList;
    }

    public void setTitleSentList(ArrayList<String> titleSentList) {
        this.titleSentList = titleSentList;
    }

    public ArrayList<String> getDocumentSentList() {
        return documentSentList;
    }

    public void setDocumentSentList(ArrayList<String> articleSentList) {
        this.documentSentList = articleSentList;
    }

    public ArrayList<ArrayList<String>> getCIDList() {
        return CIDList;
    }

    public void setCIDList(ArrayList<ArrayList<String>> CIDList) {
        this.CIDList = CIDList;
    }

    public ArrayList<Mention> getMentionList() {
        return mentionList;
    }

    public void setMentionList(ArrayList<Mention> mentionList) {
        this.mentionList = mentionList;
    }

    public String getDocumentID() {
        return documentID;
    }

    public void setDocumentID(String documentID) {
        this.documentID = documentID;
    }

    
    
}
