/*
 * This work by Firoj Alam is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License.
 * Permissions beyond the scope of this license may be available by sending an email to firojalam@gmail.com.
 * http://creativecommons.org/licenses/by-nc/4.0/deed.en_US
 * 
 */
package nlp.cdr.dataprepare;

import nlp.cdr.re.*;
import nlp.cdr.dataanalysis.Mention;
import nlp.cdr.dataanalysis.Document;
import edu.stanford.nlp.ling.CoreAnnotations;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 *
 * @author Firoj Alam
 */
public class DataReaderSent {

    private static final Logger logger = Logger.getLogger(DataReaderSent.class.getName());
    private LinkedHashMap<String, String> contentFileDict = new LinkedHashMap<>();
    private LinkedHashMap<String, String> nerFileDict = new LinkedHashMap<>();
    private LinkedHashMap<String, ArrayList<Token>> contentTokenDict = new LinkedHashMap<>();
    private LinkedHashMap<String, Document> documentREDict = new LinkedHashMap<>();
    private String sourceDir = null;
    private Properties props = new Properties();
    private StanfordCoreNLP pipeline = null;

    public DataReaderSent() {
//        props.setProperty("annotators", "tokenize,ssplit");
//        pipeline = new StanfordCoreNLP(props);
    }

    public DataReaderSent(StanfordCoreNLP nlpPipeline) {
        //props.setProperty("annotators", "tokenize,ssplit");
        //pipeline = new StanfordCoreNLP(props);
        this.pipeline = nlpPipeline;
    }

    /**
     *
     * @param dirName
     */
    public void readDirClassification(String dirName) {
        try {
            String dirPath = new File(dirName).getAbsolutePath();
            this.setSourceDir(dirPath);
            File folder = new File(dirName);
            File[] listOfFiles = folder.listFiles();
            
            String fileName = null;
            for (int i = 0; i < listOfFiles.length; i++) {
                try {
                    if (listOfFiles[i].isFile()) {
                        fileName = listOfFiles[i].getName();
                        String filePath = listOfFiles[i].getAbsolutePath();
                        String fileId = fileName.substring(0, fileName.indexOf("."));
                        if (fileName.endsWith(".content.restored.iob2")) {
                            this.contentFileDict.put(fileId, filePath);
                        } else if (fileName.endsWith(".dner.output.pubtator")) {
                            this.nerFileDict.put(fileId, filePath);
                        }
                    }
                } catch (Exception ex) {
                    logger.log(Level.SEVERE, "Some unwanted file, not in proper format.", ex);
                }
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Something is wrong. Please follow the stack-trace", ex);
        }
    }

    /**
     *
     * @param dirName
     * @return
     */
    public void readDir(String dirName) {
        try {
            String dirPath = new File(dirName).getAbsolutePath();
            this.setSourceDir(dirPath);
            File folder = new File(dirName);
            File[] listOfFiles = folder.listFiles();
            String fileName = null;
            for (int i = 0; i < listOfFiles.length; i++) {
                try {
                    if (listOfFiles[i].isFile()) {
                        fileName = listOfFiles[i].getName();
                        String filePath = listOfFiles[i].getAbsolutePath();
                        String fileId = fileName.substring(0, fileName.indexOf("."));
                        if (fileName.endsWith(".content.restored.iob2")) {
                            this.contentFileDict.put(fileId, filePath);
                        } else if (fileName.endsWith(".dner.output.pubtator")) {//
                            this.nerFileDict.put(fileId, filePath);
                        }
                    }
                } catch (Exception ex) {
                    logger.log(Level.SEVERE, "Some unwanted file, not in proper format.", ex);
                }
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Something is wrong. Please follow the stack-trace", ex);
        }
    }

    /**
     *
     * @param contentFileDict
     * @return
     */
    public LinkedHashMap<String, HashMap<Integer,ArrayList<Token>>> processToken(LinkedHashMap<String, String> contentFileDict) {
        LinkedHashMap<String, HashMap<Integer,ArrayList<Token>>> docTokenContentDict = new LinkedHashMap<>();
        for (Map.Entry<String, String> entrySet : contentFileDict.entrySet()) {
            String fileID = entrySet.getKey();
            String filePath = entrySet.getValue();
            HashMap<Integer,ArrayList<Token>> sentDict = this.readContent(filePath);
            docTokenContentDict.put(fileID, sentDict);
            //contentTokenDict.put(fileID, tokList);
        }
        return docTokenContentDict;
    }

    /**
     *
     * @param fileName
     * @return
     */
    private HashMap<Integer,ArrayList<Token>> readContent(String fileName) {
        HashMap<Integer,ArrayList<Token>> sentDict = new HashMap<>();
        ArrayList<Token> tokList = new ArrayList<>();
        try {
            BufferedReader fileRead = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
            String str = "";
            int tokID = 0;
            int sentID = 0;
            while ((str = fileRead.readLine()) != null) {
                str = str.trim();
                Token token = new Token();
                if (str.isEmpty()) {
                    token.setToken(">>>>>>>END OF SENTENCE");
                    token.setTokenPosition(null);
                    tokList.add(token);
                    sentDict.put(sentID, tokList);
                    sentID++;
                    tokList = new ArrayList<>();                    
                } else {
                    String arr[] = str.split("\t");
                    String tok = arr[0];
                    String charStartIndex = arr[2];
                    String charEndIndex = arr[3];
                    String lemma = arr[4];
                    String pos = arr[5];                   
                    String entityType = arr[6];
                    String entityID = arr[7];

                    tokID++;
                    token.setTokenPosition(tokID + "");
                    token.setCharStartIndex(charStartIndex);
                    token.setCharEndIndex(charEndIndex);
                    token.setEntityType(entityType);
                    token.setToken(tok);
                    token.setLemma(lemma);
                    token.setTokenPos(pos);
                    token.setEntityID(entityID);
                    tokList.add(token);
                }
            }//end read file
            sentDict.put(sentID, tokList);
            fileRead.close();
        } catch (FileNotFoundException ex) {
            logger.log(Level.SEVERE, "File is not available, please check.", ex);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Something is wrong here, please follw the stack-trace.", ex);
        }
        return sentDict;
    }

    public LinkedHashMap<String, ArrayList<Token>> getContentTokenDict() {
        return contentTokenDict;
    }

    
    public LinkedHashMap<String, Document> processNEContent(LinkedHashMap<String, String> nerFileDict) {
        for (Map.Entry<String, String> entrySet : nerFileDict.entrySet()) {
            String fileID = entrySet.getKey();
            String filePath = entrySet.getValue();
            Document doc = this.readData(filePath);
            documentREDict.put(fileID, doc);
        }
        return documentREDict;
    }

    /**
     *
     * @param fileName
     * @return
     */
    public Document readData(String fileName) {
        Document document = new Document();
        try {
            BufferedReader fileRead = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
            String str = "";
            ArrayList<String> tmpList = null;
            ArrayList<ArrayList<String>> cidList = new ArrayList<>();
            ArrayList<Mention> mentionList = new ArrayList<>();
            String docID = "";
            document.setMentionList(mentionList);
            document.setCIDList(cidList);

            while ((str = fileRead.readLine()) != null) {
                str = str.trim();

                if (str.isEmpty()) {
                    continue;
                } else if (str.contains("|t|")) {
                    String arr[] = str.split("\\|");
                    docID = arr[0];
                    document.setDocumentID(arr[0]);
                    ArrayList<String> sentList = this.getSentList(arr[2], pipeline);
                    document.setTitleSentList(sentList);
                } else if (str.contains("|a|")) {
                    String arr[] = str.split("\\|");
                    ArrayList<String> sentList = this.getSentList(arr[2], pipeline);
                    document.setDocumentSentList(sentList);
                } else if (str.contains("CID")) {
                    String arr[] = str.split("\\s+");
                    tmpList = new ArrayList<>();
                    tmpList.add(arr[2]);
                    tmpList.add(arr[3]);
                    document.getCIDList().add(tmpList);
                } else {
                    String arr[] = str.split("\t");
                    String id = arr[0];
                    int startIndex = Integer.parseInt(arr[1]);
                    int endIndex = Integer.parseInt(arr[2]);
                    String token = arr[3];
                    String mention = arr[4];
                    String mentionID = arr[5];
                    if (mentionID.contains("|")) {
                        String arr2[] = mentionID.split("\\|");
                        Mention mentionObj = new Mention();
                        mentionObj.setDocID(id + "_" + startIndex + "_" + endIndex);
                        mentionObj.setStartIndex(startIndex);
                        mentionObj.setEndIndex(endIndex);
                        mentionObj.setToken(token);
                        mentionObj.setMention(mention);
                        mentionObj.setMentionID(arr2[0]);
                        document.getMentionList().add(mentionObj);
                        document.setDocumentID(id);

                        mentionObj = new Mention();
                        mentionObj.setDocID(id + "_" + startIndex + "_" + endIndex);
                        mentionObj.setStartIndex(startIndex);
                        mentionObj.setEndIndex(endIndex);
                        mentionObj.setToken(token);
                        mentionObj.setMention(mention);
                        mentionObj.setMentionID(arr2[1]);
                        document.getMentionList().add(mentionObj);
                        document.setDocumentID(id);
                    } else {
                        Mention mentionObj = new Mention();
                        mentionObj.setDocID(id + "_" + startIndex + "_" + endIndex);
                        mentionObj.setStartIndex(startIndex);
                        mentionObj.setEndIndex(endIndex);
                        mentionObj.setToken(token);
                        mentionObj.setMention(mention);
                        mentionObj.setMentionID(mentionID);
                        document.getMentionList().add(mentionObj);
                        document.setDocumentID(id);
                    }
                }
            }//end read file
            fileRead.close();
        } catch (FileNotFoundException ex) {
            logger.log(Level.SEVERE, "File is not available, please check.", ex);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Something is wrong here, please follw the stack-trace.", ex);
        }
        return document;

    }

        /**
     *
     * @param contentFileDict
     * @return
     */
    public LinkedHashMap<String, ArrayList<Token>> processTokenDoc(LinkedHashMap<String, String> contentFileDict) {
        for (Map.Entry<String, String> entrySet : contentFileDict.entrySet()) {
            String fileID = entrySet.getKey();
            String filePath = entrySet.getValue();
            ArrayList<Token> tokList = this.readContentDoc(filePath);
            contentTokenDict.put(fileID, tokList);
        }
        return contentTokenDict;
    }

    /**
     *
     * @param fileName
     * @return
     */
    private ArrayList<Token> readContentDoc(String fileName) {

        ArrayList<Token> tokList = new ArrayList<>();
        try {
            BufferedReader fileRead = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
            String str = "";
            int tokID = 0;
            while ((str = fileRead.readLine()) != null) {
                str = str.trim();
                Token token = new Token();
                if (str.isEmpty()) {
                    token.setToken(">>>>>>>END OF SENTENCE");
                    token.setTokenPosition(null);
                    tokList.add(token);
                } else {

                    String arr[] = str.split("\t");
                    String tok = arr[0];
                    String charStartIndex = arr[2];
                    String charEndIndex = arr[3];
                    String lemma = arr[4];
                    String pos = arr[5];                   
                    String entityType = arr[6];
                    String entityID = arr[7];

                    tokID++;
                    token.setTokenPosition(tokID + "");
                    token.setCharStartIndex(charStartIndex);
                    token.setCharEndIndex(charEndIndex);
                    token.setEntityType(entityType);
                    token.setToken(tok);
                    token.setLemma(lemma);
                    token.setTokenPos(pos);
                    token.setEntityID(entityID);
                    tokList.add(token);
                }
            }//end read file
            fileRead.close();
        } catch (FileNotFoundException ex) {
            logger.log(Level.SEVERE, "File is not available, please check.", ex);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Something is wrong here, please follw the stack-trace.", ex);
        }
        return tokList;
    }

    /**
     *
     * @param fileName
     * @return
     */
    public LinkedHashMap<String, Document> readDataStream(String stream) {
        LinkedHashMap<String, Document> dataDict = new LinkedHashMap<>();
        Document document = new Document();
        try {
            //BufferedReader fileRead = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
            StringTokenizer fileRead = new StringTokenizer(stream,"\n");
            String str = "";
            ArrayList<String> tmpList = null;
            ArrayList<ArrayList<String>> cidList = new ArrayList<>();
            ArrayList<Mention> mentionList = new ArrayList<>();
            String docID = "";
            document.setMentionList(mentionList);
            document.setCIDList(cidList);

            while (fileRead.hasMoreTokens()) {
                str=fileRead.nextToken().trim();
                //str = str.trim();

                if (str.isEmpty()) {
                    continue;
                } else if (str.contains("|t|")) {
                    String arr[] = str.split("\\|");
                    docID = arr[0];
                    document.setDocumentID(arr[0]);
                    ArrayList<String> sentList = this.getSentList(arr[2], pipeline);
                    document.setTitleSentList(sentList);
                } else if (str.contains("|a|")) {
                    String arr[] = str.split("\\|");
                    ArrayList<String> sentList = this.getSentList(arr[2], pipeline);
                    document.setDocumentSentList(sentList);
                } else if (str.contains("CID")) {
                    String arr[] = str.split("\\s+");
                    tmpList = new ArrayList<>();
                    tmpList.add(arr[2]);
                    tmpList.add(arr[3]);
                    document.getCIDList().add(tmpList);
                } else {
                    String arr[] = str.split("\t");
                    String id = arr[0];
                    int startIndex = Integer.parseInt(arr[1]);
                    int endIndex = Integer.parseInt(arr[2]);
                    String token = arr[3];
                    String mention = arr[4];
                    String mentionID = arr[5];
                    if (mentionID.contains("|")) {
                        String arr2[] = mentionID.split("\\|");
                        Mention mentionObj = new Mention();
                        mentionObj.setDocID(id + "_" + startIndex + "_" + endIndex);
                        mentionObj.setStartIndex(startIndex);
                        mentionObj.setEndIndex(endIndex);
                        mentionObj.setToken(token);
                        mentionObj.setMention(mention);
                        mentionObj.setMentionID(arr2[0]);
                        document.getMentionList().add(mentionObj);
                        document.setDocumentID(id);

                        mentionObj = new Mention();
                        mentionObj.setDocID(id + "_" + startIndex + "_" + endIndex);
                        mentionObj.setStartIndex(startIndex);
                        mentionObj.setEndIndex(endIndex);
                        mentionObj.setToken(token);
                        mentionObj.setMention(mention);
                        mentionObj.setMentionID(arr2[1]);
                        document.getMentionList().add(mentionObj);
                        document.setDocumentID(id);
                    } else {
                        Mention mentionObj = new Mention();
                        mentionObj.setDocID(id + "_" + startIndex + "_" + endIndex);
                        mentionObj.setStartIndex(startIndex);
                        mentionObj.setEndIndex(endIndex);
                        mentionObj.setToken(token);
                        mentionObj.setMention(mention);
                        mentionObj.setMentionID(mentionID);
                        document.getMentionList().add(mentionObj);
                        document.setDocumentID(id);
                    }
                }
            }//end read stream
            dataDict.put(docID, document);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Problem in reading stream...", ex);
        }
        
        return dataDict;

    }    
    /**
     * 
     * @param fileName
     * @return 
     */
    public LinkedHashMap<String, Document> readGoldRE(String fileName) {
        LinkedHashMap<String, Document> dataDict = new LinkedHashMap<>();
        try {
            BufferedReader fileRead = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
            String str = "";
            Document document = new Document();
            ArrayList<String> tmpList = null;
            ArrayList<ArrayList<String>> cidList = null;
            ArrayList<Mention> mentionList = null;
            String docID = "";
            while ((str = fileRead.readLine()) != null) {
                str = str.trim();
                if (str.isEmpty()) {
                    dataDict.put(docID, document);
                    document = new Document();
                    cidList = new ArrayList<>();
                    mentionList = new ArrayList<>();
                    document.setMentionList(mentionList);
                    document.setCIDList(cidList);
                } else if (str.contains("|t|")) {
                    String arr[] = str.split("\\|");
                    docID = arr[0];
                    document.setDocumentID(arr[0]);
                    ArrayList<String> sentList = this.getSentList(arr[2], pipeline);
                    document.setTitleSentList(sentList);
                } else if (str.contains("|a|")) {
                    String arr[] = str.split("\\|");
                    ArrayList<String> sentList = this.getSentList(arr[2], pipeline);
                    document.setDocumentSentList(sentList);
                } else if (str.contains("CID")) {
                    String arr[] = str.split("\\s+");
//                    if(arr.length!=3){
//                        System.err.println(" relation ids "+str);
//                    }
                    tmpList = new ArrayList<>();
                    tmpList.add(arr[2]);
                    tmpList.add(arr[3]);
                    document.getCIDList().add(tmpList);
                } else {
                    String arr[] = str.split("\t");
                    String id = arr[0];
                    int startIndex = Integer.parseInt(arr[1]);
                    int endIndex = Integer.parseInt(arr[2]);
                    String token = arr[3];
                    String mention = arr[4];
                    String mentionID = arr[5];
                    if (mentionID.contains("|")) {
                        String arr2[] = mentionID.split("\\|");
                        Mention mentionObj = new Mention();
                        mentionObj.setDocID(id + "_" + startIndex + "_" + endIndex);
                        mentionObj.setStartIndex(startIndex);
                        mentionObj.setEndIndex(endIndex);
                        mentionObj.setToken(token);
                        mentionObj.setMention(mention);
                        mentionObj.setMentionID(arr2[0]);
                        document.getMentionList().add(mentionObj);
                        document.setDocumentID(id);

                        mentionObj = new Mention();
                        mentionObj.setDocID(id + "_" + startIndex + "_" + endIndex);
                        mentionObj.setStartIndex(startIndex);
                        mentionObj.setEndIndex(endIndex);
                        mentionObj.setToken(token);
                        mentionObj.setMention(mention);
                        mentionObj.setMentionID(arr2[1]);
                        document.getMentionList().add(mentionObj);
                        document.setDocumentID(id);
                    } else {
                        Mention mentionObj = new Mention();
                        mentionObj.setDocID(id + "_" + startIndex + "_" + endIndex);
                        mentionObj.setStartIndex(startIndex);
                        mentionObj.setEndIndex(endIndex);
                        mentionObj.setToken(token);
                        mentionObj.setMention(mention);
                        mentionObj.setMentionID(mentionID);
                        document.getMentionList().add(mentionObj);
                        document.setDocumentID(id);
                    }
                }

            }//end read file
            fileRead.close();
        } catch (FileNotFoundException ex) {
            logger.log(Level.SEVERE, "File is not available, please check.", ex);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Something is wrong here, please follw the stack-trace.", ex);
        }
        return dataDict;

    }

    /**
     * Split into sentences from document using stanford NE tagger
     *
     * @param txt
     * @return
     */
    private ArrayList<String> getSentList(String txt, StanfordCoreNLP pipeline) {
        ArrayList<String> sentList = new ArrayList<>();

        Annotation annotation = new Annotation(txt);
        pipeline.annotate(annotation);
        List<CoreMap> sentences = annotation.get(CoreAnnotations.SentencesAnnotation.class);
        for (CoreMap sentence : sentences) {
            sentList.add(sentence.toString());
        }
        return sentList;
    }

    public LinkedHashMap<String, String> getContentFileDict() {
        return contentFileDict;
    }

    public void setContentFileDict(LinkedHashMap<String, String> contentFileDict) {
        this.contentFileDict = contentFileDict;
    }

    public LinkedHashMap<String, String> getNerFileDict() {
        return nerFileDict;
    }

    public void setNerFileDict(LinkedHashMap<String, String> nerFileDict) {
        this.nerFileDict = nerFileDict;
    }

    public String getSourceDir() {
        return sourceDir;
    }

    public void setSourceDir(String sourceDir) {
        this.sourceDir = sourceDir;
    }

    public LinkedHashMap<String, Document> getDocumentREDict() {
        return documentREDict;
    }

    public void setDocumentREDict(LinkedHashMap<String, Document> documentREDict) {
        this.documentREDict = documentREDict;
    }

}
