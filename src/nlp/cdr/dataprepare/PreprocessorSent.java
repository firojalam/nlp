/*
 * This work by Firoj Alam is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License.
 * Permissions beyond the scope of this license may be available by sending an email to firojalam@gmail.com.
 * http://creativecommons.org/licenses/by-nc/4.0/deed.en_US
 * 
 */


package nlp.cdr.dataprepare;

import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import nlp.cdr.dataanalysis.ChemicalDisease;
import nlp.cdr.dataanalysis.DictionaryReader;
import nlp.cdr.dataanalysis.Document;
import nlp.cdr.re.DataFormatter;
import nlp.cdr.re.DataReader;
import nlp.cdr.re.Preprocessor;
import nlp.cdr.re.ProcessOutput;
import nlp.cdr.re.REfeatureExtractor;
import nlp.cdr.re.ReDocument;
import nlp.cdr.re.Token;
import nlp.utils.LibraryPath;
import nlp.utils.ParseCommands;
import nlp.utils.ReadConfig;
import org.apache.commons.cli.CommandLine;

/**
 *
 * @author Firoj Alam
 */
public class PreprocessorSent {
private static final Logger logger = Logger.getLogger(Preprocessor.class.getName());
    private HashMap<String, String> tokenFileDict = null;
    private HashMap<String, String> reAnnotationFileDict = null;
    private LinkedHashMap<String, String> nerFileDict = null;
    
    public PreprocessorSent() {
    }
    
    /**
     * Prepares data
     * @param inputDir
     * @param outputDirTok
     * @param outputDirAnnotation
     * @param dataReader
     * @param tokForm 
     */
    public void prepareData(String inputDir, String outputDirTok,String outputDirAnnotation,DataReader dataReader, String tokForm){
        String libPath = new LibraryPath().getLibPath();        
        //reads token and NER content from input files
        LinkedHashMap<String, ArrayList<Token>> contentTokenDict = dataReader.processToken(dataReader.getContentFileDict());
        LinkedHashMap<String, Document> documentREDict = dataReader.processNEContent(dataReader.getNerFileDict());
        nerFileDict = dataReader.getNerFileDict();
        //reads MESH dictionary
        DictionaryReader readDict = new DictionaryReader();
        String dictionaryFileName = libPath+"/etc/CTD_chemicals_diseases.ser";
        HashMap<String, ChemicalDisease> chemicalDiseaseDict = readDict.readDictionaryObject(dictionaryFileName);
        
        //format and add some features with re instances
        DataFormatter dataFormatter = new DataFormatter();
        LinkedHashMap<String, ReDocument> relationInstDict = dataFormatter.processRE4Prediction(contentTokenDict, documentREDict, dataReader.getSourceDir(),chemicalDiseaseDict);
        
        //output *.token and *.annotation file for feature extraction
        ProcessOutput output = new ProcessOutput();
        HashMap<String, String> tokenFileDict = output.tokenOutput(contentTokenDict, outputDirTok,tokForm);
        HashMap<String, String> reAnnotationFileDict = output.relationInstanceOutput(relationInstDict, outputDirAnnotation);    
        this.tokenFileDict = tokenFileDict;
        this.reAnnotationFileDict = reAnnotationFileDict;
    }

    public HashMap<String, String> getTokenFileDict() {
        return tokenFileDict;
    }

    public LinkedHashMap<String, String> getNerFileDict() {
        return nerFileDict;
    }

    public HashMap<String, String> getReAnnotationFileDict() {
        return reAnnotationFileDict;
    }
    
    /**
     * Calculates execution time of the whole system
     * @param d1
     * @param d2 
     */
    public static void duration(Date d1, Date d2){
        String timeTaken = "";
        try {
            long diff = d2.getTime() - d1.getTime();
            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);
            timeTaken = diffDays + " days, " + diffHours + " hours, " + diffMinutes + " minutes, " + diffSeconds + " seconds.";
        } catch (Exception ex) {
            logger.log(Level.SEVERE,"Time cal problem.");
        }
        logger.log(Level.INFO, "Time taken: {0}", timeTaken);
        logger.info("Done..");    
    }    
    /**
     * 
     * @param args 
     */
    public static void main(String[] args){
        Date d1 = new Date();
        System.out.println("System started at: " + d1);
        ParseCommands cmds  = new ParseCommands();
        CommandLine cmdDict = cmds.parseCommandsSent(args);
        String inputDir = cmdDict.getOptionValue("i");
        String goldREfile = cmdDict.getOptionValue("g");
        String outputSVM = cmdDict.getOptionValue("o");
        String configFile = cmdDict.getOptionValue("c");
        String tokForm = cmdDict.getOptionValue("t");

        //String inFileName = args[0];
//        String goldREfile = args[1];

        /**
         * Input arguments it expects:
         * 1. Gold annotation file
         * 2. Directory from Roberto's output
         * 3. Name of the output file. this would be in svm-light format
         */
        //String goldREfile = "/Users/firojalam/Study_PhD_projects/CDR/data/CDR_Training/CDR_TrainingSet.PubTator.txt";
//        String goldREfile = "/home/firoj.alam/Study_PhD_projects/CDR/data/CDR_Training/CDR_TrainingSet.PubTator.txt";
//        logger.info("Reads gold data as input..");
//        String inputDir="/home/firoj.alam/Study_PhD_projects/CDR/data/TrainingSet_7_15_15/";
//        String outputSVM = "";
        //String dictionaryFileName = "/Users/firojalam/Study_PhD_projects/CDR/data/CTD_chemicals_diseases_v1.tsv";
        //String configFile = "config.txt";        
        
        
        ///////Required library path
        String libPath = new LibraryPath().getLibPath();
        String dataDir = libPath + "/featextractor/data";
        String dataDirTmp = libPath + "/featextractor/data/tmp";
        String outputDirTok = libPath + "/featextractor/data/input";        
        String outputDirAnnotation = libPath + "/featextractor/data/annotation/sent";
        String featDir = libPath + "/featextractor/data/features/sent";
        logger.info("Cleaning the input and annotation directories...");
        try{
            File dir = new File(outputDirTok);
            for(File file: dir.listFiles()) file.delete();
            dir = new File(outputDirAnnotation);
            for(File file: dir.listFiles()) file.delete();            
            dir = new File(featDir);
            for(File file: dir.listFiles()) file.delete();            
            dir = new File(dataDirTmp);
            for(File file: dir.listFiles()) file.delete();
        }catch(Exception ex){
            logger.log(Level.SEVERE, "Problem while deleting files.", ex);
        }
        
        Properties props = new Properties();
        StanfordCoreNLP pipeline = null;
        //props.setProperty("annotators", "tokenize, ssplit, pos, lemma");
        props.setProperty("annotators", "tokenize, ssplit");
        pipeline = new StanfordCoreNLP(props);
        
        logger.info("Reading input data and extracting tokens...");
        DataReaderSent dataReader = new DataReaderSent(pipeline);
        dataReader.readDir(inputDir);
        LinkedHashMap<String, ArrayList<Token>> contentTokenDict = dataReader.processTokenDoc(dataReader.getContentFileDict());
        //LinkedHashMap<String, HashMap<Integer,ArrayList<Token>>> contentTokenDict = dataReader.processToken(dataReader.getContentFileDict());
        LinkedHashMap<String, Document> documentREDict = dataReader.processNEContent(dataReader.getNerFileDict());
        LinkedHashMap<String, Document> goldRE = dataReader.readGoldRE(goldREfile);
        ExtractSentRelation sentRelation = new ExtractSentRelation();
        LinkedHashMap<String, ArrayList<SentData>> sentDataDict = sentRelation.extractSentWithRelation(documentREDict);
        
        //LinkedHashMap<String, HashMap<Integer,ArrayList<Token>>> contentTokenDict = sentRelation.extractSentTokens(sentDataDict,pipeline);
        //System.exit(0);
        
        logger.info("Reading dictionary...");
        DictionaryReader readDict = new DictionaryReader();
              
        String dictionaryFileName = libPath+"/etc/CTD_chemicals_diseases.ser";
        HashMap<String, ChemicalDisease> chemicalDiseaseDict = readDict.readDictionaryObject(dictionaryFileName);
        //HashMap<String, ChemicalDisease> chemicalDiseaseDict = null;        
          
        logger.info("Generating relation instances...");
        DataFormatterSent dataFormatter = new DataFormatterSent();
        LinkedHashMap<String, ArrayList<ReDocument>> relationInstDict = dataFormatter.processREwithGold(contentTokenDict, sentDataDict, dataReader.getSourceDir(),chemicalDiseaseDict, goldRE);
        
        logger.info("Writing data to files...");
        ProcessOutput processOutput = new ProcessOutput();
        //String newPath = "/home/firoj.alam/Study_PhD_projects/CDR/data/TrainingSet_Sent/";        
        HashMap<String, String> reFileDict = processOutput.relationInstanceOutput4Sent(relationInstDict, outputDirAnnotation);
        HashMap<String, String> tokenFileDict = processOutput.tokenOutput4Sent(contentTokenDict, outputDirTok,tokForm,reFileDict);
        
        logger.info("Extracting features for relation extraction...");
        ReadConfig config = new ReadConfig(configFile);
        HashMap<String, String> configDict = config.getConfigDict();
        REfeatureExtractor featureExtractor = new REfeatureExtractor(configDict);
        HashMap<String, String> svmFeatFileDict = featureExtractor.extractFeatures(tokenFileDict, featDir, reFileDict);
        logger.info("Extracting features for relation extraction...");
        processOutput.writeData4SVM(svmFeatFileDict, outputSVM);
        
        Date d2 = new Date();
        duration(d1,d2);
    }  
}
