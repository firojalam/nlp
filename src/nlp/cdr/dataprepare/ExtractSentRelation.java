/*
 * This work by Firoj Alam is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License.
 * Permissions beyond the scope of this license may be available by sending an email to firojalam@gmail.com.
 * http://creativecommons.org/licenses/by-nc/4.0/deed.en_US
 * 
 */
package nlp.cdr.dataprepare;

import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import nlp.cdr.dataanalysis.Document;
import nlp.cdr.dataanalysis.Mention;
import nlp.cdr.dataanalysis.Sentence;
import nlp.cdr.re.Token;
import nlp.utils.Write2File;

/**
 *
 * @author Firoj Alam
 */
public class ExtractSentRelation {

    private HashSet<String> uniqueMentionListSameSent = new HashSet<>();
    private ArrayList<String> mentionList = new ArrayList<>();
    private HashSet<String> uniqueMentionList = new HashSet<>();
    private HashSet<String> uniqueSentList = new HashSet<>();
    private LinkedHashSet<String> uniqueSentMentionList = new LinkedHashSet<>();

    /**
     *
     * Extract sentences containing relation
     *
     * @param dataDict
     * @return
     */
    public LinkedHashMap<String, ArrayList<SentData>> extractSentWithRelation(LinkedHashMap<String, Document> dataDict) {

        ArrayList<String> sentList = null;
        //Write2File wrtMention = new Write2File();
        //wrtMention.openFile("sent_data.txt");
        LinkedHashMap<String, ArrayList<SentData>> sentDataDict = new LinkedHashMap<>();
        for (Map.Entry<String, Document> entrySet : dataDict.entrySet()) {
            String docID = entrySet.getKey();
            Document document = entrySet.getValue();

            sentList = new ArrayList<>();
            sentList.addAll(document.getTitleSentList());
            sentList.addAll(document.getDocumentSentList());
            ArrayList<Sentence> sentListUpdated = getUpdatedSentList(sentList);
            //create candidate pair
            ArrayList<Mention> mentionList = document.getMentionList();
            ArrayList<SentData> sentDataList = new ArrayList<>();
            for (int i = 0; i < sentListUpdated.size(); i++) {
                Sentence sent = sentListUpdated.get(i);
                SentData sentData = checkMentions(sent, mentionList);
                if (sentData != null) {
                    sentDataList.add(sentData);
                }
            }
            //this.printSentData(docID, wrtMention, sentDataList, document.getCIDList());
            sentDataDict.put(docID, sentDataList);
        }//for each document
        //wrtMention.closeFile();
        return sentDataDict;
    }

    /**
     * Checks whether sentence contains any mentions or not
     *
     * @param sent
     * @param mentionList
     * @return
     */
    private SentData checkMentions(Sentence sent, ArrayList<Mention> mentionList) {
        SentData sentData = new SentData();
        ArrayList<Mention> mentionListTmp = new ArrayList<>();
        for (int i = 0; i < mentionList.size(); i++) {
            Mention mention = mentionList.get(i);
            if (mention.getStartIndex() >= sent.getStartIndex() && mention.getEndIndex() <= sent.getEndIndex()) {
                mentionListTmp.add(mention);
            } else if (mention.getStartIndex() > sent.getEndIndex()) {
                break;
            }
        }

        if (mentionListTmp.size() > 1) {
            LinkedHashSet<String> chemicalList = this.getCIDList(mentionListTmp, "Chemical");
            LinkedHashSet<String> diseaseList = this.getCIDList(mentionListTmp, "Disease");
            if (chemicalList.size() > 0 && diseaseList.size() > 0) {
                sentData.setMentionList(mentionListTmp);
                sentData.setSent(sent);
                return sentData;
            }
//            for (Iterator<String> iterator = chemicalList.iterator(); iterator.hasNext();) {
//                String chemical = iterator.next();
//                for (Iterator<String> iterator1 = diseaseList.iterator(); iterator1.hasNext();) {                    
//                    String disease = iterator1.next();
//                    String chemicalDisease = chemical.trim() + "-" + disease.trim();
//                }
//            }
        }
        return null;

    }

    /**
     * Designs unique CID list
     *
     * @param mentionList
     * @param cid
     * @return
     */
    private LinkedHashSet<String> getCIDList(ArrayList<Mention> mentionList, String cid) {
        LinkedHashSet<String> cidList = new LinkedHashSet<>();
        for (Mention mention : mentionList) {
            if (mention.getMention().equals(cid)) {
                cidList.add(mention.getMentionID());
            }
        }
        return cidList;
    }

    /**
     * Updates sentences with index
     *
     * @param sentList
     * @return
     */
    private ArrayList<Sentence> getUpdatedSentList(ArrayList<String> sentList) {
        ArrayList<Sentence> sentListUpdated = new ArrayList<>();
        Sentence sentObj = null;
        int startIndex = 0;
        int endIndex = 0;
        for (int i = 0; i < sentList.size(); i++) {
            String sent = sentList.get(i);
            sentObj = new Sentence();
            sentObj.setSentID(i);
            sentObj.setText(sent);
            sentObj.setStartIndex(startIndex);
            endIndex = endIndex + sent.length();
            sentObj.setEndIndex(endIndex);
            startIndex = endIndex + 1;
            sentListUpdated.add(sentObj);
        }
        return sentListUpdated;
    }

    /**
     * Designs mention list
     *
     * @param mentionList
     * @param cid0
     * @return
     */
    private ArrayList<Mention> getMentionList(ArrayList<Mention> mentionList, String cid0) {
        ArrayList<Mention> tokenList = new ArrayList<>();
        for (Mention mention : mentionList) {
            if (mention.getMentionID().equals(cid0)) {
                tokenList.add(mention);
            }
        }
        return tokenList;
    }

    /**
     *
     * @param chemicalMentionList
     * @param diseaseMentionList
     * @param sentListUpdated
     * @return
     */
    private boolean getCandidateMentionSent(ArrayList<Mention> chemicalMentionList, ArrayList<Mention> diseaseMentionList, ArrayList<Sentence> sentListUpdated) {
        boolean sameSentFound = false;
        for (int i = 0; i < sentListUpdated.size(); i++) {
            Sentence sent = sentListUpdated.get(i);
            String mentionStr = "";
            for (int j = 0; j < chemicalMentionList.size(); j++) {
                Mention mention = chemicalMentionList.get(j);
                if (mention.getStartIndex() >= sent.getStartIndex() && mention.getEndIndex() <= sent.getEndIndex()) {
                    for (int k = 0; k < diseaseMentionList.size(); k++) {
                        Mention mentionCID1 = diseaseMentionList.get(k);
                        mentionStr = mention.getToken().trim() + "#" + mentionCID1.getToken().trim();
                        //this.uniqueMentionList.add(mentionStr);
                        if (mentionCID1.getStartIndex() >= sent.getStartIndex() && mentionCID1.getEndIndex() <= sent.getEndIndex()) {
                            //System.out.print(mentionStr+"\t"+sent.getText()+"\t");
                            return true;

                        }
                    }//disease loop
                }
            }//chemical loop
        }
        return sameSentFound;
    }

    /**
     * Print data
     *
     * @param docID
     * @param wrtMention
     * @param sentDataList
     * @param goldCIDList
     */
    private void printSentData(String docID, Write2File wrtMention, ArrayList<SentData> sentDataList, ArrayList<ArrayList<String>> goldCIDList) {
        String str = "";
        for (int i = 0; i < sentDataList.size(); i++) {
            SentData sentData = sentDataList.get(i);
            Sentence sent = sentData.getSent();
            wrtMention.write2File(docID + "|s|" + sent.getSentID() + "|" + sent.getText() + "\n");
        }
        for (int i = 0; i < sentDataList.size(); i++) {
            SentData sentData = sentDataList.get(i);
            ArrayList<Mention> mentionList = sentData.getMentionList();
            for (int j = 0; j < mentionList.size(); j++) {
                Mention mention = mentionList.get(j);
                wrtMention.write2File(docID + "\t" + mention.getStartIndex() + "\t" + mention.getEndIndex() + "\t" + mention.getToken() + "\t" + mention.getMention() + "\t" + mention.getMentionID() + "\n");
            }
        }
        if (sentDataList.size() > 0) {
            for (int i = 0; i < goldCIDList.size(); i++) {
                ArrayList<String> cidList = goldCIDList.get(i);
                String cid = cidList.get(0) + "\t" + cidList.get(1);
                wrtMention.write2File(docID + "\tCID\t" + cid + "\n");
            }
            wrtMention.write2File("\n");
        }
    }

    /**
     *
     * @param sentDataDict
     * @return
     */
    public LinkedHashMap<String, HashMap<Integer, ArrayList<Token>>> extractSentTokens(LinkedHashMap<String, ArrayList<SentData>> sentDataDict, StanfordCoreNLP pipeline) {
        LinkedHashMap<String, HashMap<Integer, ArrayList<Token>>> sentTokenData = new LinkedHashMap<>();
        for (Map.Entry<String, ArrayList<SentData>> entrySet : sentDataDict.entrySet()) {
            String docID = entrySet.getKey();
            ArrayList<SentData> sentDataList = entrySet.getValue();
            HashMap<Integer, ArrayList<Token>> sentTokenDict = new HashMap<>();
            for (int i = 0; i < sentDataList.size(); i++) {
                SentData sentData = sentDataList.get(i);
                int sentID = sentData.getSent().getSentID();
                ArrayList<Token> tokenList = extractTokenList(sentData.getSent().getText(), pipeline);
                sentTokenDict.put(sentID, tokenList);
            }
            sentTokenData.put(docID, sentTokenDict);
        }
        return sentTokenData;
    }

    /**
     * Parse sentence into token, lemma and pos
     *
     * @param sent
     * @param pipeline
     * @return
     */
    private ArrayList<Token> extractTokenList(String sentence, StanfordCoreNLP pipeline) {
        ArrayList<Token> tokenList = new ArrayList<>();
        Annotation annotation = new Annotation(sentence);
        pipeline.annotate(annotation);
        int tokID = 0;
        Token token = null;
        int startIndex = 0;
        int endIndex = 0;
        for (CoreLabel wToken : annotation.get(TokensAnnotation.class)) {
            // this is the text of the token
            String word = wToken.get(TextAnnotation.class);
            //endIndex=startIndex+word.length()-1;
            // this is the POS tag of the token
            String pos = wToken.get(PartOfSpeechAnnotation.class);
            // this is the lemma label of the token
            String lemma = wToken.get(LemmaAnnotation.class);
            tokID++;
            token = new Token();
            token.setTokenPosition(tokID + "");
            token.setToken(word);
            token.setLemma(lemma);
            token.setTokenPos(pos);
            //token.setCharStartIndex(startIndex+"");
            //token.setCharEndIndex(endIndex+"");
            //startIndex=endIndex+1;
            tokenList.add(token);
        }
        return tokenList;
    }
}
