/*
 * This work by Firoj Alam is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License.
 * Permissions beyond the scope of this license may be available by sending an email to firojalam@gmail.com.
 * http://creativecommons.org/licenses/by-nc/4.0/deed.en_US
 * 
 */


package nlp.cdr.dataprepare;

import java.util.ArrayList;
import nlp.cdr.dataanalysis.Mention;
import nlp.cdr.dataanalysis.Sentence;

/**
 *
 * @author Firoj Alam
 */
public class SentData {
    private Sentence sent = new Sentence();
    private ArrayList<Mention> mentionList = new ArrayList<>();

    public SentData() {
    }

    public Sentence getSent() {
        return sent;
    }

    public void setSent(Sentence sent) {
        this.sent = sent;
    }

    public ArrayList<Mention> getMentionList() {
        return mentionList;
    }

    public void setMentionList(ArrayList<Mention> mentionList) {
        this.mentionList = mentionList;
    }
    
    
}
