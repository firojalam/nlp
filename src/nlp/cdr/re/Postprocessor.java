/*
 * This work by Firoj Alam is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License.
 * Permissions beyond the scope of this license may be available by sending an email to firojalam@gmail.com.
 * http://creativecommons.org/licenses/by-nc/4.0/deed.en_US
 * 
 */
package nlp.cdr.re;

import edu.stanford.nlp.ling.Label;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import nlp.cdr.dataanalysis.Document;
import nlp.cdr.dataanalysis.Mention;
import nlp.utils.Write2File;

/**
 * Combines all results such as NER and RE then produces pubtator formatted output.
 * @author Firoj Alam
 */
public class Postprocessor {
    //logger
    private static final Logger logger = Logger.getLogger(Postprocessor.class.getName());
    private HashMap<String, String> configDict = null;
    
    public Postprocessor() {
    }

    public Postprocessor(HashMap<String, String> config) {
        this.configDict=config;
        //if()
    }
    
    /**
     * Combines NER output and RE class labels for each document and output in pubtator format.
     * @param nerFileDict
     * @param reAnnotationFileDict
     * @param classifiedFileDict
     * @param inputDir 
     */
    public void processFilesInPubTatorFormat(LinkedHashMap<String, String> nerFileDict, HashMap<String, String> reAnnotationFileDict, HashMap<String, String> classifiedFileDict, String inputDir){
        
        for (Map.Entry<String, String> entrySet : nerFileDict.entrySet()) {
            try {
                String docID = entrySet.getKey();
                String nerDocPath = entrySet.getValue();
                String path = new File(inputDir).getAbsolutePath();
                String outputFile = path + "/" + docID + ".cid.output.pubtator";
                String annotationFile = reAnnotationFileDict.get(docID);
                String predfile = classifiedFileDict.get(docID);
                this.processPubTatorFormat(nerDocPath, annotationFile, predfile, outputFile);
            }catch(Exception ex){
                logger.log(Level.SEVERE, "Somehting is wrong while combining output, please check the stack-trace.",ex);
            }
        }        
    }
    /**
     * Postprocess the NER and RE data in pubtator format.
     *
     * @param nerOutput - File containing NER annotation in pubtator format
     * @param annotationFile - Feature/Instance file for RE prediction
     * @param rePredictedFile - RE predicted file from SVMLight classifier
     * @param outfile output file to write
     */
    private void processPubTatorFormat(String nerOutput, String annotationFile, String rePredictedFile, String outfile) {
        Write2File wrt = new Write2File();
        wrt.openFile(outfile);
        //Read NER content from the file
        String nerFileContent = this.readNERoutput(nerOutput);
        wrt.write2File(nerFileContent+"\n");
        //Read features from the file
        ArrayList<REpredInst> reInstList = this.readFeatureFile(annotationFile);
        //Read predictions from the file
        HashMap<Integer, String> predDict = this.readPredictedFile(rePredictedFile);
        reInstList = this.mapREPredictions(reInstList, predDict);
        for (int i = 0; i < reInstList.size(); i++) {
            REpredInst inst = reInstList.get(i);
            wrt.write2File(inst.toString()+"\n");
        }
        wrt.closeFile();
    }
    /**
     * Combines NER output and RE class labels for each document and output in pubtator format.
     * It also searches Relation in gold-label dataset with predicted entity set.
     * @param nerFileDict
     * @param reAnnotationFileDict
     * @param classifiedFileDict
     * @param inputDir 
     */
    public ArrayList<String> processStreamInPubTatorFormat(LinkedHashMap<String, String> nerFileDict, HashMap<String, String> reAnnotationFileDict, HashMap<String, String> classifiedFileDict, String inputDir){
        RELookUp relookUP = null;
        ArrayList<String> outFileList = new ArrayList<>();
        if(this.configDict.containsKey("RE_dict")){
            logger.info("Reading gold relation dictionary...");
            relookUP = new RELookUp(this.configDict);
        }
        for (Map.Entry<String, String> entrySet : nerFileDict.entrySet()) {
            try {
                String docID = entrySet.getKey();
                String nerDocPath = entrySet.getValue();
                String path = new File(inputDir).getAbsolutePath();
                String outputFile = path + "/" + docID + ".cid.output.pubtator";
                String annotationFile = reAnnotationFileDict.get(docID);
                String predfile = classifiedFileDict.get(docID);
                this.processStreamPubTatorFormat(nerDocPath, annotationFile, predfile, outputFile,relookUP);
                outFileList.add(outputFile);
            }catch(Exception ex){
                logger.log(Level.SEVERE, "Somehting is wrong while combining output, please check the stack-trace.",ex);
            }
        }
        return outFileList;
    }
    /**
     * Postprocess the NER and RE data in pubtator format.
     *
     * @param nerOutput - File containing NER annotation in pubtator format
     * @param annotationFile - Feature/Instance file for RE prediction
     * @param rePredictedFile - RE predicted file from SVMLight classifier
     * @param outfile output file to write
     * @param relookUP
     */
    private void processStreamPubTatorFormat(String nerOutput, String annotationFile, String rePredictedFile, String outfile, RELookUp relookUP) {
        Write2File wrt = new Write2File();
        wrt.openFile(outfile);
        StringBuilder data = new StringBuilder();
        
        //Read NER content from the file
        String nerFileContent = this.readNERoutput(nerOutput);
        
        wrt.write2File(nerFileContent+"\n");
        data.append(nerFileContent).append("\n");
        //Read features from the file
        ArrayList<REpredInst> reInstList = this.readFeatureFile(annotationFile);
        //Read predictions from the file
        HashMap<Integer, String> predDict = this.readPredictedFile(rePredictedFile);
        reInstList = this.mapREPredictions(reInstList, predDict);
        for (int i = 0; i < reInstList.size(); i++) {
            REpredInst inst = reInstList.get(i);
            data.append(inst.toString()).append("\n");
        }
        data.append("\n");
        DataReader dataReader = new DataReader();
        LinkedHashMap<String, Document> dataDict = dataReader.readDataStream(data.toString());
        
        //// IF the system already read the gold relation dictionary then it matches that with the predicted set 
        if(relookUP!=null){
            logger.info("Checking the relations in gold relation set...");
            dataDict = relookUP.readRE(dataDict, relookUP.getReListR());
        }
        
        Map.Entry<String, Document> entry = dataDict.entrySet().iterator().next();
        String docID = entry.getKey();
        Document document = entry.getValue();        
        data = new StringBuilder();
        for (int i = 0; i < document.getCIDList().size(); i++) {
            ArrayList<String> cid = document.getCIDList().get(i);
            data.append(docID).append("\t").append("CID").append("\t");
            data.append(cid.get(0)).append("\t").append(cid.get(1)).append("\n");
        }
        wrt.write2File(data.toString());
        wrt.closeFile();
    }    
    /**
     * Combines NER output and RE class labels for each document and output in pubtator format.
     * It also searches Relation in gold-label dataset with predicted entity set.
     * @param nerFileDict
     * @param reDocAnnotationFileDict
     * @param reSentAnnotationFileDict
     * @param classifiedFileDict
     * @param classifiedFileSentDict
     * @param inputDir 
     */    
    public void processStreamInPubTatorFormatDocSent(LinkedHashMap<String, String> nerFileDict, 
            HashMap<String, String> reDocAnnotationFileDict, HashMap<String, String> reSentAnnotationFileDict,
            HashMap<String, String> classifiedFileDict, 
            HashMap<String, String> classifiedFileSentDict, String inputDir){
        RELookUp relookUP = null;
        if(this.configDict.containsKey("RE_dict")){
            logger.info("Reading gold relation dictionary...");
            relookUP = new RELookUp(this.configDict);
        }
        for (Map.Entry<String, String> entrySet : nerFileDict.entrySet()) {
            try {
                String docID = entrySet.getKey();
                String nerDocPath = entrySet.getValue();
                String path = new File(inputDir).getAbsolutePath();
                String outputFile = path + "/" + docID + ".cid.output.pubtator";
                String annotationFileDoc = reDocAnnotationFileDict.get(docID);
                String predFileDoc = classifiedFileDict.get(docID);
                
                if(classifiedFileSentDict.containsKey(docID)){
                    String predFileSent = classifiedFileSentDict.get(docID);
                    String annotationFileSent = reSentAnnotationFileDict.get(docID);
                    this.processStreamPubTatorFormatDocSent(nerDocPath, annotationFileDoc, annotationFileSent,predFileDoc, predFileSent,outputFile,relookUP);
                }else{
                    this.processStreamPubTatorFormat(nerDocPath, annotationFileDoc, predFileDoc, outputFile,relookUP);
                }
            }catch(Exception ex){
                logger.log(Level.SEVERE, "Somehting is wrong while combining output, please check the stack-trace.",ex);
            }
        }        
    }
    /**
     * Postprocess the NER and RE data in pubtator format.
     *
     * @param nerOutput - File containing NER annotation in pubtator format
     * @param annotationFileDoc - Feature/Instance file for RE prediction
     * @param predFileDoc - RE predicted file from SVMLight classifier
     * @param outfile output file to write
     * @param relookUP
     */
    private void processStreamPubTatorFormatDocSent(String nerOutput, String annotationFileDoc,String annotationFileSent, String predFileDoc, String predFileSent, String outfile, RELookUp relookUP) {
        Write2File wrt = new Write2File();
        wrt.openFile(outfile);
        StringBuilder data = new StringBuilder();
        
        //Read NER content from the file
        String nerFileContent = this.readNERoutput(nerOutput);
        
        wrt.write2File(nerFileContent+"\n");
        data.append(nerFileContent).append("\n");
        /**
         * Steps for document
         */
        //Read features from the file
        ArrayList<REpredInst> reInstList = this.readFeatureFile(annotationFileDoc);
        //Read predictions from the file
        HashMap<Integer, String> predDict = this.readPredictedFile(predFileDoc);
        reInstList = this.mapREPredictions(reInstList, predDict);
        for (REpredInst inst : reInstList) {
            data.append(inst.toString()).append("\n");
        }
        /**
         * Steps for sentence
         */        
        reInstList = this.readFeatureFile(annotationFileSent);
        //Read predictions from the file
        predDict = this.readPredictedFile(predFileSent);
        reInstList = this.mapREPredictions(reInstList, predDict);
        for (REpredInst inst : reInstList) {
            data.append(inst.toString()).append("\n");
        }        
        data.append("\n");
        DataReader dataReader = new DataReader();
        LinkedHashMap<String, Document> dataDict = dataReader.readDataStream(data.toString());
        
        //// IF the system already read the gold relation dictionary then it matches that with the predicted set 
        if(relookUP!=null){
            logger.info("Checking the relations in gold relation set...");
            dataDict = relookUP.readRE(dataDict, relookUP.getReListR());
        }
        
        Map.Entry<String, Document> entry = dataDict.entrySet().iterator().next();
        String docID = entry.getKey();
        Document document = entry.getValue();        
        data = new StringBuilder();
        for (int i = 0; i < document.getCIDList().size(); i++) {
            ArrayList<String> cid = document.getCIDList().get(i);
            data.append(docID).append("\t").append("CID").append("\t");
            data.append(cid.get(0)).append("\t").append(cid.get(1)).append("\n");
        }
        wrt.write2File(data.toString());
        wrt.closeFile();
    }    

    /**
     * Maps the predicted instances
     * @param reInstList 
     * @param predDict
     * @return RE instances with class label
     */
    private ArrayList<REpredInst> mapREPredictions(ArrayList<REpredInst> reInstList, HashMap<Integer, String> predDict){
        ArrayList<REpredInst> rePredDict = new ArrayList<>();
        try {
            for (int i = 0; i < reInstList.size(); i++) {
                REpredInst inst = reInstList.get(i);
                if (predDict.containsKey(i)) {
                    inst.setClsLabel(predDict.get(i));
                    rePredDict.add(inst);
                }
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Something is wrong while mapping the predicted label. Please check the stack-trace.", ex);
        }
        return rePredDict;
    }
    /**
     * Reads NER output from a file.
     *
     * @param fileName name of the file with absolute path
     * @return content of the file
     */
    private String readNERoutput(String fileName) {
        StringBuilder text = new StringBuilder();
        try {
            BufferedReader fileRead = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
            String str = "";
            while ((str = fileRead.readLine()) != null) {
                text.append(str).append("\n");
            }//end read file
            fileRead.close();
        } catch (FileNotFoundException ex) {
            System.out.println("Please check your file.");
            logger.log(Level.SEVERE, "Please check your output file. It is not available.", ex);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Something is wrong while reading the file. Please check the stack-trace.", ex);
        }
        return text.toString().trim();

    }

    /**
     * Reads Feature file that is supplied to classification system.
     *
     * @param fileName name of the file with absolute path
     * @return content of the file
     */
    private ArrayList<REpredInst> readFeatureFile(String fileName) {
        ArrayList<REpredInst> reInstList = new ArrayList<>();
        try {
            BufferedReader fileRead = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
            String str = "";
            String baseName = new File(fileName).getName();
            String fileId = baseName.substring(0, baseName.indexOf("."));
            while ((str = fileRead.readLine()) != null) {
                if(str.trim().equals("RELATION")){
                    break;
                }
            }//reads till it found RELATION tag
            while ((str = fileRead.readLine()) != null) {
                str = str.trim();
                String arr[]=str.split(" ");                
                String chemicalID = arr[2];
                String diseaseID = arr[4];
                REpredInst reInst = new REpredInst();
                reInst.setChemicalID(chemicalID);
                reInst.setDocID(fileId);
                reInst.setDiseaseID(diseaseID);
                reInstList.add(reInst);
            }//end read file

            fileRead.close();
        } catch (FileNotFoundException ex) {
            System.out.println("Please check your file.");
            logger.log(Level.SEVERE, "Please check your feature file. It is not available.", ex);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Something is wrong while reading the file. Please check the stack-trace.", ex);
        }
        return reInstList;
    }

    /**
     * Reads class-labels, classified by the classification system.
     *
     * @param fileName name of the file with absolute path
     * @return content of the file in hashmap with index as key and class label as value
     */
    private HashMap<Integer, String> readPredictedFile(String fileName) {
        HashMap<Integer, String> predDict = new HashMap<>();
        try {
            BufferedReader fileRead = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
            String str = "";
            int index = 0;
            while ((str = fileRead.readLine()) != null) {
                str = str.trim();
                double clsLab = Double.parseDouble(str);
                if (clsLab > 0) {
                    predDict.put(index, str);
                }
                index++;
            }//end read file
            fileRead.close();
        } catch (FileNotFoundException ex) {
            System.out.println("Please check your file.");
            logger.log(Level.SEVERE, "Please check your prediction file. It is not available.", ex);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Something is wrong while reading the file. Please check the stack-trace.", ex);
        }
        return predDict;
    }

    /**
     * 
     * @param args 
     */
    public static void main(String args[]){
        String path="/Users/firojalam/Study_PhD_projects/CDR/data/TrainingSet_7_15_15/";
        String neroutput = path+"3425586.ner.output";
        String annotation = path+"3425586.annotation";
        String pred = path+"3425586.pred";
        Postprocessor postprocessor = new Postprocessor();
        postprocessor.processPubTatorFormat(neroutput, annotation, pred, path+"re_out.pubtator.txt");
    }
}
