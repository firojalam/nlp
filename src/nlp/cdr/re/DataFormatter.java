/*
 * This work by Firoj Alam is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License.
 * Permissions beyond the scope of this license may be available by sending an email to firojalam@gmail.com.
 * http://creativecommons.org/licenses/by-nc/4.0/deed.en_US
 * 
 */
package nlp.cdr.re;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.logging.Logger;
import nlp.cdr.dataanalysis.ChemicalDisease;
import nlp.cdr.dataanalysis.Document;
import nlp.cdr.dataanalysis.Mention;
import nlp.cdr.dataanalysis.Sentence;

/**
 *
 * @author Firoj Alam
 */
public class DataFormatter {
    private static final Logger logger = Logger.getLogger(DataFormatter.class.getName());
    public DataFormatter() {
    }

    /**
     *
     * @param contentTokenDict
     * @param documentREDict
     * @param sourceDir
     * @param chemicalDiseaseDict
     * @param goldRE
     * @return
     */
    public LinkedHashMap<String, ReDocument> processREwithGold(LinkedHashMap<String, ArrayList<Token>> contentTokenDict, LinkedHashMap<String, Document> documentREDict, String sourceDir, HashMap<String, ChemicalDisease> chemicalDiseaseDict, LinkedHashMap<String, Document> goldRE) {
        LinkedHashMap<String, ReDocument> relationInstDict = new LinkedHashMap<>();

        for (Map.Entry<String, Document> entrySet : documentREDict.entrySet()) {
            String docID = entrySet.getKey();
            Document document = entrySet.getValue();
            ArrayList<Mention> mentionList = document.getMentionList();
            ArrayList<Token> docTokenList = contentTokenDict.get(docID);
            LinkedHashMap<String, Entity> entityList = new LinkedHashMap<>();
            for (int i = 0; i < mentionList.size(); i++) {
                Mention mention = mentionList.get(i);
                String entityID = mention.getMentionID();
                if (entityList.containsKey(entityID)) {
                    this.updateMentionIndex2(docTokenList, mention);
                    if (mention.getStartIndex() == Integer.MIN_VALUE || mention.getEndIndex() == Integer.MIN_VALUE) {

                    } else {
                        entityList.get(entityID).getMentionList().add(mention);
                    }
                } else {
                    Entity entity = new Entity();
                    entity.setEntityID(entityID);
                    this.updateMentionIndex2(docTokenList, mention);
                    if (mention.getStartIndex() == Integer.MIN_VALUE || mention.getEndIndex() == Integer.MIN_VALUE) {

                    } else {
                        entity.getMentionList().add(mention);
                    }
                    entityList.put(entityID, entity);
                }
            }
            /**
            * Create positive-negative relation Instances
            */
            LinkedHashSet<String> chemicalList = this.getCIDList(mentionList, "Chemical");
            LinkedHashSet<String> diseaseList = this.getCIDList(mentionList, "Disease");
            ///Gold relation
            ArrayList<String> cidDictGold = new ArrayList<>();
            Document goldDoc = goldRE.get(docID);
            for (int i = 0; i < goldDoc.getCIDList().size(); i++) {
                ArrayList<String> cidList = goldDoc.getCIDList().get(i);
                cidDictGold.add(cidList.get(0).trim() + "-" + cidList.get(1).trim());
            }//traverse cidList of documnets
            ArrayList<RelationInstance> relationList = new ArrayList<>();
            //docRelationInstDict.put(docID, tmpList);
            ArrayList<String> sentList = new ArrayList<>();
            sentList.addAll(document.getTitleSentList());
            sentList.addAll(document.getDocumentSentList());            
            ArrayList<Sentence> sentListUpdated = getUpdatedSentList(sentList);
            for (Iterator<String> iterator = chemicalList.iterator(); iterator.hasNext();) {
                String chemical = iterator.next();
                ArrayList<Mention> chemicalMentionList = getMentionList(mentionList, chemical);
                for (Iterator<String> iterator1 = diseaseList.iterator(); iterator1.hasNext();) {                    
                    String disease = iterator1.next();
                    ArrayList<Mention> diseaseMentionList = getMentionList(mentionList, disease);
                    String chemicalDisease = chemical.trim() + "-" + disease.trim();
                    if (cidDictGold.contains(chemicalDisease)) {
                        RelationInstance instance = new RelationInstance();
                        instance.setClassLab(true);
                        instance.setEntityChemicalID(chemical);
                        instance.setEntityChemicalType("Chemical");
                        instance.setEntityDiseaseID(disease);
                        instance.setEntityDiseaseType("Disease");
                        this.isInGazetteers(instance, chemicalDisease, chemicalDiseaseDict);
                        boolean sameSent = this.isEntityMentionInSameSent(chemicalMentionList,diseaseMentionList,sentListUpdated);
                        instance.setIsInSameSent(sameSent);
                        boolean inTitle = this.isEntityMentionInTitle(chemicalMentionList,diseaseMentionList,getUpdatedSentList(document.getTitleSentList()));
                        instance.setIsInTitle(inTitle);
                        boolean inAbs = this.isEntityMentionInAbstract(chemicalMentionList,diseaseMentionList,getUpdatedSentList(document.getDocumentSentList()));
                        instance.setIsInAbstract(inAbs);

                        relationList.add(instance);
                        //docRelationInstDict.get(docID).add(instance);
                    } else {
                        RelationInstance instance = new RelationInstance();
                        instance.setClassLab(false);
                        instance.setEntityChemicalID(chemical);
                        instance.setEntityChemicalType("Chemical");
                        instance.setEntityDiseaseID(disease);
                        instance.setEntityDiseaseType("Disease");
                        this.isInGazetteers(instance, chemicalDisease, chemicalDiseaseDict);
                        boolean sameSent = this.isEntityMentionInSameSent(chemicalMentionList,diseaseMentionList,sentListUpdated);
                        instance.setIsInSameSent(sameSent);                        
                        boolean inTitle = this.isEntityMentionInTitle(chemicalMentionList,diseaseMentionList,getUpdatedSentList(document.getTitleSentList()));
                        instance.setIsInTitle(inTitle);
                        boolean inAbs = this.isEntityMentionInAbstract(chemicalMentionList,diseaseMentionList,getUpdatedSentList(document.getDocumentSentList()));
                        instance.setIsInAbstract(inAbs);                        
                        relationList.add(instance);
                    }
                }
            }//Generating relation instances  
            ReDocument relationDoc = new ReDocument();
            relationDoc.setEntityList(entityList);
            relationDoc.setRelationList(relationList);
            relationInstDict.put(docID, relationDoc);
        }//for each document            
        logger.info("Done generating instance...");
        return relationInstDict;
    }

    /**
     * Process file for feature extraction and generate instances
     * @param contentTokenDict
     * @param documentREDict
     * @param sourceDir
     * @param chemicalDiseaseDict
     * @return hashmap with fileid as key and ReDocument as value
     */
    public LinkedHashMap<String, ReDocument> processRE4Prediction(LinkedHashMap<String, ArrayList<Token>> contentTokenDict, LinkedHashMap<String, Document> documentREDict, String sourceDir, HashMap<String, ChemicalDisease> chemicalDiseaseDict) {
    
        LinkedHashMap<String, ReDocument> relationInstDict = new LinkedHashMap<>();
        for (Map.Entry<String, Document> entrySet : documentREDict.entrySet()) {
            String docID = entrySet.getKey();
            Document document = entrySet.getValue();
            ArrayList<Mention> mentionList = document.getMentionList();
            ArrayList<Token> docTokenList = contentTokenDict.get(docID);
            LinkedHashMap<String, Entity> entityList = new LinkedHashMap<>();
            for (int i = 0; i < mentionList.size(); i++) {
                Mention mention = mentionList.get(i);
                String entityID = mention.getMentionID();
                if (entityList.containsKey(entityID)) {
                    this.updateMentionIndex2(docTokenList, mention);
                    if (mention.getStartIndex() == Integer.MIN_VALUE || mention.getEndIndex() == Integer.MIN_VALUE) {

                    } else {
                        entityList.get(entityID).getMentionList().add(mention);
                    }
                } else {
                    Entity entity = new Entity();
                    entity.setEntityID(entityID);
                    this.updateMentionIndex2(docTokenList, mention);
                    if (mention.getStartIndex() == Integer.MIN_VALUE || mention.getEndIndex() == Integer.MIN_VALUE) {

                    } else {
                        entity.getMentionList().add(mention);
                    }
                    entityList.put(entityID, entity);
                }
            }

            /**
             * Create positive-negative relation Instances
             */
            LinkedHashSet<String> chemicalList = this.getCIDList(mentionList, "Chemical");
            LinkedHashSet<String> diseaseList = this.getCIDList(mentionList, "Disease");
            ArrayList<RelationInstance> relationList = new ArrayList<>();

            ArrayList<String> sentList = new ArrayList<>();
            sentList.addAll(document.getTitleSentList());
            sentList.addAll(document.getDocumentSentList());
            ArrayList<Sentence> sentListUpdated = getUpdatedSentList(sentList);
            for (Iterator<String> iterator = chemicalList.iterator(); iterator.hasNext();) {
                String chemical = iterator.next();
                ArrayList<Mention> chemicalMentionList = getMentionList(mentionList, chemical);
                for (Iterator<String> iterator1 = diseaseList.iterator(); iterator1.hasNext();) {
                    String disease = iterator1.next();
                    ArrayList<Mention> diseaseMentionList = getMentionList(mentionList, disease);
                    String chemicalDisease = chemical.trim() + "-" + disease.trim();
                    RelationInstance instance = new RelationInstance();
                    instance.setClassLab(false);
                    instance.setEntityChemicalID(chemical);
                    instance.setEntityChemicalType("Chemical");
                    instance.setEntityDiseaseID(disease);
                    instance.setEntityDiseaseType("Disease");
                    this.isInGazetteers(instance, chemicalDisease, chemicalDiseaseDict);
                    boolean sameSent = this.isEntityMentionInSameSent(chemicalMentionList, diseaseMentionList, sentListUpdated);
                    instance.setIsInSameSent(sameSent);
                    boolean inTitle = this.isEntityMentionInTitle(chemicalMentionList, diseaseMentionList, getUpdatedSentList(document.getTitleSentList()));
                    instance.setIsInTitle(inTitle);
                    boolean inAbs = this.isEntityMentionInAbstract(chemicalMentionList, diseaseMentionList, getUpdatedSentList(document.getDocumentSentList()));
                    instance.setIsInAbstract(inAbs);
                    relationList.add(instance);
                }
            }//Generating relation instances  
            ReDocument relationDoc = new ReDocument();
            relationDoc.setEntityList(entityList);
            relationDoc.setRelationList(relationList);
            relationInstDict.put(docID, relationDoc);
        }//for each document    
        logger.info("Done generating instance...");
        return relationInstDict;
    }
    
    /**
     * Match the entity-type
     *
     * @param mentionList
     * @param cid entity-type
     * @return associated entity-type list
     */
    private LinkedHashSet<String> getCIDList(ArrayList<Mention> mentionList, String cid) {
        LinkedHashSet<String> cidList = new LinkedHashSet<>();
        for (int i = 0; i < mentionList.size(); i++) {
            Mention mention = mentionList.get(i);
            if (mention.getMention().equals(cid)) {
                cidList.add(mention.getMentionID());
            }
        }
        return cidList;
    }

    /**
     *
     * @param contentTokenDict
     * @param mention update the mention's token position
     */
    private void updateMentionIndex2(ArrayList<Token> contentTokenDict, Mention mention) {
        String tokInMention[] = mention.getToken().split("\\s+");
        String firstTok = null;
        String lastTok = null;
        int firstIndex = Integer.MIN_VALUE;
        int lastIndex = Integer.MIN_VALUE;
        firstTok = tokInMention[0];
        for (int j = 0; j < contentTokenDict.size(); j++) {
            Token token = contentTokenDict.get(j);
            if (token.getTokenPosition() != null) {
                if (mention.getStartIndex() == Integer.parseInt(token.getCharStartIndex())) {
                    firstIndex = Integer.parseInt(token.getTokenPosition());
                    lastIndex = this.getLastIndex(contentTokenDict, j, mention);
                    break;
                }
            }
        }
        if(firstIndex==Integer.MIN_VALUE ||lastIndex==Integer.MIN_VALUE){
            System.err.println("Error: "+mention.getToken());
        }
        mention.setStartIndex(firstIndex);
        mention.setEndIndex(lastIndex);
    }

    /**
     * 
     * @param contentTokenDict
     * @param j
     * @param mention
     * @return 
     */
    private int getLastIndex(ArrayList<Token> contentTokenDict, int j, Mention mention) {
        int lastIndex = Integer.MIN_VALUE;
        for (int i = j; i < contentTokenDict.size(); i++) {
            Token token = contentTokenDict.get(i);
            if (token.getTokenPosition() != null) {
                if (mention.getEndIndex() == Integer.parseInt(token.getCharEndIndex())) {
                    lastIndex = Integer.parseInt(token.getTokenPosition());
                    return lastIndex;
                }
            }
        }
        return lastIndex;
    }

    /**
     *
     * @param contentTokenDict
     * @param mention update the mention's token position
     */
    private void updateMentionIndex(ArrayList<Token> contentTokenDict, Mention mention) {
        String tokInMention[] = mention.getToken().split("\\s+");
        String firstTok = null;
        String lastTok = null;
        int firstIndex = 0;
        int lastIndex = 0;
        firstTok = tokInMention[0];

        if (tokInMention.length == 1) {
            //lastTok= tokInMention[0];
            for (int j = 0; j < contentTokenDict.size(); j++) {
                Token token = contentTokenDict.get(j);
                if (token.getTokenPosition() != null) {
                    if (mention.getStartIndex() == Integer.parseInt(token.getCharStartIndex())
                            && mention.getEndIndex() == Integer.parseInt(token.getCharEndIndex())) {
                        firstIndex = Integer.parseInt(token.getTokenPosition());
                        lastIndex = Integer.parseInt(token.getTokenPosition());
                        break;
                    }
                }
            }
        } else if (tokInMention.length > 1) {
            lastTok = tokInMention[tokInMention.length - 1];
            int index = 0;
            for (int j = 0; j < contentTokenDict.size(); j++) {
                Token token = contentTokenDict.get(j);
                if (token.getTokenPosition() != null) {
                    if (mention.getStartIndex() == Integer.parseInt(token.getCharStartIndex())) {
                        firstIndex = Integer.parseInt(token.getTokenPosition());
                        break;
                    }
                }
            }
            for (int i = index; i < contentTokenDict.size(); i++) {
                Token token = contentTokenDict.get(i);
                if (token.getTokenPosition() != null) {
                    if (mention.getEndIndex() == Integer.parseInt(token.getCharEndIndex())) {
                        lastIndex = Integer.parseInt(token.getTokenPosition());
                        break;
                    }
                }
            }
        }
        mention.setStartIndex(firstIndex);
        mention.setEndIndex(lastIndex);
    }

    /**
     * Checks if entity-pair in a relation is exist in the gazetteers or not
     *
     * @param instance
     * @param chemicalDisease
     * @param chemicalDiseaseDict
     */
    private void isInGazetteers(RelationInstance instance, String chemicalDisease, HashMap<String, ChemicalDisease> chemicalDiseaseDict) {
        if (chemicalDiseaseDict.containsKey(chemicalDisease)) {
            instance.setIsInGazetteers(true);
        } else {
            instance.setIsInGazetteers(false);
        }
    }

    /**
     * 
     * @param sentList
     * @return 
     */
    private ArrayList<Sentence> getUpdatedSentList(ArrayList<String> sentList) {
        ArrayList<Sentence> sentListUpdated = new ArrayList<>();
        Sentence sentObj = null;
        int startIndex = 0;
        int endIndex = 0;
        for (int i = 0; i < sentList.size(); i++) {
            String sent = sentList.get(i);
            sentObj = new Sentence();
            sentObj.setText(sent);
            sentObj.setStartIndex(startIndex);
            endIndex = endIndex + sent.length();
            sentObj.setEndIndex(endIndex);
            startIndex = endIndex + 1;
            sentListUpdated.add(sentObj);
        }
        return sentListUpdated;
    }

    /**
     * 
     * @param mentionList
     * @param cid0
     * @return 
     */
    private ArrayList<Mention> getMentionList(ArrayList<Mention> mentionList, String cid0) {
        ArrayList<Mention> tokenList = new ArrayList<>();
        for (int j = 0; j < mentionList.size(); j++) {
            Mention mention = mentionList.get(j);
            if (mention.getMentionID().equals(cid0)) {
                tokenList.add(mention);
            }
        }
        return tokenList;
    }

    /**
     * 
     * @param chemicalMentionList
     * @param diseaseMentionList
     * @param sentListUpdated 
     */
    private boolean isEntityMentionInSameSent(ArrayList<Mention> chemicalMentionList, ArrayList<Mention> diseaseMentionList, ArrayList<Sentence> sentListUpdated) {
        boolean sameSentFound = false;
        for (int i = 0; i < sentListUpdated.size(); i++) {
            Sentence sent = sentListUpdated.get(i);
            String mentionStr = "";
            for (int j = 0; j < chemicalMentionList.size(); j++) {
                Mention mention = chemicalMentionList.get(j);
                if (mention.getStartIndex() >= sent.getStartIndex() && mention.getEndIndex() <= sent.getEndIndex()) {
                    for (int k = 0; k < diseaseMentionList.size(); k++) {
                        Mention mentionCID1 = diseaseMentionList.get(k);
                        //mentionStr = mention.getToken().trim() + "#" + mentionCID1.getToken().trim();                        
                        if (mentionCID1.getStartIndex() >= sent.getStartIndex() && mentionCID1.getEndIndex() <= sent.getEndIndex()) {
                            //System.out.print(mentionStr+"\t"+sent.getText()+"\t");
                            return true;                            
                        }
                    }//disease loop
                }
            }//chemical loop
        }
        return sameSentFound;
    }    

    /**
     * 
     * @param chemicalMentionList
     * @param diseaseMentionList
     * @param sentListUpdated 
     */
    private boolean isEntityMentionInTitle(ArrayList<Mention> chemicalMentionList, ArrayList<Mention> diseaseMentionList, ArrayList<Sentence> sentListUpdated) {
        boolean sameSentFound = false;
        for (int i = 0; i < sentListUpdated.size(); i++) {
            Sentence sent = sentListUpdated.get(i);
            String mentionStr = "";
            for (int j = 0; j < chemicalMentionList.size(); j++) {
                Mention mention = chemicalMentionList.get(j);
                if (mention.getStartIndex() >= sent.getStartIndex() && mention.getEndIndex() <= sent.getEndIndex()) {
                    for (int k = 0; k < diseaseMentionList.size(); k++) {
                        Mention mentionCID1 = diseaseMentionList.get(k);
                        //mentionStr = mention.getToken().trim() + "#" + mentionCID1.getToken().trim();                        
                        if (mentionCID1.getStartIndex() >= sent.getStartIndex() && mentionCID1.getEndIndex() <= sent.getEndIndex()) {
                            //System.out.print(mentionStr+"\t"+sent.getText()+"\t");
                            return true;                            
                        }
                    }//disease loop
                }
            }//chemical loop
        }
        return sameSentFound;
    }    
    
    /**
     * 
     * @param chemicalMentionList
     * @param diseaseMentionList
     * @param sentListUpdated 
     */
    private boolean isEntityMentionInAbstract(ArrayList<Mention> chemicalMentionList, ArrayList<Mention> diseaseMentionList, ArrayList<Sentence> sentListUpdated) {
        boolean sameSentFound = false;
        for (int i = 0; i < sentListUpdated.size(); i++) {
            Sentence sent = sentListUpdated.get(i);
            String mentionStr = "";
            for (int j = 0; j < chemicalMentionList.size(); j++) {
                Mention mention = chemicalMentionList.get(j);
                if (mention.getStartIndex() >= sent.getStartIndex() && mention.getEndIndex() <= sent.getEndIndex()) {
                    for (int k = 0; k < diseaseMentionList.size(); k++) {
                        Mention mentionCID1 = diseaseMentionList.get(k);
                        //mentionStr = mention.getToken().trim() + "#" + mentionCID1.getToken().trim();                        
                        if (mentionCID1.getStartIndex() >= sent.getStartIndex() && mentionCID1.getEndIndex() <= sent.getEndIndex()) {
                            //System.out.print(mentionStr+"\t"+sent.getText()+"\t");
                            return true;                            
                        }
                    }//disease loop
                }
            }//chemical loop
        }
        return sameSentFound;
    }        

}
