/*
 * This work by Firoj Alam is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License.
 * Permissions beyond the scope of this license may be available by sending an email to firojalam@gmail.com.
 * http://creativecommons.org/licenses/by-nc/4.0/deed.en_US
 * 
 */


package nlp.cdr.re;

import java.util.ArrayList;
import nlp.cdr.dataanalysis.Mention;

/**
 *
 * @author Firoj Alam
 */
public class Entity {
    private String entityID = null;
    private ArrayList<Mention> mentionList = new ArrayList();

    public String getEntityID() {
        return entityID;
    }

    public void setEntityID(String entityID) {
        this.entityID = entityID;
    }

    public ArrayList<Mention> getMentionList() {
        return mentionList;
    }

    public void setMentionList(ArrayList<Mention> mentionList) {
        this.mentionList = mentionList;
    }
    

}
