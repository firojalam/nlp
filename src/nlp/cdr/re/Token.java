/*
 * This work by Firoj Alam is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License.
 * Permissions beyond the scope of this license may be available by sending an email to firojalam@gmail.com.
 * http://creativecommons.org/licenses/by-nc/4.0/deed.en_US
 * 
 */


package nlp.cdr.re;

/**
 *
 * @author Firoj Alam
 */
public class Token {

    private String tokenPosition = null; // position in document
    private String token = null;
    private String tokenPos = null;
    private String lemma = null;
    private String entityType = null;
    private String entityID = null;
    private String charStartIndex = null;
    private String charEndIndex = null;

    public Token() {

    }

    public String getTokenPosition() {
        return tokenPosition;
    }

    public void setTokenPosition(String tokenPosition) {
        this.tokenPosition = tokenPosition;
    }


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTokenPos() {
        return tokenPos;
    }

    public void setTokenPos(String tokenPos) {
        this.tokenPos = tokenPos;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public String getEntityID() {
        return entityID;
    }

    public void setEntityID(String entityID) {
        this.entityID = entityID;
    }

    public String getCharStartIndex() {
        return charStartIndex;
    }

    public void setCharStartIndex(String charStartIndex) {
        this.charStartIndex = charStartIndex;
    }

    public String getCharEndIndex() {
        return charEndIndex;
    }

    public void setCharEndIndex(String charEndIndex) {
        this.charEndIndex = charEndIndex;
    }

    public String getLemma() {
        return lemma;
    }

    public void setLemma(String lemma) {
        this.lemma = lemma;
    }

    @Override
    public String toString() {
        return "Token{" + "tokenPosition=" + tokenPosition + ", token=" + token + ", tokenPos=" + tokenPos + ", lemma=" + lemma + ", entityType=" + entityType + ", entityID=" + entityID + ", charStartIndex=" + charStartIndex + ", charEndIndex=" + charEndIndex + '}';
    }

    
    
    
    
    
}
