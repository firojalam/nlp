/*
 * This work by Firoj Alam is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License.
 * Permissions beyond the scope of this license may be available by sending an email to firojalam@gmail.com.
 * http://creativecommons.org/licenses/by-nc/4.0/deed.en_US
 * 
 */
package nlp.cdr.re;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import nlp.utils.LibraryPath;
import nlp.utils.StreamGobbler;

/**
 * It utilizes some external scripts to extract feature for RE system
 * @author Firoj Alam
 */
public class REfeatureExtractor {

    private static final Logger logger = Logger.getLogger(REfeatureExtractor.class.getName());
    private HashMap<String, String> configDict = new HashMap<>();

    public REfeatureExtractor(HashMap<String, String> config) {
        this.configDict = config;
    }

    /**
     * Main extractor method, that runs two feature extraction process
     * @param tokenFileDict token file dictionary mapped with docID as key and 
     * docpath as value. The file contains token and pos-tag. 
     * @param featDir feature directory name to which extracted feature will be saved
     * @param reAnnotationFileDict annotation file dictionary mapped with docID as key and 
     * docpath as value. The file contains relevant information for RE instances and it produced in the preprocessing step.
     * @return 
     */
    public HashMap<String, String> extractFeatures(HashMap<String, String> tokenFileDict, String featDir, HashMap<String, String> reAnnotationFileDict) {
        HashMap<String, String> svmFeatFileDict = new HashMap<>();
        //this.extractReFeatures(tokenFileDict, featDir);
        //HashMap<String, String> featFileDict = this.extractReFeaturesParallel(tokenFileDict, featDir);
        //svmFeatFileDict = this.extractFeatures4SVM(featDir, annotationDir, dataDirTmp);
        //svmFeatFileDict = this.extractFeatures4SVMParallel(featFileDict, reAnnotationFileDict, dataDirTmp);
        svmFeatFileDict = this.extractFeatures4SVMParallel(tokenFileDict, reAnnotationFileDict, featDir);
        return svmFeatFileDict;
    }

    /**
     * Extract features in parallel
     *
     * @param tokenFileDict token file dictionary mapped with docID as key and 
     * docpath as value. The file contains token and pos-tag. 
     * @param featDir feature directory name to which extracted feature will be saved
     * @return feature file list
     */
    public HashMap<String, String> extractReFeaturesParallel(HashMap<String, String> tokenFileDict, String featDir) {
        HashMap<String, String> featFileDict = new HashMap<>();
        int NTHREADS = this.getNumberOfThreads();
        ExecutorService executor = Executors.newFixedThreadPool(NTHREADS);

        String libPath = new LibraryPath().getLibPath();
//        String featExtract = libPath + "/featextractor/bin/extractFeatures/extractFeatures_v1.py";
//        String bfFeat = libPath + "/featextractor/dict/bf_set1.txt";
        String featExtract = libPath + "/" + this.configDict.get("FEAT_extractor");
        String bfFeat = libPath + "/" + this.configDict.get("BF_dict");
        for (Map.Entry<String, String> entrySet : tokenFileDict.entrySet()) {
            try {
                String docID = entrySet.getKey();
                String docTokenFilePath = entrySet.getValue();
                String featFile = featDir + "/" + docID + ".features";
                final List command = new ArrayList();
                command.add("python3.4");
                command.add(featExtract);
                command.add(docTokenFilePath);
                command.add(bfFeat);
                command.add(featFile);
                featFileDict.put(docID, featFile);
                Runnable worker = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            executeExternal(command, "feat");
                        } catch (Exception ex) {
                            logger.info("Problem in the executing threads");
                        }
                    }//end run
                };
                executor.execute(worker);
            } catch (Exception ex) {
                logger.log(Level.SEVERE, "Something is wrong while running first step of feature extraction.", ex);
            }
        }//end file list dictionary
        executor.shutdown();
        while (!executor.isTerminated()) {
            // waiting for the threads to be finished
        }
        logger.info("Finished all threads.");
        return featFileDict;
    }

    /**
     * Extract features for relation extraction
     *
     * @param tokenFileDict token file dictionary mapped with docID as key and 
     * docpath as value. The file contains token and pos-tag. 
     * @param featDir feature directory name to which extracted feature will be saved
     */
    private void extractReFeatures(HashMap<String, String> tokenFileDict, String featDir) {
        String libPath = new LibraryPath().getLibPath();
        String featExtract = libPath + "/" + this.configDict.get("FEAT_extractor");
        String bfFeat = libPath + "/" + this.configDict.get("BF_dict");

        for (Map.Entry<String, String> entrySet : tokenFileDict.entrySet()) {
            String docID = entrySet.getKey();
            String docTokenFilePath = entrySet.getValue();
            String featFile = featDir + "/" + docID + ".features";
            List command = new ArrayList();
            command.add("python3.4");
            command.add(featExtract);
            command.add(docTokenFilePath);
            command.add(bfFeat);
            command.add(featFile);
            this.executeExternal(command, "feat");
        }
        logger.info("Feature extraction step one is done...");
    }

    /**
     * Extract SVM features in parallel
     * @param featFileDict feature file dictionary mapped with docID as key and 
     * docpath as value
     * @param reAnnotationFileDict dictionary mapped with docID as key and 
     * docpath as value. The file contains relevant information for RE instances
     * @param dataDir feature directory name to which extracted feature will be saved
     * @return feature file list svm formatted
     */
    private HashMap<String, String> extractFeatures4SVMParallel(HashMap<String, String> featFileDict, HashMap<String, String> reAnnotationFileDict, String dataDir) {
        String libPath = new LibraryPath().getLibPath();
        HashMap<String, String> svmFeatFileDict = new HashMap<>();
        int NTHREADS = this.getNumberOfThreads();
        ExecutorService executor = Executors.newFixedThreadPool(NTHREADS);

        //format the features for SVMLight
        logger.info("Feature extraction step is started...");
        String featExtract = libPath + "/" + this.configDict.get("SVM_feat");
        String dictFeat = libPath + "/" + this.configDict.get("FEAT_selected");

        for (Map.Entry<String, String> entrySet : reAnnotationFileDict.entrySet()) {
            try {
                String docID = entrySet.getKey();
                String annotationFile = entrySet.getValue();
                String featFile = featFileDict.get(docID);
                //String annotationFile = reAnnotationFileDict.get(docID);
                String svmDatFile = dataDir + "/" + docID + ".dat";
                final List command = new ArrayList();
                command.add("python3.4");
                command.add(featExtract);
                command.add(dictFeat);
                command.add(featFile);
                command.add(annotationFile);
                command.add(svmDatFile);
                svmFeatFileDict.put(docID, svmDatFile);

                Runnable worker = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            executeExternal(command, "svm_feat");
                        } catch (Exception ex) {
                            logger.info("Problem in the executing threads while runing classifier.");
                        }
                    }//end run
                };
                executor.execute(worker);
            } catch (Exception ex) {
                logger.log(Level.SEVERE, "Something is wrong while running second step of feature extraction.", ex);
            }
        }//end dictionary of files

        executor.shutdown();
        while (!executor.isTerminated()) {
            // waiting for the threads to be finished
        }
        logger.info("Finished all threads.");
        return svmFeatFileDict;
    }

    /**
     * Extract SVM features
     * @param featDir feature file directory
     * @param annotationDir annotation file directory
     * @param dataDirTmp directory to save svm formatted file
     * @return feature file list svm formatted
     */
    private HashMap<String, String> extractFeatures4SVM(String featDir, String annotationDir, String dataDirTmp) {
        String libPath = new LibraryPath().getLibPath();
        HashMap<String, String> svmFeatFileDict = new HashMap<>();
        //format the features for SVMLight
        logger.info("Feature extraction step two is started...");
        String featExtract = libPath + "/" + this.configDict.get("SVM_feat");
        String dictFeat = libPath + "/" + this.configDict.get("FEAT_selected");
        List command = new ArrayList();
        command.add("python3.4");
        command.add(featExtract);
        command.add(dictFeat);
        command.add(featDir + "/");
        command.add(annotationDir + "/");
        command.add(dataDirTmp + "/");
        this.executeExternal(command, "svm_feat");

        //list svmlight formatted feature file will be used for classification
        File folder = new File(dataDirTmp);
        File[] listOfFiles = folder.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".dat");
            }
        });
        for (int i = 0; i < listOfFiles.length; i++) {
            String fileName = listOfFiles[i].getName().replace(".dat", "");
            String filePath = listOfFiles[i].getAbsolutePath();
            svmFeatFileDict.put(fileName, filePath);
        }
        return svmFeatFileDict;
    }

    /**
     * Run the external script
     *
     * @param command
     */
    private void executeExternal(List command, String log) {
        String libPath = new LibraryPath().getLibPath();
        try {
            ProcessBuilder builder = new ProcessBuilder(command);
            Process process = builder.start();
            builder.redirectErrorStream(true);
            process.waitFor();

//            FileOutputStream fosErr = new FileOutputStream(libPath + "/log/" + log + "_err.log");
//            // any error message?
//            StreamGobbler errorGobbler = new StreamGobbler(process.getErrorStream(), "LOG", fosErr);
//            // any output?
//            FileOutputStream fosOut = new FileOutputStream(libPath + "/log/" + log + "_out.log");
//            StreamGobbler outputGobbler = new StreamGobbler(process.getInputStream(), "OUTPUT", fosOut);
//            // kick them off
//            errorGobbler.start();
//            outputGobbler.start();
//
//            fosErr.flush();
//            fosErr.close();
//            fosOut.flush();
//            fosOut.close();
            process.destroy();
        } catch (IOException | InterruptedException ex) {
            logger.log(Level.SEVERE, "Problem in feature extraction.", ex);
        }
    }

    /**
     * Check the number of core in the system
     *
     * @return number of threads selected to run (75%)
     */
    private int getNumberOfThreads() {
        int selectedThreads = 1;
        try {
            int threads = Runtime.getRuntime().availableProcessors();
            if (threads > 1) {
                selectedThreads = (threads * 75) / 100;
            } else {
                selectedThreads = threads;
            }
            logger.log(Level.INFO, "Number of core {0} and selected to run {1}", new Object[]{threads, selectedThreads});
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Problem in reading number of processors.", ex);
        }
        return selectedThreads;
    }

}
