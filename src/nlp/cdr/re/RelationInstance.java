/*
 * This work by Firoj Alam is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License.
 * Permissions beyond the scope of this license may be available by sending an email to firojalam@gmail.com.
 * http://creativecommons.org/licenses/by-nc/4.0/deed.en_US
 * 
 */


package nlp.cdr.re;

/**
 *
 * @author Firoj Alam
 */
public class RelationInstance {
    private boolean classLab = false;
    private boolean isInGazetteers = false;
    private boolean isInSameSent = false;
    private boolean isInTitle = false;    
    private boolean isInAbstract = false;        
    private boolean isChemicalInducedDisease = false;
    private String entityChemicalID = null;
    private String entityChemicalType = null;
    private String entityDiseaseID = null;
    private String entityDiseaseType = null;    

    public boolean isClassLab() {
        return classLab;
    }

    public void setClassLab(boolean classLab) {
        this.classLab = classLab;
    }

    public boolean isIsInGazetteers() {
        return isInGazetteers;
    }

    public void setIsInGazetteers(boolean isInGazetteers) {
        this.isInGazetteers = isInGazetteers;
    }

    public String getEntityChemicalID() {
        return entityChemicalID;
    }

    public void setEntityChemicalID(String entityChemicalID) {
        this.entityChemicalID = entityChemicalID;
    }

    public String getEntityChemicalType() {
        return entityChemicalType;
    }

    public void setEntityChemicalType(String entityChemicalType) {
        this.entityChemicalType = entityChemicalType;
    }

    public String getEntityDiseaseID() {
        return entityDiseaseID;
    }

    public void setEntityDiseaseID(String entityDiseaseID) {
        this.entityDiseaseID = entityDiseaseID;
    }

    public String getEntityDiseaseType() {
        return entityDiseaseType;
    }

    public void setEntityDiseaseType(String entityDiseaseType) {
        this.entityDiseaseType = entityDiseaseType;
    }

    public boolean isIsInSameSent() {
        return isInSameSent;
    }

    public void setIsInSameSent(boolean isInSameSent) {
        this.isInSameSent = isInSameSent;
    }

    public boolean isIsChemicalInducedDisease() {
        return isChemicalInducedDisease;
    }

    public void setIsChemicalInducedDisease(boolean isChemicalInducedDisease) {
        this.isChemicalInducedDisease = isChemicalInducedDisease;
    }

    public boolean isIsInTitle() {
        return isInTitle;
    }

    public void setIsInTitle(boolean isInTitle) {
        this.isInTitle = isInTitle;
    }

    public boolean isIsInAbstract() {
        return isInAbstract;
    }

    public void setIsInAbstract(boolean isInAbstract) {
        this.isInAbstract = isInAbstract;
    }

    
    @Override
    public String toString() {
        return "RelationInstance{" + "classLab=" + classLab + ", isInGazetteers=" + isInGazetteers + ", isInSameSent=" + isInSameSent + ", isChemicalInducedDisease=" + isChemicalInducedDisease + ", entityChemicalID=" + entityChemicalID + ", entityChemicalType=" + entityChemicalType + ", entityDiseaseID=" + entityDiseaseID + ", entityDiseaseType=" + entityDiseaseType + '}';
    }
    
    
}
