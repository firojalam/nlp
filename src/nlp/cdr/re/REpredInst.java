/*
 * This work by Firoj Alam is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License.
 * Permissions beyond the scope of this license may be available by sending an email to firojalam@gmail.com.
 * http://creativecommons.org/licenses/by-nc/4.0/deed.en_US
 * 
 */


package nlp.cdr.re;

/**
 *
 * @author Firoj Alam
 */
public class REpredInst {
    private String docID = null;
    private String clsLabel = null;
    private String CID = "CID";
    private String chemicalID = null;
    private String diseaseID = null;

    public REpredInst() {
    }

    public String getDocID() {
        return docID;
    }

    public void setDocID(String docID) {
        this.docID = docID;
    }

    public String getClsLabel() {
        return clsLabel;
    }

    public void setClsLabel(String clsLabel) {
        this.clsLabel = clsLabel;
    }

    public String getChemicalID() {
        return chemicalID;
    }

    public void setChemicalID(String chemicalID) {
        this.chemicalID = chemicalID;
    }

    public String getDiseaseID() {
        return diseaseID;
    }

    public void setDiseaseID(String diseaseID) {
        this.diseaseID = diseaseID;
    }

    @Override
    public String toString() {
        return docID + "\t" + CID + "\t" + chemicalID + "\t" + diseaseID;
    }
    
    
}
