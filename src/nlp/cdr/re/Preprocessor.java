/*
 * This work by Firoj Alam is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License.
 * Permissions beyond the scope of this license may be available by sending an email to firojalam@gmail.com.
 * http://creativecommons.org/licenses/by-nc/4.0/deed.en_US
 * 
 */


package nlp.cdr.re;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.logging.Logger;
import nlp.cdr.dataanalysis.ChemicalDisease;
import nlp.cdr.dataanalysis.DictionaryReader;
import nlp.cdr.dataanalysis.Document;
import nlp.cdr.dataprepare.DataFormatterSent;
import nlp.cdr.dataprepare.ExtractSentRelation;
import nlp.cdr.dataprepare.SentData;
import nlp.utils.LibraryPath;


/**
 *
 * @author Firoj Alam
 */
public class Preprocessor {
    private static final Logger logger = Logger.getLogger(Preprocessor.class.getName());
    private HashMap<String, String> tokenFileDict = null;
    private HashMap<String, String> reDocumentAnnotationFileDict = null;
    private HashMap<String, String> reSentAnnotationFileDict = null;
    private LinkedHashMap<String, String> nerFileDict = null;
    public Preprocessor() {
    }
    
    public void prepareData(String inputDir, String outputDirTok,String outputDirAnnotation,DataReader dataReader, String tokForm){
        String libPath = new LibraryPath().getLibPath();        
        //reads token and NER content from input files
        LinkedHashMap<String, ArrayList<Token>> contentTokenDict = dataReader.processToken(dataReader.getContentFileDict());
        LinkedHashMap<String, Document> documentREDict = dataReader.processNEContent(dataReader.getNerFileDict());
        nerFileDict = dataReader.getNerFileDict();
        //reads MESH dictionary
        DictionaryReader readDict = new DictionaryReader();
        String dictionaryFileName = libPath+"/etc/CTD_chemicals_diseases.ser";
        HashMap<String, ChemicalDisease> chemicalDiseaseDict = readDict.readDictionaryObject(dictionaryFileName);
        
        //format and add some features with re instances
        DataFormatter dataFormatter = new DataFormatter();
        LinkedHashMap<String, ReDocument> relationInstDict = dataFormatter.processRE4Prediction(contentTokenDict, documentREDict, dataReader.getSourceDir(),chemicalDiseaseDict);
        
        //output *.token and *.annotation file for feature extraction
        ProcessOutput output = new ProcessOutput();
        HashMap<String, String> tokenFileDict = output.tokenOutput(contentTokenDict, outputDirTok,tokForm);
        HashMap<String, String> reAnnotationFileDict = output.relationInstanceOutput(relationInstDict, outputDirAnnotation);    
        this.tokenFileDict = tokenFileDict;
        this.reDocumentAnnotationFileDict = reAnnotationFileDict;
    }
    
    /**
     * Token file dictionary
     * @return 
     */
    public HashMap<String, String> getTokenFileDict() {
        return tokenFileDict;
    }
    /**
     * NER file dictionary
     * @return 
     */
    public LinkedHashMap<String, String> getNerFileDict() {
        return nerFileDict;
    }

    public void prepareDataMultiClassifier(String inputDir, String outputDirTok,String outputDocDirAnnotation,String outputSentDirAnnotation,DataReader dataReader, String tokForm){
        String libPath = new LibraryPath().getLibPath();        
        //reads token and NER content from input files
        LinkedHashMap<String, ArrayList<Token>> contentTokenDict = dataReader.processToken(dataReader.getContentFileDict());
        LinkedHashMap<String, Document> documentREDict = dataReader.processNEContent(dataReader.getNerFileDict());
        nerFileDict = dataReader.getNerFileDict();
        //reads MESH dictionary
        DictionaryReader readDict = new DictionaryReader();
        String dictionaryFileName = libPath+"/etc/CTD_chemicals_diseases.ser";
        HashMap<String, ChemicalDisease> chemicalDiseaseDict = readDict.readDictionaryObject(dictionaryFileName);
        
        //format and add some features with re instances
        DataFormatter dataFormatter = new DataFormatter();
        LinkedHashMap<String, ReDocument> relationInstDict = dataFormatter.processRE4Prediction(contentTokenDict, documentREDict, dataReader.getSourceDir(),chemicalDiseaseDict);
        
        //output *.token and *.annotation file for feature extraction
        ProcessOutput processOutput = new ProcessOutput();
        this.tokenFileDict = processOutput.tokenOutput(contentTokenDict, outputDirTok,tokForm);
        this.reDocumentAnnotationFileDict = processOutput.relationInstanceOutput(relationInstDict, outputDocDirAnnotation);    
        //this.tokenFileDict = tokenFileDict;
        //this.reDocumentAnnotationFileDict = reAnnotationFileDict;
        /**
         * Extract sentence label decisions
         * 
         */
        logger.info("Generating relation instances...");
        ExtractSentRelation sentRelation = new ExtractSentRelation();
        LinkedHashMap<String, ArrayList<SentData>> sentDataDict = sentRelation.extractSentWithRelation(documentREDict);        
        DataFormatterSent dataFormatterSent = new DataFormatterSent();
        LinkedHashMap<String, ArrayList<ReDocument>> relationInstDictSent = dataFormatterSent.processREwithGold(contentTokenDict, sentDataDict, dataReader.getSourceDir(),chemicalDiseaseDict, documentREDict);        
        this.reSentAnnotationFileDict = processOutput.relationInstanceOutput4Sent(relationInstDictSent, outputSentDirAnnotation);
        
    }
    /**
     * Annotation file dictionary for document classification
     * @return
     */
    public HashMap<String, String> getReDocumentAnnotationFileDict() {
        return reDocumentAnnotationFileDict;
    }

    /**
     * Annotation file dictionary for sentence classification
     * @return
     */
    public HashMap<String, String> getReSentAnnotationFileDict() {
        return reSentAnnotationFileDict;
    }
    
    
    /**
     * Calculates execution time of the whole system
     * @param d1
     * @param d2 
     */
    public static void duration(Date d1, Date d2){
        String timeTaken = "";
        try {
            long diff = d2.getTime() - d1.getTime();
            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);
            timeTaken = diffDays + " days, " + diffHours + " hours, " + diffMinutes + " minutes, " + diffSeconds + " seconds.";
        } catch (Exception ex) {
            System.out.println("Time cal problem.");
        }
        System.out.println("Time taken: " + timeTaken);
        System.out.println("Done..");    
    }    
    /**
     * 
     * @param args 
     */
    public static void main(String[] args){
        Date d1 = new Date();
        System.out.println("System started at: " + d1);

        //String inFileName = args[0];
//        String goldREfile = args[1];
//        String path="/Users/firojalam/Study_PhD_projects/CDR/data/TrainingSet_7_15_15/";
//        String goldREfile = "/Users/firojalam/Study_PhD_projects/CDR/data/CDR_Training/CDR_TrainingSet.PubTator.txt";
        String path="/Users/firojalam/Study_PhD_projects/CDR/data/DevelopmentSet_7_15_15/";
        String goldREfile = "/Users/firojalam/Study_PhD_projects/CDR/data/CDR_Dev/CDR_DevelopmentSet.PubTator.txt";

        String dictionaryFileName = "/Users/firojalam/Study_PhD_projects/CDR/data/CTD_chemicals_diseases.tsv";
        DataReader dataReader = new DataReader();
        dataReader.readDir(path);
        
        
        LinkedHashMap<String, ArrayList<Token>> contentTokenDict = dataReader.processToken(dataReader.getContentFileDict());
        LinkedHashMap<String, Document> documentREDict = dataReader.processNEContent(dataReader.getNerFileDict());
        LinkedHashMap<String, Document> goldRE = dataReader.readGoldRE(goldREfile);
        
        DataFormatter dataFormatter = new DataFormatter();
        DictionaryReader readDict = new DictionaryReader();
        HashMap<String, ChemicalDisease> chemicalDiseaseDict = readDict.readDictionary(dictionaryFileName);
        //HashMap<String, ChemicalDisease> chemicalDiseaseDict = null;        
        
        LinkedHashMap<String, ReDocument> relationInstDict = dataFormatter.processREwithGold(contentTokenDict, documentREDict, dataReader.getSourceDir(),chemicalDiseaseDict, goldRE);
        
        ProcessOutput output = new ProcessOutput();
//        output.tokenOutput(contentTokenDict, path);
//        output.relationInstanceOutput(relationInstDict, path);
        Date d2 = new Date();
        duration(d1,d2);
    }     
}
