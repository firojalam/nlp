/*
 * This work by Firoj Alam is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License.
 * Permissions beyond the scope of this license may be available by sending an email to firojalam@gmail.com.
 * http://creativecommons.org/licenses/by-nc/4.0/deed.en_US
 * 
 */
package nlp.cdr.re;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import nlp.cdr.dataanalysis.Mention;
import nlp.utils.Write2File;

/**
 *
 * @author Firoj Alam
 */
public class ProcessOutput {

    private static final Logger logger = Logger.getLogger(ProcessOutput.class.getName());

    //Logger logger = LogManager.getLogger(LogManager.ROOT_LOGGER_NAME);
    /**
     * Print token info to file
     *
     * @param contentTokenDict
     * @param dirName
     * @param tokenForm
     * @return
     */
    public HashMap<String, String> tokenOutput(LinkedHashMap<String, ArrayList<Token>> contentTokenDict, String dirName, String tokenForm) {
        HashMap<String, String> tokenFileDict = new HashMap<>();
        StringBuilder text = null;
        for (Map.Entry<String, ArrayList<Token>> entrySet : contentTokenDict.entrySet()) {
            Write2File wrt = new Write2File();
            try {
                text = new StringBuilder();
                String fileID = entrySet.getKey();
                ArrayList<Token> tokenList = entrySet.getValue();
                String dirPath = new File(dirName).getAbsolutePath();
                String outputFile = dirPath + "/" + fileID + ".token";
                tokenFileDict.put(fileID, outputFile);
                wrt.openFile(outputFile);
                for (int i = 0; i < tokenList.size(); i++) {
                    Token token = tokenList.get(i);
                    if (tokenForm.equalsIgnoreCase("token")) {
                        if (token.getTokenPosition() == null) {
                            text.append(token.getToken()).append("\n"); //end of sent
                        } else {
                            if (token.getTokenPos().equals("CD")) {
                                text.append("CD").append("\t");    //token
                                text.append(token.getTokenPos()).append("\n");    //pos  
                            } else {
                                text.append(token.getToken()).append("\t");    //token
                                text.append(token.getTokenPos()).append("\n");    //pos         
                            }
                        }
                    } else if (tokenForm.equalsIgnoreCase("lemma")) {
                        if (token.getTokenPosition() == null) {
                            text.append(token.getToken()).append("\n"); //end of sent
                        } else {
                            if (token.getTokenPos().equals("CD")) {
                                text.append("CD").append("\t");    //token
                                text.append(token.getTokenPos()).append("\n");    //pos  
                            } else {
                                text.append(token.getLemma()).append("\t");    //lemma
                                text.append(token.getTokenPos()).append("\n");    //pos 
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                logger.log(Level.SEVERE, "Something is wrong while reading token info.", ex);
            }

            wrt.write2File(text.toString());
            wrt.write2File(">>>>>>>END OF SENTENCE\n");
            wrt.closeFile();
        }// end number of documents
        return tokenFileDict;
    }

    /**
     *
     * @param relationInstDict
     * @param dirName
     */
    public HashMap<String, String> relationInstanceOutput(LinkedHashMap<String, ReDocument> relationInstDict, String dirName) {
        HashMap<String, String> reFileDict = new HashMap<>();
        StringBuilder text = null;
        for (Map.Entry<String, ReDocument> entrySet : relationInstDict.entrySet()) {
            String docID = entrySet.getKey();
            ReDocument relationDoc = entrySet.getValue();
            Write2File wrt = new Write2File();
            String dirPath = new File(dirName).getAbsolutePath();
            String outputFile = dirPath + "/" + docID + ".annotation";
            reFileDict.put(docID, outputFile);
            wrt.openFile(outputFile);
            text = new StringBuilder();
            ArrayList<RelationInstance> relationInst = relationDoc.getRelationList();
            LinkedHashMap<String, Entity> entityList = relationDoc.getEntityList();
            for (Map.Entry<String, Entity> entrySet1 : entityList.entrySet()) {
                try {
                    String entityID = entrySet1.getKey();
                    Entity entity = entrySet1.getValue();
                    text.append(entity.getEntityID()).append(" ").append(entity.getMentionList().size()).append(" ");
                    for (int i = 0; i < entity.getMentionList().size(); i++) {
                        Mention mention = entity.getMentionList().get(i);
                        text.append(mention.getStartIndex()).append(" ");
                        text.append(mention.getEndIndex()).append(" ");
                    }
                    text.append("\n");
                } catch (Exception ex) {
                    logger.log(Level.SEVERE, "Something is wrong while reading entities of relation.", ex);
                }
            }
            text.append("RELATION\n");
            for (int i = 0; i < relationInst.size(); i++) {
                try {
                    RelationInstance relation = relationInst.get(i);
                    String cls = "";
                    if (relation.isClassLab()) {
                        cls = "1";
                    } else {
                        cls = "-1";
                    }
                    text.append(cls).append(" ");
                    text.append("CID").append(" ");
                    text.append(relation.getEntityChemicalID()).append(" ").append(relation.getEntityChemicalType()).append(" ");
                    text.append(relation.getEntityDiseaseID()).append(" ").append(relation.getEntityDiseaseType()).append(" ");
                    String gz = "";
                    if (relation.isIsInGazetteers()) {
                        gz = "1";
                    } else {
                        gz = "-1";
                    }
                    text.append(gz).append(" ");
                    String sameSent = "";
                    if (relation.isIsInSameSent()) {
                        sameSent = "1";
                    } else {
                        sameSent = "-1";
                    }
                    text.append(sameSent).append(" ");
                    String title = "";
                    if (relation.isIsInTitle()) {
                        title = "1";
                    } else {
                        title = "-1";
                    }
                    text.append(title).append(" ");
                    String abs = "";
                    if (relation.isIsInAbstract()) {
                        abs = "1";
                    } else {
                        abs = "-1";
                    }
                    text.append(abs).append("\n");

                } catch (Exception ex) {
                    logger.log(Level.SEVERE, "Something is wrong while writing relation instances in file.", ex);
                }
            }
            wrt.write2File(text.toString().trim() + "\n");
            wrt.closeFile();
        }//for each document
        return reFileDict;
    }

    /**
     * Print token info to file
     *
     * @param contentTokenDict
     * @param dirName
     * @param tokenForm
     * @return
     */
    public HashMap<String, String> tokenOutput4Sent(LinkedHashMap<String, ArrayList<Token>> contentTokenDict, String dirName, String tokenForm, HashMap<String, String> reFileDict) {
        //HashMap<String, String> tokenOutput4Sent(LinkedHashMap<String, HashMap<Integer, ArrayList<Token>>> contentTokenDict, String dirName, String tokenForm, HashMap<String, String> reFileDict)
        //
        HashMap<String, String> tokenFileDict = new HashMap<>();
        StringBuilder text = null;

//        for (Map.Entry<String, String> entrySet : reFileDict.entrySet()) {
//            String fileID = entrySet.getKey();
//            String arr[] = fileID.split("_");
//            String docID = arr[0];
//            int sentID = Integer.parseInt(arr[1]);
        Write2File wrt = null;

        //HashMap<Integer, ArrayList<Token>> sentTokenDict = contentTokenDict.get(docID);
        for (Map.Entry<String, ArrayList<Token>> entrySet : contentTokenDict.entrySet()) {
            try {
                text = new StringBuilder();

                //  Integer sentID = entrySet1.getKey();
//                if(!sentTokenDict.containsKey(sentID)){
//                    System.out.println("Not found\t"+docID+"\t"+sentID);
//                    continue;
//                }
                String docID = entrySet.getKey();
                ArrayList<Token> tokenList = contentTokenDict.get(docID);
                String dirPath = new File(dirName).getAbsolutePath();
                String outputFile = dirPath + "/" + docID + ".token";
                tokenFileDict.put(docID, outputFile);
                wrt = new Write2File();
                wrt.openFile(outputFile);
                for (int i = 0; i < tokenList.size(); i++) {
                    Token token = tokenList.get(i);
                    if (tokenForm.equalsIgnoreCase("token")) {
                        if (token.getTokenPosition() == null) {
                            text.append(token.getToken()).append("\n"); //end of sent
                        } else {
                            if (token.getTokenPos().equals("CD")) {
                                text.append("CD").append("\t");    //token
                                text.append(token.getTokenPos()).append("\n");    //pos  
                            } else {
                                text.append(token.getToken()).append("\t");    //token
                                text.append(token.getTokenPos()).append("\n");    //pos         
                            }
                        }
                    } else if (tokenForm.equalsIgnoreCase("lemma")) {
                        if (token.getTokenPosition() == null) {
                            text.append(token.getToken()).append("\n"); //end of sent
                        } else {
                            if (token.getTokenPos().equals("CD")) {
                                text.append("CD").append("\t");    //token
                                text.append(token.getTokenPos()).append("\n");    //pos  
                            } else {
                                text.append(token.getLemma()).append("\t");    //lemma
                                text.append(token.getTokenPos()).append("\n");    //pos 
                            }
                        }
                    }
                }
                wrt.write2File(text.toString());
                wrt.write2File(">>>>>>>END OF SENTENCE\n");
                wrt.closeFile();
                // }//end for each sent

            } catch (Exception ex) {
                logger.log(Level.SEVERE, "Something is wrong while reading token info." + ex);
            }

        }// end number of documents
        return tokenFileDict;
    }

    /**
     *
     * @param relationInstDict
     * @param dirName
     * @return
     */
    public HashMap<String, String> relationInstanceOutput4Sent(LinkedHashMap<String, ArrayList<ReDocument>> relationInstDict, String dirName) {
        HashMap<String, String> reFileDict = new HashMap<>();
        StringBuilder text = null;
        for (Map.Entry<String, ArrayList<ReDocument>> entrySet : relationInstDict.entrySet()) {
            String docID = entrySet.getKey();
            ArrayList<ReDocument> relationDocList = entrySet.getValue();
            if(relationDocList.isEmpty()){
                //System.err.println("No sentence level relation found for document "+docID);
                continue;
            } 
            Write2File wrt = new Write2File();
            String dirPath = new File(dirName).getAbsolutePath();
            String outputFile = dirPath + "/" + docID + ".annotation";
            reFileDict.put(docID, outputFile);
            wrt.openFile(outputFile);
            text = new StringBuilder();

            for (int i = 0; i < relationDocList.size(); i++) {
                ReDocument relationDoc = relationDocList.get(i);
                LinkedHashMap<String, Entity> entityList = relationDoc.getEntityList();
                String txt = this.extractRelationEntityData(entityList);
                text.append(txt);
            }
            text.append("RELATION\n");
            for (int i = 0; i < relationDocList.size(); i++) {
                ReDocument relationDoc = relationDocList.get(i);
                ArrayList<RelationInstance> relationInst = relationDoc.getRelationList();
                String txt = this.extractRelationInstData(relationInst);
                text.append(txt);
            }
            wrt.write2File(text.toString().trim() + "\n");
            wrt.closeFile();
        }//for each document
        return reFileDict;
    }

    /**
     * Extract relation for each sent
     * @param relationInst
     * @param entityList
     * @return 
     */
    private String extractRelationEntityData(LinkedHashMap<String, Entity> entityList) {
        StringBuilder text = new StringBuilder();
        for (Map.Entry<String, Entity> entrySet1 : entityList.entrySet()) {
            try {
                String entityID = entrySet1.getKey();
                Entity entity = entrySet1.getValue();
                text.append(entity.getEntityID()).append(" ").append(entity.getMentionList().size()).append(" ");
                for (int i = 0; i < entity.getMentionList().size(); i++) {
                    Mention mention = entity.getMentionList().get(i);
                    text.append(mention.getStartIndex()).append(" ");
                    text.append(mention.getEndIndex()).append(" ");
                }
                text.append("\n");
            } catch (Exception ex) {
                logger.log(Level.SEVERE, "Something is wrong while reading entities of relation.", ex);
            }
        }
        return text.toString();
    }
    /**
     * 
     * @param relationInst
     * @return 
     */
    private String extractRelationInstData(ArrayList<RelationInstance> relationInst) {
        StringBuilder text = new StringBuilder();
            for (int i = 0; i < relationInst.size(); i++) {
            try {
                RelationInstance relation = relationInst.get(i);
                String cls = "";
                if (relation.isClassLab()) {
                    cls = "1";
                } else {
                    cls = "-1";
                }
                text.append(cls).append(" ");
                text.append("CID").append(" ");
                text.append(relation.getEntityChemicalID()).append(" ").append(relation.getEntityChemicalType()).append(" ");
                text.append(relation.getEntityDiseaseID()).append(" ").append(relation.getEntityDiseaseType()).append(" ");
                String gz = "";
                if (relation.isIsInGazetteers()) {
                    gz = "1";
                } else {
                    gz = "-1";
                }
                text.append(gz).append("\n");
//                    String sameSent = "";
//                    if (relation.isIsInSameSent()) {
//                        sameSent = "1";
//                    } else {
//                        sameSent = "-1";
//                    }
//                    text.append(sameSent).append(" ");
//                    String title = "";
//                    if (relation.isIsInTitle()) {
//                        title = "1";
//                    } else {
//                        title = "-1";
//                    }
//                    text.append(title).append(" ");
//                    String abs = "";
//                    if (relation.isIsInAbstract()) {
//                        abs = "1";
//                    } else {
//                        abs = "-1";
//                    }
//                    text.append(abs).append("\n");

            } catch (Exception ex) {
                logger.log(Level.SEVERE, "Something is wrong while writing relation instances in file.", ex);
            }
        }
        return text.toString();
    }    

    /**
     * Writes data for svm
     * @param svmFeatFileDict
     * @param outputSVM 
     */
    public void writeData4SVM(HashMap<String, String> svmFeatFileDict, String outputSVM) {
        Write2File wrt = new Write2File();
        wrt.openFile(outputSVM);
        for (Map.Entry<String, String> entrySet : svmFeatFileDict.entrySet()) {
            try {
                //String docID = entrySet.getKey();
                String file = entrySet.getValue();
                //String content = new Scanner(new File(file)).useDelimiter("\\Z").next();
                String content = this.readTextFromFile(file);
                if (content.isEmpty()){
                    System.err.println("Empty file: "+file);
                }else {
                    wrt.write2File(content.trim()+"\n");
                }
            } catch (Exception ex) {
                logger.log(Level.SEVERE, "Problem in reading file", ex);
            }
        }
        wrt.closeFile();
    }
    
    /**
     * Reads NER output from a file.
     *
     * @param fileName name of the file with absolute path
     * @return content of the file
     */
    private String readTextFromFile(String fileName) {
        StringBuilder text = new StringBuilder();
        try {
            BufferedReader fileRead = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
            String str = "";
            while ((str = fileRead.readLine()) != null) {
                str = str.trim();
                //if(!str.isEmpty())
                text.append(str).append("\n");
            }//end read file
            fileRead.close();
        } catch (FileNotFoundException ex) {
            logger.log(Level.SEVERE, "Please check your output file. It is not available.", ex);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Something is wrong while reading the file. Please check the stack-trace.", ex);
        }
        return text.toString().trim();

    }    

    /**
     * Writes all pubtator file output into a single file.
     * @param outFileList
     * @param outputFile 
     */
    public void writePubTatorData(ArrayList<String> outFileList, String outputFile) {
        Write2File wrt = new Write2File();
        wrt.openFile(outputFile);
        for (int i = 0; i < outFileList.size(); i++) {
            String file = outFileList.get(i);
            String content = this.readTextFromFile(file);
            wrt.write2File(content.trim()+"\n");
        }
        wrt.closeFile();
    }
}
