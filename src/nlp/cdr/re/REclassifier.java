/*
 * This work by Firoj Alam is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License.
 * Permissions beyond the scope of this license may be available by sending an email to firojalam@gmail.com.
 * http://creativecommons.org/licenses/by-nc/4.0/deed.en_US
 * 
 */
package nlp.cdr.re;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import nlp.utils.LibraryPath;
import nlp.utils.StreamGobbler;

/**
 *
 * @author Firoj Alam
 */
public class REclassifier {

    private static final Logger logger = Logger.getLogger(REclassifier.class.getName());

    public HashMap<String, String> classifier(HashMap<String, String> svmFeatFileDict, HashMap<String, String> config, String dataDir, String model) {
        String libPath = new LibraryPath().getLibPath();
        HashMap<String, String> classifiedFileDict = new HashMap<>();
        //String classifier = libPath+"/"+config.get("SVM_linux");
        String classifier = "";
        if (this.getOSInfo().equals("mac")) {
            classifier = libPath + "/" + config.get("SVM_mac").toString();
        } else if (this.getOSInfo().equals("linux")) {
            classifier = libPath + "/" + config.get("SVM_linux").toString();
        }
        //String model = libPath+"/"+config.get("model"); 
        for (Map.Entry<String, String> entrySet : svmFeatFileDict.entrySet()) {
            String docID = entrySet.getKey();
            String docFilePath = entrySet.getValue();
            String outputFile = dataDir + "/" + docID + "_pred.txt";
            classifiedFileDict.put(docID, outputFile);
            try {
                List command = new ArrayList();
                command.add(classifier);
                command.add(docFilePath);
                command.add(model);
                command.add(outputFile);
                ProcessBuilder builder = new ProcessBuilder(command);
                Process process = builder.start();
                builder.redirectErrorStream(true);
                process.waitFor();
//                FileOutputStream fosErr = new FileOutputStream(libPath + "/log/svm_classify_error.log");
//                // any error message?
//                StreamGobbler errorGobbler = new StreamGobbler(process.getErrorStream(), "LOG", fosErr);
//                // any output?
//                FileOutputStream fosOut = new FileOutputStream(libPath + "/log/svm_classify_out.log");
//                StreamGobbler outputGobbler = new StreamGobbler(process.getInputStream(), "OUTPUT", fosOut);
//                // kick them off
//                errorGobbler.start();
//                outputGobbler.start();
//
//                fosErr.flush();
//                fosErr.close();
//                fosOut.flush();
//                fosOut.close();
                process.destroy();
            } catch (Exception ex) {
                logger.log(Level.SEVERE, "Problem in classification.", ex);
            }
        }
        return classifiedFileDict;
    }

    /**
     * It detects the OS on the system it is running
     *
     * @return os information
     */
    private String getOSInfo() {
        String osInfo = "";
        String str = System.getProperty("os.name");
        if (str.startsWith("Windows")) {
            osInfo = "windows";
        } else if (str.startsWith("Mac")) {
            osInfo = "mac";
        } else if (str.startsWith("Linux")) {
            osInfo = "linux";
        }
        return osInfo;
    }

}
