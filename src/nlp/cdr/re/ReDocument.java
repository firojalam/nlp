/*
 * This work by Firoj Alam is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License.
 * Permissions beyond the scope of this license may be available by sending an email to firojalam@gmail.com.
 * http://creativecommons.org/licenses/by-nc/4.0/deed.en_US
 * 
 */
package nlp.cdr.re;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 *
 * @author Firoj Alam
 */
public class ReDocument {

    private LinkedHashMap<String, Entity> entityList = new LinkedHashMap<>();
    private ArrayList<RelationInstance> relationList = new ArrayList<>();

    public ReDocument() {
    }

    public LinkedHashMap<String, Entity> getEntityList() {
        return entityList;
    }

    public void setEntityList(LinkedHashMap<String, Entity> entityList) {
        this.entityList = entityList;
    }

    public ArrayList<RelationInstance> getRelationList() {
        return relationList;
    }

    public void setRelationList(ArrayList<RelationInstance> relationList) {
        this.relationList = relationList;
    }

}
