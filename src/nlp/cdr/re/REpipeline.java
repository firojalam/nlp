/*
 * This work by Firoj Alam is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License.
 * Permissions beyond the scope of this license may be available by sending an email to firojalam@gmail.com.
 * http://creativecommons.org/licenses/by-nc/4.0/deed.en_US
 * 
 */
package nlp.cdr.re;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import nlp.utils.LibraryPath;
import nlp.utils.ParseCommands;
import nlp.utils.ReadConfig;
import org.apache.commons.cli.CommandLine;

/**
 * Pipeline for RE extraction process, Takes NE output from NER system and then
 * preprocess, extract features for RE
 *
 * @author Firoj Alam
 */
public class REpipeline {

    private static final Logger logger = Logger.getLogger(REpipeline.class.getName());

    /**
     * Calculates execution time of the whole system
     *
     * @param d1
     * @param d2
     */
    public static void duration(Date d1, Date d2) {
        String timeTaken = "";
        try {
            long diff = d2.getTime() - d1.getTime();
            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);
            timeTaken = diffDays + " days, " + diffHours + " hours, " + diffMinutes + " minutes, " + diffSeconds + " seconds.";
        } catch (Exception ex) {
            logger.info("Time cal problem.");
        }
        logger.log(Level.INFO, "Time taken: {0}", timeTaken);
        logger.info("Done..");
    }

    /**
     * Pipeline for RE extraction process
     *
     * @param args
     */
    public static void main(String args[]) {
        Date d1 = new Date();
        logger.log(Level.INFO, "System started at: {0}", d1);
        //String inputDir = "/home/firoj.alam/JavaProjects/NLP_versions/tmp/preprocessed_file_X_firoj/"; //preprocessed_file_X_firoj/
        //String configFile = "config.txt";
        //String tokForm = "lemma";

        ParseCommands cmds = new ParseCommands();
        CommandLine cmdDict = cmds.parseCommands(args);
        String inputDir = cmdDict.getOptionValue("i");
        String configFile = cmdDict.getOptionValue("c");
        String tokForm = cmdDict.getOptionValue("t");

        /////// Read config
        ReadConfig config = new ReadConfig(configFile);
        HashMap<String, String> configDict = config.getConfigDict();

        ///////Required library path
        String libPath = new LibraryPath().getLibPath();
        //String dataDir = libPath + "/featextractor/data";
        //String dataDirTmp = libPath + "/featextractor/data/tmp";
        String outputDirTok = libPath + "/featextractor/data/input";
        String outputDirAnnotation = libPath + "/featextractor/data/annotation";
        String featDir = libPath + "/featextractor/data/features/doc";

        /**
         * 1. Make sure that token and annotation dir are empty 2. Then run
         * preprocessor
         */
        logger.info("Cleaning the input and annotation directories...");
        try {
            File dir = new File(outputDirTok);
            for (File file : dir.listFiles()) {
                file.delete();
            }
            dir = new File(outputDirAnnotation);
            for (File file : dir.listFiles()) {
                file.delete();
            }
            dir = new File(featDir);
            //for(File file: dir.listFiles()) file.delete();            
            //dir = new File(dataDirTmp);
            for (File file : dir.listFiles()) {
                file.delete();
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Problem while deleting files.", ex);
        }
        logger.info("Preparing data for feature extraction...");
        Preprocessor preprocessor = new Preprocessor();
        DataReader dataReader = new DataReader();
        dataReader.readDirClassification(inputDir);
        preprocessor.prepareData(inputDir, outputDirTok, outputDirAnnotation, dataReader, tokForm);

        /**
         * Extract features using Anna's feature extraction system
         */
        logger.info("Extractiong features...");
        REfeatureExtractor featureExtractor = new REfeatureExtractor(configDict);
        HashMap<String, String> svmFeatFileDict = featureExtractor.extractFeatures(preprocessor.getTokenFileDict(), featDir, preprocessor.getReDocumentAnnotationFileDict());

        /**
         * Use the classifier to get the labels
         */
        logger.info("Classifying relations...");

        REclassifier classifier = new REclassifier();
        String model = libPath + "/" + configDict.get("model_sent");
        HashMap<String, String> classifiedFileDict = classifier.classifier(svmFeatFileDict, configDict, featDir, model);

        /**
         * Post-processing
         */
        logger.info("Postprocessing documents...");
        Postprocessor postprocessor = new Postprocessor(configDict);
        //postprocessor.processFilesInPubTatorFormat(preprocessor.getNerFileDict(), preprocessor.getReAnnotationFileDict(), classifiedFileDict, inputDir);
        postprocessor.processStreamInPubTatorFormat(preprocessor.getNerFileDict(), preprocessor.getReDocumentAnnotationFileDict(), classifiedFileDict, inputDir);

        Date d2 = new Date();
        duration(d1, d2);

    }
}
