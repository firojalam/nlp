/*
 * This work by Firoj Alam is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License.
 * Permissions beyond the scope of this license may be available by sending an email to firojalam@gmail.com.
 * http://creativecommons.org/licenses/by-nc/4.0/deed.en_US
 * 
 */


package nlp.cdr.re;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import nlp.cdr.dataanalysis.DictionaryReader;
import nlp.cdr.dataanalysis.Document;
import nlp.cdr.dataanalysis.Mention;
import static nlp.cdr.dataanalysis.Pipeline.duration;
import nlp.utils.LibraryPath;
import nlp.utils.ReadConfig;

/**
 * Search for RE in the training dataset, if that is available in the unknown 
 * data point too add that in the final RE set.
 * 1. Reads pubtator formatted NE and RE annotated document and make a Document object. 
 *  From the mention-list of the document we can design unique relations.
 *  Filter the relation that is already identified by the RE classifier.
 * 
 * 2. Read the Dataset RE dictionary as a form of <ChemicalDisease> object. 
 * 3. Match document's RE list with dictionary, if exist add it to the document OR leave as it is.
 * 4. 
 * @author Firoj Alam
 */

public class RELookUp {
    private static final Logger logger = Logger.getLogger(RELookUp.class.getName());
    private HashMap<String, String> configDict = null;
    private HashSet<String> reListR = null;
    /**
     * 
     * @param config 
     */
    public RELookUp(HashMap<String, String> config) {
        this.configDict = config;
        reListR = this.readDictionaryObject();
    }
    
    /**
     * Checks whether training set has any relation that matches with classified/predicted entity pair.
     * If relation is already found by the classifier then no need check. If not then check the relation 
     * in the training set relation pair. Then it add a new relation if it matches the entity pair with a relation.
     * @param dataDict Data dictionary in which relation has to add
     * @param reListData relation dictionary form the gold-label data either training OR train and dev set
     * @return Data dictionary with added relation.
     */
    public LinkedHashMap<String, Document> readRE(LinkedHashMap<String, Document> dataDict,HashSet<String> reListData){
        
        for (Map.Entry<String, Document> entrySet : dataDict.entrySet()) {
            String docID = entrySet.getKey();
            Document document = entrySet.getValue();
            ArrayList<Mention> mentionList = document.getMentionList(); 
            HashSet<String> reList = this.getCIDInst(document.getCIDList());
            LinkedHashSet<String> chemicalList = this.getCIDList(mentionList, "Chemical");
            LinkedHashSet<String> diseaseList = this.getCIDList(mentionList, "Disease");
            
            ////// Check chemical - disease pair
            HashSet<String> chemicalDiseaseList = this.getChemicalDiseaseSet(chemicalList,diseaseList);
            for (Iterator<String> iterator = chemicalDiseaseList.iterator(); iterator.hasNext();) {
                String chemicalDisease = iterator.next();
               if(reList.contains(chemicalDisease)){
                       continue;
                   }
                   ///// is it in gold-label dataset??
                   else if(reListData.contains(chemicalDisease)){
                       ArrayList<String> cidNew = new ArrayList<>();
                       String arr[] = chemicalDisease.split("-");
                       String chemical = arr[0];
                       String disease = arr[1];
                       cidNew.add(chemical.trim());
                       cidNew.add(disease.trim());
                       document.getCIDList().add(cidNew);
                       logger.info("added new relation...");
                    } 
            }
            //////Check disease - chemical pair
            chemicalDiseaseList = this.getChemicalDiseaseSet(diseaseList,chemicalList);
            for (Iterator<String> iterator = chemicalDiseaseList.iterator(); iterator.hasNext();) {
                String chemicalDisease = iterator.next();
               if(reList.contains(chemicalDisease)){
                       continue;
                   }
                   ///// is it in gold-label dataset??
                   else if(reListData.contains(chemicalDisease)){
                       ArrayList<String> cidNew = new ArrayList<>();
                       String arr[] = chemicalDisease.split("-");
                       String chemical = arr[1];
                       String disease = arr[0];
                       cidNew.add(chemical.trim());
                       cidNew.add(disease.trim());
                       document.getCIDList().add(cidNew);
                       logger.info("added new relation...");
                    } 
            }                   
        }//for all documents
        logger.info("Checked relations in gold relation set...");
        return dataDict;                
    }
    
    /**
     * 
     * @param chemicalList
     * @param diseaseList
     * @return 
     */
    private HashSet<String> getChemicalDiseaseSet(LinkedHashSet<String> chemicalList, LinkedHashSet<String> diseaseList) {
        HashSet<String> chemicalDiseaseList = new HashSet<>();
        for (Iterator<String> iterator = chemicalList.iterator(); iterator.hasNext();) {
            String chemical = iterator.next();
            for (Iterator<String> iterator1 = diseaseList.iterator(); iterator1.hasNext();) {
                String disease = iterator1.next();
                String chemicalDisease = chemical.trim() + "-" + disease.trim();
                chemicalDiseaseList.add(chemicalDisease);
                //chemicalDisease = disease.trim() + "-" + chemical.trim();
                //chemicalDiseaseList.add(chemicalDisease);
            }
        }
        return chemicalDiseaseList;
    }
    
    /**
     * 
     * @param cidList
     * @return 
     */
    private HashSet<String> getCIDInst(ArrayList<ArrayList<String>> cidList) {
        HashSet<String> reList = new HashSet<>();
        for (ArrayList<String> cid : cidList) {
            String chemicalDisease = cid.get(0) + "-" + cid.get(1);
            reList.add(chemicalDisease);
        }
        return reList;
    }
    
    /**
     * Match the entity-type
     *
     * @param mentionList
     * @param cid entity-type
     * @return associated entity-type list
     */
    private LinkedHashSet<String> getCIDList(ArrayList<Mention> mentionList, String cid) {
        LinkedHashSet<String> cidList = new LinkedHashSet<>();
        for (int i = 0; i < mentionList.size(); i++) {
            Mention mention = mentionList.get(i);
            if (mention.getMention().equals(cid)) {
                cidList.add(mention.getMentionID());
            }
        }
        return cidList;
    }

    /**
     * Reads RE dictionary that was designed using train+dev dataset.
     * @return HashSet object 
     */
    private HashSet<String> readDictionaryObject() {
        HashSet<String> dataREDict = new HashSet<>();
        String libPath = new LibraryPath().getLibPath();
        String fileName = libPath+"/"+this.configDict.get("RE_dict");
        try {
            RandomAccessFile raf = new RandomAccessFile(fileName, "r");
            FileInputStream fileIn = new FileInputStream(raf.getFD());
            Input input = new Input(fileIn);
          //Read
            Kryo kryo = new Kryo();
            //MapSerializer serializer = new MapSerializer();
            kryo.register(HashSet.class);
            dataREDict = kryo.readObject(input, HashSet.class);
            input.close();
            fileIn.close();
            fileIn.close();
        } catch (FileNotFoundException ex) {
            logger.log(Level.SEVERE, "File is not available, please check the stack-trace.", ex);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Something is wrong with parsing the file, please check.", ex);
        }
        return dataREDict;

    } 

    public HashSet<String> getReListR() {
        return reListR;
    }
  
    
    /**
     * 
     * @param args 
     */
    public static void main(String[] args) {
        // ChemicalDiseaseDict dict = null; 
        try {
            Date d1 = new Date();
            //String fileName = "/home/firoj.alam/Study_PhD_projects/CDR/data/CDR_Training/CDR_TrainingSet.PubTator.txt";
            //String fileName = "/home/firoj.alam/Study_PhD_projects/CDR/data/CDR_Dev/CDR_DevelopmentSet.PubTator.txt";
            String fileName = "/home/firoj.alam/JavaProjects/NLP_versions/tmp/ner_output2.txt";
            /////// Read config
            String configFile = "config.txt";
            ReadConfig config = new ReadConfig(configFile);
            HashMap<String, String> configDict = config.getConfigDict();

            
            RELookUp relookUP = new RELookUp(configDict);
            //HashSet<String> reListR = relookUP.readDictionaryObject();
            System.out.println("Size "+relookUP.reListR.size());

            nlp.cdr.dataanalysis.DataReader dataReader = new nlp.cdr.dataanalysis.DataReader();
            LinkedHashMap<String, Document> dataDict = dataReader.readData(fileName);
            relookUP.readRE(dataDict,relookUP.reListR);
            
            Date d2 = new Date();
            duration(d1, d2);

        } catch (Exception ex) {
            Logger.getLogger(DictionaryReader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    


}
