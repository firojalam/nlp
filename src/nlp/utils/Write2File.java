/**
 * This work is licensed under a Creative Commons
 * Attribution-NonCommercial-ShareAlike 3.0 Unported License. <br/>
 * http://creativecommons.org/licenses/by-nc-sa/3.0/
 */
package nlp.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import nlp.cdr.re.Postprocessor;

/**
 * Driver class to print text/string to a file.
 * <br/>
 *
 * @author Firoj Alam (firojalam@gmail.com)
 * @version $Revision: 0.1 1/14/2014 $
 */
public class Write2File {
    private static final Logger logger = Logger.getLogger(Write2File.class.getName());
    public BufferedWriter fileWrite = null;

    /**
     * Default constructor
     */
    public Write2File() {

    }

    /**
     * This function create a file specified by a file-name.
     *
     * @param fileName - name of the file to create
     */
    public void openFile(String fileName) {
        try {
            fileWrite = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName)));

        } catch (FileNotFoundException ex) {
            //System.out.println("Problem in opening log info file.");
            logger.log(Level.SEVERE, "Please check your output file. It is not available.", ex);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Something is wrong while opening file. Please check the stack-trace.", ex);
        }
    }

    /**
     * Writes text to the end of the file
     *
     * @param str - string or text to write to the file
     */
    public void write2File(String str) {
        try {

            this.fileWrite.write(str);

        } catch (FileNotFoundException ex) {
            logger.log(Level.SEVERE, "Please check your output file. It is not available.", ex);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Something is wrong while writing to the file. Please check the stack-trace.", ex);
        }
    }

    /**
     * It closes the file.
     */
    public void closeFile() {
        try {
            this.fileWrite.close();
        } catch (FileNotFoundException ex) {
            //System.out.println("Problem in closing log info file.");
            logger.log(Level.SEVERE, "Please check your output file. It is not available.", ex);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Something is wrong while closing the file. Please check the stack-trace.", ex);
        }
    }

}
