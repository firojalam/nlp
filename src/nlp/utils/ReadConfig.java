/**
 * This work is licensed under a Creative Commons
 * Attribution-NonCommercial-ShareAlike 3.0 Unported License. <br/>
 * http://creativecommons.org/licenses/by-nc-sa/3.0/
 */
package nlp.utils;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This is used to the configuration file of the system, which contains
 * different parameters value. And stores the parameters and values in a
 * dictionary as a key-value pair.
 *
 * @author Firoj Alam (firojalam@gmail.com)
 * @version $Revision: 0.1 12/28/2013 $
 */
public class ReadConfig {

    protected HashMap<String, String> configDict = new HashMap<String, String>();

    public ReadConfig(String fileName) {
        setConfig(fileName);
    }

    private void setConfig(String fileName) {
        try {
            BufferedReader fileRead = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
            String str = "";
            while ((str = fileRead.readLine()) != null) {
                if (str.startsWith("#") || str.trim().isEmpty()) {
                    continue;
                } else {
                    String[] strArray = str.split("\\s*=\\s*");
                    String key = strArray[0].trim();
                    String val = strArray[1].trim();
                    if (!this.configDict.containsKey(key)) {
                        this.configDict.put(key, val);
                    }
                }
            }//end read file
            fileRead.close();
        } catch (FileNotFoundException ex) {
            System.out.println("Please check your configuration file. Check that whether it is exist or not.");
            Logger.getLogger(ReadConfig.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReadConfig.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public HashMap<String, String> getConfigDict() {
        return this.configDict;
    }

    //main method
    public static void main(String[] args) {
        //for testing
        ReadConfig obj;
        obj = new ReadConfig("config.txt");
    }
}
