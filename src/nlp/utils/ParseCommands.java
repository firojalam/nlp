/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nlp.utils;

import java.io.File;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import nlp.cdr.re.DataReader;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 *
 * @author firojalam
 */
public class ParseCommands {
    private static final Logger logger = Logger.getLogger(DataReader.class.getName());
    
    /**
     * 
     * @param file a file to check its existence
     */
    private void checkFileExistance(String file) {
        try {
            File f = new File(file);
            if (!f.exists()) {
                logger.info(f + " file does not exist. Please check the file.");
                System.exit(0);
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Something is wrong. Please follow the stack-trace", ex);
            System.exit(0);
        }
    }
    
    /**
     * Parse command line arguments for the <em>FeatureVectorConversion<em\> class
     * @param args command line arguments
     * @return dictionary containing the options
     */
    public CommandLine parseCommands(String[] args) {
        // automatically generate the help statement
        HelpFormatter formatter = new HelpFormatter();
        
        // create the parser
        CommandLineParser parser = new GnuParser();
        // create the Options
        Option inputFile = OptionBuilder.withArgName("input-dir")
                .hasArg()
                .withDescription("A directory path")
                .create("i");
        Option configFile = OptionBuilder.withArgName("config-file")
                .hasArg()
                .withDescription("Name of the config file")
                .create("c");
        Option tokForm = OptionBuilder.withArgName("token-form")
                .hasArg()
                .withDescription("Token-form [token|lemma]")
                .create("t");
        
        Options options = new Options();
        options.addOption(inputFile);
        options.addOption(configFile);
        options.addOption(tokForm);
        String usageString ="";
        CommandLine cmds =null;
        try {
            // parse the command line arguments
            cmds = parser.parse(options, args);
            usageString = "java -classpath NLP.jar nlp.cdr.re.REpipeline -i inputDir -c config.txt -t [token|lemma]";
            
            if(cmds.hasOption("i") && cmds.hasOption("c") && cmds.hasOption("t")){                
                this.checkFileExistance(cmds.getOptionValue("i"));
                this.checkFileExistance(cmds.getOptionValue("c"));
            }else{
                formatter.printHelp(usageString, options);
                System.exit(0);            
            }
        } catch (ParseException exp) {
            formatter.printHelp(usageString, options);
            logger.log(Level.SEVERE, "Please check the options. {0}", exp.getMessage());
            System.exit(0);
        }
        return cmds;
    }     
    /**
     * Parse command line arguments for the <em>FeatureVectorConversion<em\> class
     * @param args command line arguments
     * @return dictionary containing the options
     */
    public CommandLine parseCommandsSent(String[] args) {
        // automatically generate the help statement
        HelpFormatter formatter = new HelpFormatter();
        
        // create the parser
        CommandLineParser parser = new GnuParser();
        // create the Options
        Option inputFile = OptionBuilder.withArgName("input-dir")
                .hasArg()
                .withDescription("A directory path")
                .create("i");
        Option configFile = OptionBuilder.withArgName("config-file")
                .hasArg()
                .withDescription("Name of the config file")
                .create("c");
        Option outputFile = OptionBuilder.withArgName("output-svm-file")
                .hasArg()
                .withDescription("Name of the output file")
                .create("o");
        Option goldFile = OptionBuilder.withArgName("gold-annotation-file")
                .hasArg()
                .withDescription("Gold annotation file")
                .create("g");
        Option tokForm = OptionBuilder.withArgName("token-form")
                .hasArg()
                .withDescription("Token-form [token|lemma]")
                .create("t");
        
        Options options = new Options();
        options.addOption(inputFile);
        options.addOption(configFile);
        options.addOption(outputFile);
        options.addOption(goldFile);
        options.addOption(tokForm);
        String usageString ="";
        CommandLine cmds =null;
        try {
            // parse the command line arguments
            cmds = parser.parse(options, args);
            usageString = "java -classpath NLP.jar nlp.cdr.dataprepare.PreprocessorSent -i inputDir -c config.txt -g gold.pubtator.txt -o output.dat -t [token|lemma]";
            
            if(cmds.hasOption("i") && cmds.hasOption("c")&&cmds.hasOption("g") && cmds.hasOption("o") && cmds.hasOption("t")){                
                this.checkFileExistance(cmds.getOptionValue("i"));
                this.checkFileExistance(cmds.getOptionValue("c"));
                this.checkFileExistance(cmds.getOptionValue("g"));
            }else{
                formatter.printHelp(usageString, options);
                System.exit(0);            
            }
        } catch (ParseException exp) {
            formatter.printHelp(usageString, options);
            logger.log(Level.SEVERE, "Please check the options. {0}", exp.getMessage());
            System.exit(0);
        }
        return cmds;
    }     
    /**
     * Parse command line arguments for the <em>FeatureVectorConversion<em\> class
     * @param args command line arguments
     * @return dictionary containing the options
     */
    public CommandLine parseCommandsSentClassifier(String[] args) {
        // automatically generate the help statement
        HelpFormatter formatter = new HelpFormatter();
        
        // create the parser
        CommandLineParser parser = new GnuParser();
        // create the Options
        Option inputFile = OptionBuilder.withArgName("input-dir")
                .hasArg()
                .withDescription("A directory path")
                .create("i");
        Option configFile = OptionBuilder.withArgName("config-file")
                .hasArg()
                .withDescription("Name of the config file")
                .create("c");
        Option model = OptionBuilder.withArgName("model-file")
                .hasArg()
                .withDescription("SVM_light model file")
                .create("m");
        Option outputFile = OptionBuilder.withArgName("output-file")
                .hasArg()
                .withDescription("Annotated output file; optional")
                .create("o");

        Option tokForm = OptionBuilder.withArgName("token-form")
                .hasArg()
                .withDescription("Token-form [token|lemma]")
                .create("t");
        
        Options options = new Options();
        options.addOption(inputFile);
        options.addOption(configFile);
        options.addOption(model);
        options.addOption(outputFile);
        options.addOption(tokForm);
        String usageString ="";
        CommandLine cmds =null;
        try {
            // parse the command line arguments
            cmds = parser.parse(options, args);
            usageString = "java -classpath NLP.jar nlp.cdr.re.RESentClassifierPipeline -i inputDir -c config.txt -m model -o output.pubtator -t [token|lemma]";
            
            if(cmds.hasOption("i") && cmds.hasOption("c") && cmds.hasOption("m") && cmds.hasOption("t")){                
                this.checkFileExistance(cmds.getOptionValue("i"));
                this.checkFileExistance(cmds.getOptionValue("c"));
                this.checkFileExistance(cmds.getOptionValue("m"));
            }else{
                formatter.printHelp(usageString, options);
                System.exit(0);            
            }
        } catch (ParseException exp) {
            formatter.printHelp(usageString, options);
            logger.log(Level.SEVERE, "Please check the options. {0}", exp.getMessage());
            System.exit(0);
        }
        return cmds;
    }     

    /**
     * 
     * @param args 
     */
    public static void main(String[] args) {
        ParseCommands obj = new ParseCommands();
        
    }
}