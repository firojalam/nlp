/*
 * This work by Firoj Alam is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License.
 * Permissions beyond the scope of this license may be available by sending an email to firojalam@gmail.com.
 * http://creativecommons.org/licenses/by-nc/4.0/deed.en_US
 * 
 */


package nlp.utils;

import java.io.File;

/**
 *
 * @author Firoj Alam
 */
public class LibraryPath {

    private static String libPath = "";

    public LibraryPath() {
        this.setLibPath();
    }

    /**
     * Get the value of libPath
     *
     * @return the value of name of the current working directory (directory of the .jar)
     */
    public String getLibPath() {
        return libPath;
    }

    /**
     * Set the value of libPath
     *
     * @param libPath new value of libPath
     */
    public void setLibPath() {
        this.libPath = "";
        String decodedPath = "";
        try {
            File jarFile = new File(LibraryPath.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
            decodedPath = jarFile.getParentFile().getPath();
            //System.out.println("current jar dir: " + decodedPath);
        } catch (Exception ex) {
            System.err.println("Problem in locating the path of the library. "+decodedPath);
            //ex.printStackTrace();
        }
       //decodedPath = "/home/firoj.alam/JavaProjects/NLP"; 
       //decodedPath = "/Users/firojalam/Study_PhD_projects/JavaProjects/NLP"; 
       
       this.libPath=decodedPath;
    }

}
