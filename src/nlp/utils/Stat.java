/**
 * This work is licensed under a Creative Commons
 * Attribution-NonCommercial-ShareAlike 3.0 Unported License. <br/>
 * http://creativecommons.org/licenses/by-nc-sa/3.0/
 */

package nlp.utils;

import java.util.ArrayList;

/**
 * This is to calculate different stats from the ArrayList of numbers
 * @author Firoj Alam (firojalam@gmail.com)
 * @version $Revision: 0.1 12/28/2013 $
 */
public class Stat {
    
    
    public Stat(){
    
    }
    /**
     * Calculate the mean with respect to the number of elements. This is
     * defined as<p/>
     * <
     * pre>
     * sum ---------------------- number elements
     * </pre>
     *
     * @return the mean
     */
    public double calMean(ArrayList<Double> list) {
        double sum = 0.0;
        for (int i = 0; i < list.size(); i++) {
            sum += list.get(i);
        }
        double mean = sum / list.size();

        return mean;
    }
    /**
     * 
     * @param list
     * @return 
     */
    public double calStd(ArrayList<Double> list){
    
        // compute sample mean
        double sum = 0.0;
        int N = list.size();
        for (double x : list)
            sum += x;
        double mean = sum/N;
        
        for (double x : list) {
            sum += (x - mean) * (x - mean);
        }
        double std = Math.sqrt(sum/(N-1));
        return std;
    }

    
    /**
     * Calculate the sum with respect to the number of elements. This is
     * defined as<p/>
     * <
     * pre>
     * sum ---------------------- number elements
     * </pre>
     *
     * @param list of double type
     * @return the mean
     */
    public double calSum(ArrayList<Double> list) {
        double sum = 0.0, n = 0.0, mean = 0.0;
        for (int i = 0; i < list.size(); i++) {
            sum += Double.parseDouble(list.get(i).toString());
        }
        return sum;
    }

    /**
     * Calculate the mean with respect to the number of elements. This is
     * defined as<p/>
     * <
     * pre>
     * sum ---------------------- number elements
     * </pre>
     * @param list of Integer type
     * @return the mean
     */
    public double calMeanInt(ArrayList<Integer> list) {
        double sum = 0.0, n = 0.0, mean = 0.0;
        for (int i = 0; i < list.size(); i++) {
            sum += Double.parseDouble(list.get(i).toString());
        }
        double meanPre = sum / list.size();

        return meanPre;
    }    
    /**
     * Calculates standard deviation
     * @param list of Integer type
     * @return standard deviation
     */
    public double calStdInt(ArrayList<Integer> list){
        // compute sample mean
        double sum = 0.0;
        int N = list.size();
        for (int x : list)
            sum += x;
        double mean = sum/N;
        
        for (int x : list) {
            sum += (x - mean) * (x - mean);
        }
//        for (int x = 0; x < list.size(); x++) {
//            sum += (list.get(x) - mean) * (list.get(x) - mean);
//        }
        double std = Math.sqrt(sum/(N-1));
        return std;
    }    
    /**
     * Calculate the sum with respect to the number of elements. This is
     * defined as<p/>
     * <
     * pre>
     * sum ---------------------- number elements
     * </pre>
     *
     * @param list of Integer type
     * @return the mean
     */
    public int calSumInt(ArrayList<Integer> list) {
        int sum = 0;
        for (int i = 0; i < list.size(); i++) {
            sum += Integer.parseInt(list.get(i).toString());
        }
        return sum;
    }    
    public static void main(String[] args){
        //for testing
        Stat obj;
        obj = new Stat();
        ArrayList<Double> list = new ArrayList<>();
        list.add(10.0);
        list.add(20.0);
        list.add(30.0);
        list.add(40.0);
        list.add(120.0);
        list.add(10.0);
        list.add(30.0);
        list.add(20.0);
        list.add(10.0);
        list.add(60.0);
        double val = obj.calStd(list);
        System.out.println(""+val);
    }
    
}
