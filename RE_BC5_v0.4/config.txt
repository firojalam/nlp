###### Config file for RE #####
##### Firoj Alam #####
## Uses relative path here by assuming that all of its dependents will be in the same folder
## The goal is to make the whole system as less independent as possible.
## Please do change any variable on the left side of equal (=) sign.

SVM_linux=bin/svm/linux/svm_classify
SVM_mac=bin/svm/mac/svm_classify
model=bin/models/train.model
BF_dict=featextractor/dict/bf_set1.txt
FEAT_selected=featextractor/dict/dictSelectedFeatures.txt
FEAT_extractor=featextractor/bin/extractFeatures/extractFeatures_v1.py
SVM_feat=featextractor/bin/prepareSVMFiles/prepareSVMFiles_v3.py

######RE dictionary from train+dev set
#RE_dict=etc/train_re.ser
RE_dict=etc/train_dev_re.ser

