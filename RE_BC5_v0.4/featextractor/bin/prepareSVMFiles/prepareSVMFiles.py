#! /usr/bin/python3.4
# Author: Anna Corazza, 24 Jul 2015
# 28 Jul 2015: second version including barrier features
# 29 Jul 2015: bug fix and modification of the syntax of the annotation file

# A feature selection step is based on a precompiled dictionnary
# A few additional features can be included
# Input: a file containing informations about entity and relations
# occurring in the document:
# a line for each entity in the document, containing:
# entityCode, number of corresponding mentions N, i_1, j_1, i_2, j_2,
#                                                               ..., i_N, j_N
# where the entity code is for example D007069 or C064227; i and j are
# the indexes of the first and last tokens of each mention; the end of
# sentence is taken into account in this numbering; fields are
# separated by a blank
#
# Following these lines, a line 
# RELATION
# and then, a line for each potential relation, that is
# for each pair of entities which is compatible with a relation; these
# lines should contain:
# isARel, CID, entityCode, typeOfEntity, entityCode, typeOfEntity
# where isARel is a Boolean flag (1 for true, -1 for false) signalling
#                     wether we are considering a golden case relation
# CID is the only type of relation we are considering now, but I would
#                    insert it for generality
# entityCode as above
# typeOfEntity is either Chemical or Disease
# the last two fields are repeated for each of the two entities
# involved in the relation
# Again, all fields are separated by blanks. 
# Output: the file in the SVM format

import sys, re, os
from datetime import datetime

# input: a string containing the barrier feature notation, which can
# assume two different forms:
#    bar:Barrierar(DT-VBN)={NNS, VBD, CC}
#    setBarriera_sinistra(DT-NNP)={NNP,POS}

# output: a list [direction, delimiters, contents], where direction
# can be l(eft) or r(ight), delimiters are the trigger and the
# end-point and contents is a list corresponding to the set of the PoS
# between the two
def extractBarrierFeature(ff):
    if ff.startswith('bar:'):
        l = len('bar:Barriera');
        direction=ff[l];
    else:    # ff.startswith('setBarriera_'):
        if ff.startswith('setBarriera_sinistra'):
            direction = 'l';
        else:   # ff.startswith('setBarriera_destra'):
            direction = 'r';
    [prefix,pattern] = ff.split('(',1);
    [delimiters, rest] = pattern.split(')',1);
    # COMMA 
    rest=rest.replace('{,,', '{COMMA, ');
    rest=rest.replace(',,}', ',COMMA}');
    rest=rest.replace(',,,', ',COMMA, ');
    contents = rest[2:len(rest)-1].split(',');
    return [direction, delimiters, contents];

# a BF activate a dictionnary one whenever the first two args are
# exactly the same, while the contents is included in the one of the
# dictionary
def isBarrierFeatureIn(a,b):
    if a[0:1] == b[0:1] and set(a[2]).issubset(set(b[2])):
        return True;
    return False;

def buildFeatureVector(tokens, pos, j, dictFeatures):
    # word features
    word = tokens[j];
    l=len(word);
    featList = ['ca:'+word, 'cz:'+ word[len(word)-1], 'len:'+str(len(word))];

    if (l>2):
        featList.append('prefix:'+word[0:2]);
        featList.append('suffix:'+word[l-2:l]);
    if (l>3):
        featList.append('prefix:'+word[0:3]);
        featList.append('suffix:'+word[l-3:l]);
    if (l>4):
        featList.append('prefix:'+word[0:4]);
        featList.append('suffix:'+word[l-4:l]);
    
    # La parola inizia con una maiuscola
    pattern = re.compile(r'^[A-ZÇÑÁÉÍÓÚÀÈÌÒÙÄËÏÖÜ].*$')
    if pattern.match(word):
        featList.append('SA');

    # La parola inizia con una minuscola
    pattern = re.compile(r'^[a-zçñáéíóúàèìòùäëïöü].*$')
    if pattern.match(word):
        featList.append('Sa');

    # La parola contiene una minuscola
    pattern = re.compile(r'^.+[A-ZÇÑÁÉÍÓÚÀÈÌÒÙÄËÏÖÜ].*$');
    if pattern.match(word):
        featList.append('CA');

    # La parola contiene piu' lettere minuscole
    pattern = re.compile(r'^.*[A-ZÇÑÁÉÍÓÚÀÈÌÒÙÄËÏÖÜ].*[A-ZÇÑÁÉÍÓÚÀÈÌÒÙÄËÏÖÜ].*$');
    if pattern.match(word):
        featList.append('CAA');

    # Le lettere della parola sono tutte maiuscole
    pattern = re.compile(r'^.*[A-ZÇÑÁÉÍÓÚÀÈÌÒÙÄËÏÖÜ].*[A-ZÇÑÁÉÍÓÚÀÈÌÒÙÄËÏÖÜ].*$');
    if pattern.match(word):
        featList.append('AA');

    # Le lettere della parola sono tutte minuscole
    pattern = re.compile(r'^[a-zçñáéíóúàèìòùäëïöü]+$')
    if pattern.match(word):
        featList.append('aa');

    # La parola inizia con un numero
    pattern = re.compile(r'^[0-9].*$')
    if pattern.match(word):
        featList.append('SN');

    # La parola contiene un numero
    pattern = re.compile(r'^.*[0-9].*$')
    if pattern.match(word):
        featList.append('CN');

    # La parola contiene un punto
    pattern = re.compile(r'^.*[\.].*$')
    if pattern.match(word):
        featList.append('CP');

    # La parola contiene una virgola
    pattern = re.compile(r'^.*[\,].*$');
    if pattern.match(word):
        featList.append('CC');

    # La parola contiene un trattino
    pattern = re.compile(r'^.*[\-].*$')
    if pattern.match(word):
        featList.append('MW');

    # word and POS unigrams, bigrams and trigrams of a window of length 5 centered in the word
    l = len(tokens);
    if (j > 1):
        featList.append('unigram_word:' + tokens[j - 2])
        featList.append('bigram_word:' + tokens[j - 2] + "----" + tokens[j - 1])
        featList.append('trigram_word:' + tokens[j - 2] + '----' + tokens[j - 1] + '----' + tokens[j])
        featList.append('unigram_pos:' + pos[j - 2])
        featList.append('bigram_pos:' + pos[j - 2] + "----" + pos[j - 1])
        featList.append('trigram_pos:' + pos[j - 2] + '----' + pos[j - 1] + '----' + pos[j])
        if (j < (l - 1)):
            featList.append('trigram_word:' + tokens[j - 2] + '----' + tokens[j - 1] + '----' + tokens[j])
            featList.append('trigram_pos:' + pos[j - 2] + '----' + pos[j - 1] + '----' + pos[j])
    if (j > 0):
        featList.append('unigram_word:' + tokens[j - 1])
        featList.append('bigram_word:' + tokens[j - 1] + "----" + tokens[j])
        featList.append('unigram_pos:' + pos[j - 1])
        featList.append('bigram_pos:' + pos[j - 1] + "----" + pos[j])
        if (j < (l - 1)):
            featList.append('trigram_word:' + tokens[j - 1] + '----' + tokens[j] + '----' + tokens[j + 1])
            featList.append('trigram_pos:' + pos[j - 1] + '----' + pos[j] + '----' + pos[j + 1])
    featList.append('unigram_word:' + tokens[j])
    featList.append('unigram_pos:' + pos[j])
    if (j < (l - 1)):
        featList.append('unigram_word:' + tokens[j + 1])
        featList.append('bigram_word:' + tokens[j] + "----" + tokens[j + 1])
        featList.append('unigram_pos:' + pos[j + 1])
        featList.append('bigram_pos:' + pos[j] + "----" + pos[j + 1])
    if (j < (l - 2)):
        featList.append('unigram_word:' + tokens[j + 2])
        featList.append('bigram_word:' + tokens[j + 1] + "----" + tokens[j + 2])
        featList.append('unigram_pos:' + pos[j + 2])
        featList.append('bigram_pos:' + pos[j + 1] + "----" + pos[j + 2])

    outList = [];
    for feat in featList:
        try:
            outList.append(dictFeatures.index(feat));
        except:
            pass;
    return outList;

def buildBarrierFeatureVector(pos, j, dictBarrierFeatures, bias):
    bfList = [];
    index = bias;
    for barrierFeat in dictBarrierFeatures:
        # print ( barrierFeat, file=sys.stderr);
        (trigger,endPoint) = barrierFeat[1].split('-');  
        if pos[j] == trigger:
            posList = [];
            if barrierFeat[0] == 'l':    # left barrier features
                for ptr in range(j):
                    if pos[j-ptr] == endPoint or pos[j-ptr] == 'EOS':
                        break;
                    if pos[j-ptr] not in posList:
                        posList.append(pos[j-ptr]);
            else:    # right barrier
                for ptr in range(len(pos)-j):
                    if pos[j+ptr] == endPoint or pos[j+ptr] == 'EOS':
                        break;
                    if pos[j+ptr] not in posList:
                        posList.append(pos[j+ptr]);
            if set(posList).issubset(set(barrierFeat[2])):
                bfList.append(index);

        index += 1;
    return bfList;



############# Main program starts here
try:
    dictFeaturesFile = open(sys.argv[1], 'r');
#    INPUTDIR = sys.argv[2]
#    ANNOTATIONDIR = sys.argv[3]    
#    fout = open(sys.argv[4],"w")
    tokenFileName = sys.argv[2]
    annotationFileName = sys.argv[3]    
    outFileName = sys.argv[4]
    #print ( 'dictFeaturesFile INPUTDIR ANNOTATIONDIR fout');
    print ('dictFeaturesFile'+' '+INPUTDIR+' '+ANNOTATIONDIR+' '+sys.argv[4], file=sys.stdout);
except:
    print ('Correct syntax: ' + sys.argv[0] + ' dictFeaturesFile tokenFileName annotationFileName svmOutputFile',file=sys.stdout);
    exit(0);
    
# reading the feature dictionnaries (BF and the others)
dictFeatures = [];
dictBarrierFeatures = [];
for line in dictFeaturesFile:
    line = line[0:len(line)-1];  # chop
    fields = line.split();
    if fields[1].startswith('bar:'):
        dictBarrierFeatures.append(extractBarrierFeature(''.join(fields[1:len(fields)-1])));
    else:
        dictFeatures.append(''.join(fields[1:len(fields)-1]));
featN = len(dictFeatures)+len(dictBarrierFeatures);

dictFeaturesFile = open('dictFeaturesFile',"w")
dictBarrierFeaturesFile = open('dictBarrierFeaturesFile',"w")

index = 1;
for feat in dictFeatures:
    print(index, feat, file=dictFeaturesFile);
    index += 1;
for feat in dictBarrierFeatures:
    print(index, feat[0], feat[1], feat[2],file=dictBarrierFeaturesFile);
    index += 1;

# reading the list of files corresponding to the documents to process
#fileList = os.listdir(INPUTDIR);

#for fileName in fileList:
#(name, ext) = fileName.split('.');
# processing the file containg the pairs token\tPOS
try:
    tokenFile = open(tokenFileName, 'r');
except:
    print ('Warning: not able to open the input file ' + tokenFileName,file=sys.stderr);

tokens = []; 
pos = [];
for line in tokenFile:
    line = line[0:len(line)-1];  # chop
    if line.startswith('>>>>'):
        tokens.append('EOS');
        pos.append('EOS');
    else:
        fields = line.split();
        tokens.append(fields[0]);
        pos.append(fields[1]);
tokenFile.close();

# processing the annotation file
try:
    annotationFile = open(annotationFileName, 'r');
except:
    print ('Warning: not able to open annotation file ' + annotationFileName,file=sys.stderr);
print ('Processing annotation file ' + annotationFileName,file=sys.stderr);

# entity lines first
entityList = {};    
for line in annotationFile:
    if line.startswith('RELATION'):
        break;
    line = line[0:len(line)-1];  # chop
    fields = line.split();
    if len(fields) == 0:
        break;
    # Construction of the part of the feature vector corresponding 
    # to the entity
    # fields: entityCode, number of corresponding mentions N, mentions
    featureVector = [];
    ptr = 2;
    for i in range(int(fields[1])):
        for j in range(int(fields[ptr]),int(fields[ptr+1])+1): 
            featureVector += buildFeatureVector(tokens, pos, j-1, dictFeatures); # possible repetitions
            featureVector += buildBarrierFeatureVector(pos, j-1, dictBarrierFeatures, len(dictFeatures)); # possible repetitions
        ptr += 2;
        # entityList[fields[0]] = set(featureVector); # to delete repetitions
        entityList[fields[0]] = list(set(featureVector)); # to delete repetitions
        entityList[fields[0]].sort();

# and then relation lines
for line in annotationFile:
    line = line[0:len(line)-1];  # chop
    fields = line.split();
    if len(fields) == 0:
        break;
    # len(dictFeatures) must be added to indexes of the second
    # entity for every feature; futhermore, the value of the
    # features is always 1 as the features are Boolean
    # SVM features indexes start from 1
    out = fields[0] + ' ';
    try:
        for feat in entityList[fields[2]]:
            out += str(feat+1) + ':1 ';
        for feat in entityList[fields[4]]:
            out += str(feat + 1 + featN) + ':1 ';
        for i in range(6,len(fields)):    # additional features
            if int(fields[i]) > 0:
                out += str(2*featN+i+1-6) + ':5 ';
        print (out, file=fout);
    except: 
        print ( 'Wrong annotation (may be due to preceding errors) in annotation file ' + name,file=sys.stderr); 
        print ( fields,file=sys.stderr);
        print ( "Unexpected error:", sys.exc_info()[0], file=sys.stderr);
        raise;

annotationFile.close();
fout.close();
print  ('Document ' + annotationFileName+ ' completed',file=sys.stderr);

