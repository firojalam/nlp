input files in data/input/
annotation files in data/annotation/
output file on stdout
comments on stderr

Requirement:
python3.4
for the other python scripts, the common version of python has been used

python package nltk.corpus for wordnet

./bin/extractFeatures/doExtractFeatures.sh 
~/anaconda3/bin/python3 ./bin/prepareSVMFiles/prepareSVMFiles.py dict/dictSelectedFeatures.txt 
~/tools/anaconda/bin/python ./bin/prepareSVMFiles/prepareSVMFiles.py dict/dictSelectedFeatures.txt >data/train.svm.dat
~/tools/anaconda/bin/python ./bin/prepareSVMFiles/prepareSVMFiles.py dict/dictSelectedFeatures.txt >data/dev.svm.dat

./bin/svm_learn data/dev.svm.dat models/model
./bin/svm_classify data/dev.svm.dat models/model data/dev.pred.txt

./bin/svm_learn data/train.svm.dat models/train.model
./bin/svm_classify models/train.model models/model data/dev.pred.txt