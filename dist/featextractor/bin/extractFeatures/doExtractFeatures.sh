#!/bin/bash
# Script completo Estrazione feature Entità
# Author: Anita Alicante
# modified by: Anna Corazza, June 2015

basedir="$PWD"
#basedir='/home/corazza/progetti/in-corso/2015/BioCreative/BioCreativeSystem/version0.0-20150727/'
# cartella che contiene script
bindir=${basedir}'/bin/extractFeatures/'
# dizionario delle features
dictDir=${basedir}'/dict'
# dati vari
dataDir=${basedir}'/data'

if [ -d "${dataDir}features" ]; then
        echo "${dataDir}features exists: check if it has redundant files"
else
        mkdir ${dir_tmp}
fi

list=`ls ${dataDir}/input/`

# Estrazione delle features 
for file in $list
do
	echo "Inizio estrazione features $file"
	nameFile=`basename $file .token`
	#Input:
	# Parametro1: token pos per ogni riga 
	# Parametro2: barrier features pairs
	#Output: features for token
	python3.4 ${bindir}extractFeatures.py ${dataDir}/input/${file} ${dictDir}/bf_set1.txt > ${dataDir}/features/${nameFile}.features
	echo "Fine Estrazione delle features $nomeFIle"

done
