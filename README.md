# README ::: Relation Extraction System for BC5 #

### What is this repository for? ###
Relation Extraction System for Biomedical domain is a system capable of extracting relation between chemical and disease.

The system expect a config file and and an input directory containing *.content.restored.iob2 and *.dner.output.pubtator files.
The config contains information about the SVMlight model name with associated path and SVMlight tools' path. 
It uses two external python scripts to extract features. Please see the details in featextractor/README.txt
It is then usages SVMlight clssifier to classify relations in a document.
After that it produces an output file with a name *.cid.output.pubtator and saves it in the same input directory.

### Version ###
Version 0.1, Date: August 1, 2015
Version 0.6, Date: Oct 25, 2015

### How to run RE system?? ###

* Summary of the setup
Note: - Before running the system, please update the latest classification model and copy it in ./bin/models/. 
Also change the model name in config file too. 


* Dependencies
- Java 1.7 or later
- python 3.4
- SVM_light - bundled with this system

* Configuration
java -classpath NLP.jar nlp.cdr.re.REpipeline -i sample/ -c config.txt -t [token|lemma]

usage: java -classpath NLP.jar nlp.cdr.re.REpipeline -i inputDir -c config.txt
 -c <config-file>   Name of the config file
 -i <input-dir>     A directory path
 -t <token-form> Token-form


* How to run tests
Untar the RE_BC5_v*.tar.gz
tar -zxvf RE_BC5_v*.tar.gz
cd RE_BC5_v*/

java -classpath NLP.jar nlp.cdr.re.REpipeline -i sample/ -c config.txt -t [token|lemma]

usage: java -classpath NLP.jar nlp.cdr.re.REpipeline -i inputDir -c config.txt -t token
 -c <config-file>   Name of the config file
 -i <input-dir>     A directory path
 -t <token-form> Token-form

### Extract Feature Vector for SVM_light ###
To run the experiments with sentence level features, the following command can be used.

java -classpath NLP.jar nlp.cdr.dataprepare.PreprocessorSent -i inputDir -c config.txt -g gold.pubtator.txt -o output.dat -t [token|lemma]

usage: java -classpath NLP.jar nlp.cdr.dataprepare.PreprocessorSent -i
            inputDir -c config.txt -g gold.pubtator.txt -o output.dat -t
            [token|lemma]
 -c <config-file>            Name of the config file
 -g <gold-annotation-file>   Gold annotation file
 -i <input-dir>              A directory path
 -o <output-svm-file>        Name of the output file
 -t <token-form>             Token-form [token|lemma]

### To obtain the classification results ###
To run the experiments with sentence level features, the following command can be used.
java -classpath NLP.jar nlp.cdr.re.RESentClassifierPipeline -i inputDir -c config.txt -m model -o output.pubtator -t [token|lemma]

### Document and Sentence level classifiers combined ###
To run the experiments with sentence level features, the following command can be used.

java -classpath NLP.jar nlp.cdr.re.REMultiClassifierPipeline -i inputDir -c config.txt -t token


### Contribution guidelines ###

* Code review


### Who do I talk to? ###
Firoj Alam, firojalam@gmail.com